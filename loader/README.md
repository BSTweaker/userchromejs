UserChromeJSLoader.mjs
======================

いわゆるSub-Script Loaderに代わるuserChrome.jsのスクリプトローダです。Firefox 115以降で動作します。使用するには`UserChromeJSLoader.mjs`ファイルをプロファイル内の`chrome`ディレクトリに配置したうえでautoconfigスクリプトに
```javascript
// skip 1st line
try {
    let loader = Services.dirsvc.get("UChrm", Ci.nsIFile);
    loader.append("UserChromeJSLoader.mjs");
    if (loader.exists) {
        let resourceHandler = Services.io.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
        resourceHandler.setSubstitution(`UCJSLoader-${loader.lastModifiedTime}`, Services.io.newFileURI(loader.parent));
        ChromeUtils.importESModule(`resource://UCJSLoader-${loader.lastModifiedTime}/${encodeURIComponent(loader.leafName)}?${loader.lastModifiedTime}`);
    }
} catch (e) {Cu.reportError(e)}
```
を記述します。従来のSub-Script Loaderを読み込むコードは取り除いてください。

デフォルトでは`chrome`直下と`chrome/SubScript`フォルダ内にある`.uc.js`ファイルを探して読み込みます。それ以外のフォルダにスクリプトを置きたい場合は`UserChromeJSLoader.mjs`の先頭にある設定に該当フォルダを追記してください。`rebuild_userChrome.uc.js`相当の機能 (ツールメニュー→UserChromeJSLoader) は内蔵しているため不要です。

最近のFirefoxの動きとして、[セキュリティ強化](https://bugzilla.mozilla.org/show_bug.cgi?id=1935985)のためにuserChrome.jsの利用が[制限](https://bugzilla.mozilla.org/show_bug.cgi?id=1938905)されそうな雰囲気があるため、このローダでは対策としてサンドボックス化・モジュール化の2つの選択肢を提供しています。デフォルトではサンドボックスは無効化されており、既存のスクリプトを動かすために特に変更は必要ありません。既存のスクリプトをサンドボックスで動かしたい、あるいはモジュール化を行いたい場合は以下の説明を参照してください。

スクリプトをサンドボックスで動かす
----------------------------------
スクリプトのメタデータセクションに`@sandbox true`を追加すると、そのスクリプトはサンドボックスで動作するようになります。サンドボックスが有効の場合、スクリプトは`@include`で指定された文書の`window`の代わりに、`window`を`prototype`に持つサンドボックスに対して`loadSubScript`されます。サンドボックス環境では主に

 * グローバルスコープで定義した変数等が`window`のプロパティにならない
 * `globalThis`やトップレベルでの`this`が`window`ではない

という差異があるため、これらの動作に依存したスクリプトは修正を行う必要があります。スクリプト全体を即時関数で覆っている場合はそのまま動作する可能性が高いと思われます。

ツール→UserChromeJSLoaderメニューからサンドボックスを常に有効にした場合、`@sandbox`メタデータの有無によらず全てのスクリプトがサンドボックスで動作するようになります。

モジュール型スクリプトを動かす
------------------------------
このローダは、ファイル名末尾が`.uc.mjs`であるスクリプトをESモジュールとして読み込みます。モジュール型スクリプトに対しては、従来`loadSubScript`が実行されるタイミングにおいて、代わりに`ChromeUtils.importESModule`が実行されます。サンドボックスの有効/無効はモジュール型スクリプトには影響しません。

モジュール型スクリプトを動作させるには、ソースコード内でファイル名から`.uc.mjs`を取り除いた名称のオブジェクトを`export`し、そのオブジェクト内に`onWindowLoad`という関数を定義しておきます。`onWindowLoad`関数はローダにより呼ばれ、引数には`@include`で対象にした文書の`window`オブジェクトが渡されるため、この関数をエントリポイントとして従来の`.uc.js`スクリプトと同等の処理を行うことができます。

ESモジュールは (プロセス単位の) シングルトンであるため、モジュール型スクリプトのファイル全体は、ファイルが最初に`importESModule`される一回に限り評価されるという点に注意が必要です。トップレベルスコープには`window`に依存した処理を書くことはできない代わりに、Actorの登録や`CustomizableUI`のウィジェット登録など、一度だけ行えばよい処理を記述するのに適しています。

`.uc.js`から`.uc.mjs`への変換例を以下に示します。
```javascript
// SampleScript.uc.js
document.getElementById("contentAreaContextMenu").addEventListener("popupshowing", event => {
    console.log(gBrowser.selectedTab.label);
});
```
この従来型のスクリプトは、モジュール化すると以下のような記述になります。
```javascript
// SampleScript.uc.mjs
export let SampleScript = {
    onWindowLoad(win) {
        win.document.getElementById("contentAreaContextMenu").addEventListener("popupshowing", event => {
            console.log(win.gBrowser.selectedTab.label);
        });
    }
};
```
