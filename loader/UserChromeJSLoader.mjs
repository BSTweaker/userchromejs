const ucjsLoaderConfig = {
    /* chrome以下にある.uc.jsが置いてあるフォルダを列挙する "."はchromeフォルダ直下を指す */
    subScriptFolders   : [".", "SubScript"],
    /* trueにするとスクリプトをファイル名の辞書順で読み込む */
    loadScriptsInOrder : true,
    /* chrome直下に保存される設定ファイル名 */
    configFileName     : "UserChromeJSLoader.config.json",
    /* スクリプトの編集 (メニュー右クリック) に使うエディタのパス */
    editorPath         : "",
};

const mainChromeWindowURL = "chrome://browser/content/browser.xhtml";
const chromeDir = PathUtils.join(PathUtils.profileDir, "chrome");
const baseResourceURI = Components.stack.filename.slice(0, Components.stack.filename.lastIndexOf("/"));
const lazy = {};
ChromeUtils.defineESModuleGetters(lazy, {
    setTimeout: "resource://gre/modules/Timer.sys.mjs",
    FileUtils: "resource://gre/modules/FileUtils.sys.mjs",
    PanelMultiView: "resource:///modules/PanelMultiView.sys.mjs",
});

class ScriptEntry {
    constructor(path, parentDir) {
        this.disabled = false;
        this.path = path;
        this.parentDir = parentDir;
        this.filename = PathUtils.filename(path);
        this.name = this.filename;
        this.lastModified = 0;
        this.invalid = false;
        this.isModule = this.filename.toLowerCase().endsWith(".uc.mjs");
        this.isSandboxed = false;
        this.isReady = this.load(false);
    }
    get uncachedFileURI() {
        return `${PathUtils.toFileURI(this.path)}?${this.lastModified}`;
    }
    get uncachedResourceURI() {
        let chromeDirURI = PathUtils.toFileURI(chromeDir);
        if (!chromeDirURI.endsWith("/")) chromeDirURI += "/";
        return `${baseResourceURI}/${this.uncachedFileURI.slice(chromeDirURI.length)}`;
    }
    load(isReload, force = false) {
        this.invalid = false;
        return IOUtils.stat(this.path).then(info => {
            if (info.type === "directory") throw new Error("This is a directory");
            if (!force && this.lastModified >= info.lastModified) {
                //console.log(`${this.name} is not modified`);
                return;
            }
            if (isReload) console.log(`Reload ${this.filename}`);
            this.lastModified = info.lastModified;
            return IOUtils.readUTF8(this.path).then(srcTxt => {
                let metadata = this.parseMetadata(srcTxt);
                let includes = [], excludes = [];
                if (Array.isArray(metadata.include)) {
                    for (let entry of metadata.include) {
                        entry = entry.trimEnd();
                        if (entry === "main") includes.push(mainChromeWindowURL);
                        else includes.push(entry);
                    }
                }
                if (Array.isArray(metadata.exclude)) {
                    for (let entry of metadata.exclude) {
                        entry = entry.trimEnd();
                        if (entry === "main") excludes.push(mainChromeWindowURL);
                        else excludes.push(entry);
                    }
                }
                if (includes.length === 0) includes.push(mainChromeWindowURL);
                if (excludes.length === 0) excludes.push("");
                this.matchRegExp = new RegExp(`(?!${this.createRegExpFromPatterns(excludes)})${this.createRegExpFromPatterns(includes)}`, "i");
                if (Array.isArray(metadata.name)) {
                    this.name = metadata.name[0];
                }
                if (Array.isArray(metadata.sandbox)) {
                    this.isSandboxed = (metadata.sandbox[0].trimEnd().toLowerCase() === "true");
                }
                else this.isSandboxed = false;
            });
        }).catch(e => {
            console.error(e);
            this.invalid = true;
        });
    }
    parseMetadata(srcTxt) {
        let metadata = {};
        let match = srcTxt.match(/\/\/\s*==UserScript==[\s\S]+?\/\/\s*==\/UserScript==/);
        if (!match) return metadata;
        match = match[0].split(/[\r\n]+/);
        for (let line of match) {
            if (!/\/\/\s*?@(\S+)($|\s+([^\r\n]+))/.test(line)) continue;
            let name  = RegExp.$1.toLowerCase().trim();
            let value = RegExp.$3;
            if (metadata[name]) {
                metadata[name].push(value);
            } else {
                metadata[name] = [value];
            }
        }
        return metadata;
    }
    createRegExpFromPatterns(patterns) {
        let regExp;
        try {
            regExp = patterns.map(pattern => {
                if (pattern.startsWith("/") && pattern.endsWith("/")) {
                    return pattern.slice(1, -1);
                }
                pattern = pattern.replace(/\*+/g, ".*");
                pattern = pattern.replace(/^\.\*\:?\/\//, "chrome://");
                pattern = pattern.replace(/^\.\*/, "(about|chrome):.*");
                return `^${pattern}$`;
            }).join("|");
        } catch (e) {
            console.error(e);
            regExp = "^$";
        }
        return regExp;
    }
    injectInto(win, context) {
        if (this.disabled) {
            return;
        }
        this.isReady.then(() => {
            let url = win.location.href;
            if (this.invalid || !this.matchRegExp.test(url)) return;
            if (this.isModule) {
                let moduleName = this.filename.slice(0, -7);
                let exportedModule = ChromeUtils.importESModule(this.uncachedResourceURI)[moduleName];
                let handler = exportedModule?.onWindowLoad;
                if (typeof handler === "function") handler.call(exportedModule, win);
            }
            else Services.scriptloader.loadSubScript(this.uncachedFileURI, context);
            console.log(`Injected ${this.parentDir}/${this.filename} to ${url}`);
        }).catch(e => {
            console.error(`Failed to inject script ${this.parentDir}/${this.filename}\n`, e);
        });
    }
    reload() {
        this.isReady = this.load(true);
    }
    edit() {
        if (!ucjsLoaderConfig.editorPath) return;
        try {
            const process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
            let app, args;
            if (Services.appinfo.OS === "Darwin" && ucjsLoaderConfig.editorPath.endsWith(".app")) {
                app = new lazy.FileUtils.File("/usr/bin/open");
                args = ["-a", ucjsLoaderConfig.editorPath, this.path];
            } else {
                app = new lazy.FileUtils.File(ucjsLoaderConfig.editorPath);
                args = [this.path];
            }
            process.init(app);
            process.runw(false, args, args.length);
        } catch (e) {console.error(e)}
    }
}

class Config {
    constructor(path) {
        this.path = path;
        this.container = new Map();
        this.isReady = IOUtils.readJSON(path).then(config => {
            if (typeof config === "object" && !Array.isArray(config)) {
                this.container = new Map(Object.entries(config));
            }
        }).catch(e => {});
    }
    get(key) {
        return this.container.get(key);
    }
    set(key, value) {
        this.container.set(key, value);
    }
    save() {
        try {
            IOUtils.writeJSON(this.path, Object.fromEntries(this.container));
        } catch (e) {}
    }
}

let scriptLibrary = new class {
    constructor(dirs) {
        this.scriptDirs = dirs;
        this.scripts = [];
        this.disabledScripts = null;
        this.groupedScripts = new Map();
        this.config = new Config(PathUtils.join(chromeDir, ucjsLoaderConfig.configFileName));
        this.isReady = this.load();
    }
    get sortedScripts() {
        return this.scripts.sort((a, b) => {
            return a.path > b.path ? 1 : -1;
        });
    }
    load() {
        return new Promise(resolve => {
            let newScripts = [];
            let existingScripts = [];
            let promises = [];
            let start = Date.now();
            for (let dir of this.scriptDirs) {
                promises.push(IOUtils.getChildren(PathUtils.joinRelative(chromeDir, dir)).then(files => {
                    for (let file of files) {
                        let filename = file.toLowerCase();
                        if (!filename.endsWith(".uc.js") && !filename.endsWith(".uc.mjs")) continue;
                        let loaded = false;
                        for (let script of this.scripts) {
                            if (script.path === file) {
                                existingScripts.push(script);
                                script.reload();
                                loaded = true;
                                break;
                            }
                        }
                        if (!loaded) newScripts.push(new ScriptEntry(file, dir));
                    }
                }).catch(e => {
                    //console.error(e);
                }));
            }
            if (!this.disabledScripts) {
                this.disabledScripts = [];
                promises.push(this.config.isReady.then(() => {
                    if (Array.isArray(this.config.get("disabled_scripts"))) {
                        this.disabledScripts = this.config.get("disabled_scripts");
                    }
                }).catch(e => console.error(e)));
            }
            Promise.all(promises).then(() => {
                console.log(`Scanning completed in ${(Date.now()-start).toFixed()} ms, found ${newScripts.length} new scripts`);
                for (let disabled of this.disabledScripts) {
                    let path = PathUtils.joinRelative(chromeDir, disabled);
                    for (let script of newScripts) {
                        if (script.path === path) script.disabled = true;
                    }
                }
                this.scripts = existingScripts.concat(newScripts);
                this.groupedScripts.clear();
                for (let script of this.sortedScripts) {
                    if (script.invalid) continue;
                    let dir = script.parentDir;
                    if (this.groupedScripts.has(dir)) this.groupedScripts.get(dir).push(script);
                    else this.groupedScripts.set(dir, [script]);
                }
                resolve();
            });
        });
    }
    reload() {
        this.isReady = this.load();
    }
    saveConfig() {
        let disabledScripts = [];
        for (let script of this.scripts) {
            if (script.disabled) {
                disabledScripts.push(script.path.slice(chromeDir.length+1));
            }
        }
        this.config.set("disabled_scripts", disabledScripts);
        this.config.save();
    }
}(ucjsLoaderConfig.subScriptFolders);

let loader = new class {
    constructor() {
        Services.obs.addObserver(this, "chrome-document-global-created", false);
        Services.obs.addObserver(this, "quit-application-granted", false);
        this.injectedWindows = new WeakMap();
    }
    observe(subject, topic, data) {
        switch (topic) {
            case "chrome-document-global-created":
                subject.addEventListener("load", this, {
                    capture: true,
                    once: true,
                });
                break;
            case "quit-application-granted":
                scriptLibrary.saveConfig();
                break;
        }
    }
    attachUIToWindow(win) {
        let toolsMenu = win.document.getElementById("menu_ToolsPopup");
        toolsMenu.appendChild(win.MozXULElement.parseXULToFragment(`
            <menu label="UserChromeJSLoader">
                <menupopup id="UCJSLoader-menu">
                    <menuitem label="有効" type="checkbox" id="UCJSLoader-enabled"/>
                    <menuitem label="常にサンドボックスを使う" type="checkbox" id="UCJSLoader-sandboxed"/>
                    <menuitem label="" disabled="true" id="UCJSLoader-status"/>
                    <menuseparator/>
                    <menuitem label="chrome フォルダを開く" id="UCJSLoader-open-folder"/>
                    <menuitem label="再スキャンして新規ウィンドウを開く" id="UCJSLoader-reload"/>
                </menupopup>
            </menu>
        `));
        toolsMenu.querySelector("#UCJSLoader-enabled")?.addEventListener("command", event => {
            let enabled = !scriptLibrary.config.get("disabled");
            scriptLibrary.config.set("disabled", !!enabled);
        });
        toolsMenu.querySelector("#UCJSLoader-reload")?.addEventListener("command", event => {
            scriptLibrary.reload();
            event.target.ownerGlobal.OpenBrowserWindow();
        });
        toolsMenu.querySelector("#UCJSLoader-sandboxed")?.addEventListener("command", event => {
            let enabled = !!scriptLibrary.config.get("force_sandbox");
            scriptLibrary.config.set("force_sandbox", !enabled);
        });
        toolsMenu.querySelector("#UCJSLoader-open-folder")?.addEventListener("command", event => {
            Services.dirsvc.get("UChrm", Ci.nsIFile).launch();
        });
        toolsMenu.addEventListener("popupshowing", event => {
            if (event.target !== toolsMenu) return;
            let parentMenu = win.document.getElementById("UCJSLoader-menu");
            let target = parentMenu.querySelector("#UCJSLoader-submenu-0");
            while (target) {
                let tmp = target.nextElementSibling;
                target.remove();
                target = tmp;
            }
            let n=0;
            for (let [key, value] of scriptLibrary.groupedScripts.entries()) {
                let label = (key === ".") ? "chrome" : `chrome/${key}`;
                parentMenu.appendChild(win.MozXULElement.parseXULToFragment(`
                    <menu label="${label}" id="UCJSLoader-submenu-${n}">
                        <menupopup/>
                    </menu>
                `));
                let submenu = parentMenu.querySelector(`#UCJSLoader-submenu-${n++} menupopup`);
                for (let script of value) {
                    let item = win.document.createXULElement("menuitem");
                    item.setAttribute("label", script.filename);
                    item.setAttribute("type", "checkbox");
                    item.setAttribute("checked", !script.disabled);
                    let callback = event => {
                        /* only command event is fired for right-click on macOS */
                        if (event.type === "command" && event.button === 0) script.disabled = !script.disabled;
                        else if (event.button === 2) script.edit();
                    };
                    item.addEventListener("command", callback, {once: true});
                    item.addEventListener("click", callback, {once: true});
                    submenu.appendChild(item);
                }
            }
            toolsMenu.querySelector("#UCJSLoader-enabled")?.setAttribute("checked", !scriptLibrary.config.get("disabled"));
            toolsMenu.querySelector("#UCJSLoader-sandboxed")?.setAttribute("checked", !!scriptLibrary.config.get("force_sandbox"));
            let statusText = this.injectedWindows.has(win) ? `サンドボックスは${this.injectedWindows.get(win) ? "全てのスクリプトで有効" : "要求時のみ有効"}です` : "userChrome.js は無効です";
            parentMenu.querySelector("#UCJSLoader-status")?.setAttribute("label", statusText);
        });
    }
    getSandboxFor(win) {
        let sb = new Cu.Sandbox(win, {
            sandboxPrototype: win,
            sameZoneAs: win,
        });
        /* toSource() is not available in sandbox */
        Cu.evalInSandbox(`
            Function.prototype.toSource = window.Function.prototype.toSource;
            Object.prototype.toSource = window.Object.prototype.toSource;
            Array.prototype.toSource = window.Array.prototype.toSource;
        `, sb);
        win.addEventListener("unload", () => {
            lazy.setTimeout(() => {
                Cu.nukeSandbox(sb);
            }, 0);
        }, {once: true});
        return sb;
    }
    injectInto(win, putWait = false) {
        if (putWait) return lazy.setTimeout(() => this.injectInto(win), 0);
        scriptLibrary.isReady.then(() => {
            if (!!scriptLibrary.config.get("disabled")) return;
            let forceSandbox = !!scriptLibrary.config.get("force_sandbox");
            let sandbox = null;
            this.injectedWindows.set(win, forceSandbox);
            for (let script of ucjsLoaderConfig.loadScriptsInOrder ? scriptLibrary.sortedScripts : scriptLibrary.scripts) {
                let context = win;
                if (forceSandbox || script.isSandboxed) {
                    if (!sandbox) sandbox = this.getSandboxFor(win);
                    context = sandbox;
                }
                script.injectInto(win, context);
            }
        });
    }
    handleEvent(event) {
        if (Services.appinfo.inSafeMode) return;
        if (event.type !== "load") return;
        let win = event.currentTarget;
        let isFrame = win.top !== win;
        let isMainWindow = win.location.href === mainChromeWindowURL;
        if (!isMainWindow && /^(about:(blank|newtab|home))/i.test(win.location.href)) return;
        console.log(`Load ${isFrame ? "frame" : "window"} ${win.location.href}`);
        this.injectInto(win, typeof win.gBrowser === "undefined");
        if (!isMainWindow) return;
        this.attachUIToWindow(win);
    }
}();
