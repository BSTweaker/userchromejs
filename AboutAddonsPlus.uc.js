// ==UserScript==
// @name           Show addon details in about:addons
// @include        about:addons
// ==/UserScript==

(function() {
    const lazy = {};
    ChromeUtils.defineESModuleGetters(lazy, {
        AddonManager: "resource://gre/modules/AddonManager.sys.mjs"
    });
    function show() {
        const addons = document.querySelectorAll(".addon-name");
        for (let addon of addons) {
            const addonId = addon.querySelector("a")?.href.slice("addons://detail/".length);
            if (addonId) lazy.AddonManager.getAddonByID(decodeURIComponent(addonId)).then(result => {
                const {version, updateDate} = result;
                const info = document.createElement("span");
                info.style.fontSize = "smaller";
                info.style.fontStyle = "italic";
                info.style.marginInlineEnd = "8px";
                info.textContent = `${version} - ${updateDate.toLocaleDateString("ja-JP")}`;
                addon.insertAdjacentElement("afterend", info);
            }).catch(err => console.error(addonId, err));
        }
    }
    document.addEventListener("view-loaded", show);
    if (!gViewController.isLoading) show();
}());
