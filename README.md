Waterfox 56からFirefoxに移行することをモチベーションに近代化したuserChrome.jsのスクリプト達。userChrome.jsのインストール方法は[ここ](http://wiki.nothing.sh/page/userChrome.js%CD%D1%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8)とかを見ましょう。

Fission対応・ESM化とインストール方法について
============================================

概要
----
Firefoxの[Fission](https://hacks.mozilla.org/2021/05/introducing-firefox-new-site-isolation-security-architecture/)化 (サイト毎のプロセス分離) の進行により、e10s時代に使われていた`loadFrameScript`でコンテンツウィンドウにスクリプトを流し込む方法は時代遅れになり、代わりに`JSWindowActor`を使った実装が必要になりました。そのため、ここにあるスクリプトもそれに従って修正が行われています。

`JSWindowActor`の定義にはECMAScriptモジュール (ESM) 形式 (Firefoxでは拡張子`.mjs`を使用)のソース上での実装が要求されるため、スクリプト本体である`.uc.js`ファイルからactorを定義する部分が`.mjs`ファイルに分離されています。この構成で作られたuserChrome.jsスクリプトを動作させる場合、`.uc.js`ファイルだけでなく分離された`.mjs`ファイルも適切に配置する必要があります。

actorの実装には当初Mozilla独自のJavaScriptコードモジュール (JSM) 形式 (拡張子`.jsm`) が利用されていましたが、Firefox 102の頃から徐々に標準化された[ESM形式への移行](https://spidermonkey.dev/areweesmifiedyet/)が始まりました。Firefox内部のESM化に伴いFirefox 136をもってJSMのサポートは[廃止](https://bugzilla.mozilla.org/show_bug.cgi?id=1866732)されたため、ここで公開されているスクリプトもJSMからESMへの移行が行われています。JSMではいわゆるdynamic exportが可能であり、変数`EXPORTED_SYMBOLS`を動的に変更することによって`.uc.js`ファイル内に直接actorを定義することが可能でしたが、ESMではサポートされていないため完全な分離が必要になりました。具体的には`DeepLTranslator.uc.js`はESM化に伴い`.mjs`ファイルの配置が必要になりました。

インストール方法
----------------
`.uc.js`ファイルはこれまでと同様にuserChrome.jsのローダが認識する場所に置きます。これに加えて、プロファイルディレクトリ内の`chrome`ディレクトリ直下に`jsm`という名前のディレクトリを作成し、その中に必要な`.mjs`ファイルを置きます。`.mjs`ファイルはリポジトリの[jsmディレクトリ](https://bitbucket.org/BSTweaker/userchromejs/src/esm/jsm/)以下にあります。`.uc.js`ファイルに対応する`...Parent.mjs`/`...Child.mjs`の2つのファイルをダウンロードしてください。

`.mjs`ファイルが正しく配置されていないと`.uc.js`が読み込まれた時に`DragNGoModoki: required .mjs files are not placed correctly!`のようなメッセージがコンソールに出力され、スクリプトは正しく動作しません。

なお、`chrome`ディレクトリ直下の`jsm`ディレクトリを使うというのはあくまでここで公開されているスクリプトの方法であり、他のスクリプトでも通用するユニバーサルな方法ではないので注意してください。

更新時の注意
------------
`.uc.js`と対応する`.mjs`ファイルの両方の更新を確認してください。一方が古いままだと不整合により動作しない可能性があります。

互換性
------
Firefox内蔵モジュールのESM化が十分に進んだFirefox 115以降での動作を想定しています。

ESM化前の古いスクリプトはリポジトリの[pre-esmブランチ](https://bitbucket.org/BSTweaker/userchromejs/src/pre-esm/)にあり、Firefox115より前のバージョンではこちらを使用できますが、基本メンテナンスはされないものと思ってください。

JSActors雑感
------------
Fission対応にあたり感じたことをメモしておきます。

### addMessageListenerの煩雑さから解放される
新しいメッセージが必要になる度に`addMessageListener`を追加する必要がない。`receiveMessage`だけを弄ればよい。

### 往復型の非同期通信が楽
親→子→親や子→親→子のようなリクエストに対してレスポンスが返るタイプの通信は双方で`sendAsyncMessage`する必要があったが (後者の場合`sendSyncMessage`という手もあるが) 、要求側が`sendAsyncMessage`の代わりに`sendQuery`を使い、応答側が`receiveMessage`で該当するメッセージに対して`Promise`を返すことにより、`sendQuery`で直接その`Promise`を受け取ることができる。

### スクリプトの読み込みがlazy
`window.messageManager.loadFrameScript`で指定したスクリプトはcontent windowが生成された瞬間に読み込まれるが、`ChromeUtils.registerWindowActor`で登録した`JSWindowActorChild`は親プロセスからメッセージが飛んできた時、あるいは登録時に指定したイベントがcontent windowで発生するまで読み込まれない。従来framescript内にあった親プロセスからメッセージを受け取るために`addMessageListener`するコード、あるいはイベントの発生を監視するために`addEventListener`するコードを用意する必要がない。適切なオプションを指定して`registerWindowActor`すれば良きに計らってくれる。

### スクリプトが読み込まれる対象を簡単に限定できる
`registerWindowActor`のオプションで読み込まれる対象を[URLのマッチパターン](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Match_patterns)の形式で指定可能。限定しないと`loadFrameScript`と同様にWebExtensionsなアドオンで生成されたUIパネル等にも読み込まれるので積極的に限定すべきである。

DragNgoModoki.uc.js
===================

派生元は[ここ](https://github.com/alice0775/userChrome.js/blob/master/DragNgoModoki_Fx40.uc.js)

ジェスチャ定義方法の変更について
--------------------------------
2022年11月23日の[コミット](https://bitbucket.org/BSTweaker/userchromejs/commits/80f31db3858692668c13a65a54436708750cc3a6)以降のバージョンでは、`.mjs`ファイルと同じ場所に`DNGGestureDefinitions.js`という名前のファイルを置くことで、その中の`GESTURES`という変数がジェスチャの定義として使用されます。スクリプト本体からジェスチャ定義を分離することで、スクリプトの更新時にジェスチャ定義を修正する手間がなくなります。[サンプルファイル](https://bitbucket.org/BSTweaker/userchromejs/src/master/jsm/DNGGestureDefinitions.js)をダウンロードして弄るのが良いです。なお従来どおり`DragNGoModokiChild.mjs`の中に直接定義することも可能で、その場合は`DNGGestureDefinitions.js`は無視されます。

ジェスチャの動作を定義する`cmd`関数のインターフェイスは次の通りです。
```javascript
function cmd(actor, event, info) {
  /*
   * actor: ドロップイベントが発生したcontent windowに対応するDragNGoModokiのJSWindowActorChild
   *        content windowはactor.contentWindowで参照できる
   * event: ドロップイベントのMouseEvent
   * info : Object { urls:[], texts:[], nodes:[], files:[], fname:[] }
   */
}
```
このインターフェイスはオリジナル版ともここで以前公開していたものとも異なります。そのためジェスチャ定義を既存のスクリプトからコピペしても動かないので、適宜変更を行ってください。

ジェスチャ定義には新たに`match`というプロパティをサポートしており、これを追加するとそのジェスチャは特定のURLにマッチした時のみ発火するようになります。例えば以下を通常の定義より前に置いておくとgoogle.com, google.co.jpでは上方向のジェスチャに対してはDuckDuckGo検索が行われるようになります。
```javascript
{dir:'U', modifier:'',name:'テキストを新しいタブでDuckDuckGo検索',obj:'text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['DuckDuckGo'], 'tab');},match:["*://*.google.com/*", "*://*.google.co.jp/*"]}
```

`match`には[マッチパターン](https://developer.mozilla.org/ja/docs/Mozilla/Add-ons/WebExtensions/Match_patterns)文字列の配列を指定してください。

`DNGGestureDefinitions.js`はsandbox内で解釈されるため、(`actor`経由で触れるものを除いて) 直接XPCOM等chrome権限のAPIに触ることはできませんが、`cmd`関数内の`this`は`DragNGoModokiChild.mjs`に定義されている`commandHelpers`というオブジェクトを参照しており、このオブジェクト内にヘルパ関数を定義することにより間接的にchrome権限のAPIを呼び出すことが可能です。

Firefox 94以降またはESR 91.3以降では、`config.RESET_GESTURE`に指定したジェスチャ (デフォルトはRDLU) を行った際に外部ジェスチャ定義ファイルの強制再読み込みを行います。`DNGGestureDefinitions.js`を編集後、Firefoxを起動したまま即座に変更を反映させることができます。これを行わない場合は、既にドラッグ操作を行ったことがあるブラウザが帰属しているプロセスに対しては再起動まで変更が反映されません。

注意
----
* SaveFolderModokiと連携する場合はここで公開されているものを使ってください
* SaveFolderModokiを使わないで直接リンクや画像を指定ディレクトリに保存することはできません
* `.xpi`のインストールはできません
* popupTranslateとの連携は動きません
* Fissionが有効な場合は[(おそらく)Firefox 93以降](https://bugzilla.mozilla.org/show_bug.cgi?id=1727176)でないとフレームを跨いだドラッグを正しくトラッキングできません


saveFolderModoki.uc.js
======================

派生元は[ここ](https://github.com/alice0775/userChrome.js/blob/master/saveFolderModoki40.uc.xul)

注意
----
オリジナルのスクリプトには画像を右クリック2回すると保存するという機能がありますが、現在のFirefoxはmacOS上では右クリック2回を検出できない感じなので、Windows以外 (動作を確認していないLinux等を含む) では中クリック2回で保存するように変更してあります。この挙動は[この部分](https://bitbucket.org/BSTweaker/userchromejs/src/master/jsm/SaveFolderModokiChild.mjs#lines-5)で変更できます。


UserScriptLoader.uc.js
======================

派生元は[ここ](https://github.com/Griever/userChromeJS/tree/master/UserScriptLoader)

長らく不完全な状態で公開していたけれど、割と気合を入れて近代化改修してみたよ

使い方
------
オリジナル版と同じです。プロファイルディレクトリの`chrome`ディレクトリ直下に`UserScriptLoader`ディレクトリを作成し、その中に使いたい`.user.js`ファイルを放り込んでください。

アドレスバーのアイコンを右クリックするとメニューを表示、中クリックすると`UserScriptLoader`ディレクトリ内のスクリプトを再読み込み (メニューからRebuildを選ぶのと同じ) します。左クリックした時の挙動はFirefoxのバージョンによって違い、91未満では従来どおり有効/無効の切り替え、91以降では現在のタブで読み込まれているスクリプト等の情報パネルが表示されます。このパネルからは各スクリプトにより登録されたコマンド (従来は右クリックメニュー内にあったもの) にアクセスできます。

スクリプトっぽいページを開いた場合、メニューから"Save and Install Script..."を選ぶと保存を (`UserScriptLoader`ディレクトリに保存した場合はインストールも) 行います。

主な変更点
----------
### @grant メタデータに対応
メタデータブロックに`@grant`が存在する場合、指定された`GM_*` APIのみアクセス可能になります。`@grant`が存在しない場合は従来どおり全ての`GM_*` APIにアクセス可能になります。`unsafeWindow`はgrantの有無によらず利用可能です。

`@grant none`が指定された場合、`GM_info`オブジェクトを除く`全てのGM_*` APIにアクセスできなくなりますが、`window`オブジェクト以下にあるWebページ側のコンテキストで作成されたプロパティへのアクセスが`unsafeWindow`を介さずに可能になります。なお、この場合でもスクリプトが隔離されたsandbox内で動いていることに変わりはなく、ページ側のコンテキストからユーザスクリプト側のコンテキストに触ることはできません。

### 以下の GM_\* APIに対応
- `GM_info`
- `GM_log`
- `GM_xmlhttpRequest`
- `GM_addStyle`
- `GM_addElement`
- `GM_getValue`
- `GM_setValue`
- `GM_listValues`
- `GM_deleteValue`
- `GM_addValueChangeListener`
- `GM_removeValueChangeListener`
- `GM_getResourceText`
- `GM_getResourceURL`
- `GM_openInTab`
- `GM_setClipboard`
- `GM_registerMenuCommand`
- `GM_unregisterMenuCommand`
- `GM_notification`

`GM_addStyle`はページのCSPをバイパスします。`GM_addElement`も`style`や`img`タグの追加に使う限りはバイパスしますが、`script`タグについては今のところは[Firefoxの仕様](https://bugzilla.mozilla.org/show_bug.cgi?id=1446231)により不可です。

### Greasemonkey 4系の GM.\* APIに対応
該当APIを`@grant`することで利用可能になります。`Promise`を返すやつにも対応しています。

### @run-at document-start なスクリプトの実行タイミングの調整
`@run-at`が`document-start`なスクリプトは従来`DOMWindowCreated`イベントが発火されてすぐに実行されていましたが、このタイミングでは`window.document`は存在しますが`document.documentElement`が存在するとは限りません。`document-start`で実行されるスクリプトの多くは`documentElement`の存在を前提にしている感じであるため、本バージョンでは`documentElement`が挿入されるまで実行を待ちます。

### @include, @match, @exlcude-match 等の対応を改善
`@include`に正規表現が指定されている場合に正しくマッチするようにしました。また、`@match`をネイティブの[`MatchPattern` API](https://searchfox.org/mozilla-central/source/dom/chrome-webidl/MatchPattern.webidl)で処理するようにし、あわせて`@exclude-match`にも対応しました。

その他
------
各スクリプトで`GM_setValue`された値と有効/無効のステートは`chrome`ディレクトリ下の`UserScriptLoader.json`に保存されます。それ以外の設定値は`about:config`の`UserScriptLoader.`以下に保存されます。

全てのスクリプトの実行を除外するURLは`about:config`の`UserScriptLoader.GLOBAL_EXCLUDE_MATCHES`にJSONとして解釈可能な[マッチパターン](https://developer.mozilla.org/ja/docs/Mozilla/Add-ons/WebExtensions/Match_patterns)文字列の配列を文字列値として指定します。例えば`google.com`と`mozilla.org`ドメインの全てのURLを除外したい場合、指定すべき値は`["*://*.google.com/*", "*://*.mozilla.org/*"]`のようになります。

メニューのスクリプト一覧のスクリプト名を右クリックすると、該当するスクリプトを`about:config`の`UserScriptLoader.EDITOR`で指定されたエディタで開きます。指定されていない場合は`view_source.editor.path`を見て、これも指定されていない場合はFirefoxのブラウザコンソールで開きます (スクラッチパッドの代わりだが使い勝手は良くない)。なおmacOSでは`.app`で終わるパッケージのパスを指定することで`/usr/bin/open -a`を介して開きます。

メニューの設定項目にある"Cache Script"は元々は無効にした場合はスクリプトを毎回ディスクから読む (修正時にRebuildが不要になる) という挙動でしたが、本バージョンでは有効時はコンテントプロセス側に一度実行したスクリプトをキャッシュする、無効時は毎回メインプロセスにスクリプトを取りに行くという挙動になります。Fission有効化に伴いコンテントプロセスは頻繁に生成消滅するため、正直キャッシュの意味はあまりないです。

このスクリプトの存在意義
------------------------
ユーザスクリプトを管理/実行するWebExtensionsなアドオンは複数存在しており、わざわざuserChrome.jsでこれらの機能を維持する必要性は少ないように感じる諸兄も少なくないと思います。自分がUserScriptLoader.uc.jsを維持しているのはユーザスクリプトから呼び出されて裏でchrome権限のコードが走る関数を好き勝手に追加できるという他人には理解してもらえそうにない理由が9割ぐらいなのですが、残りはManifest v3への移行に関して雲行きが怪しい点があるためです。

WebExtensionsなアドオンではページコンテンツにアクセス可能なsandbox環境を自由に生成することができないため、ページのCSPを回避しながらスクリプトを実行するにはアドオン自身の (ユーザスクリプトにとっては権限が不必要に高い) sandboxを共有する形でcontent scriptとしてユーザスクリプトを実行せざるを得ません。そのため、安全性 (WebExt APIへのアクセスの防止、windowオブジェクトを介したスクリプト間の干渉の阻止等) を確保するためにレガシーアドオン時代には必要のなかった冗長な実装が必要になります。なおTampermonkeyのようにwebRequest APIを用いてレスポンスのCSPヘッダ自体を改竄するデンジャラスな機能を備えているものもあります。

この問題への解決策としてFirefoxが独自に提供しているのが[`userScripts` API](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/userScripts)で、このAPIにより登録されたスクリプトはcontent scriptと同様にCSPの影響を受けず、スクリプト毎に生成される権限が制限されたsandbox内で実行される (UserScriptLoaderやレガシーアドオン時代のユーザスクリプト系アドオンとほぼ同じ) 仕組みになっています。このAPIを利用している代表的なアドオンが[FireMonkey](https://addons.mozilla.org/ja/firefox/addon/firemonkey/)で、Firefoxで使うなら個人的にはこれをオススメします。

しかしながら、Manifest v3の存在によりユーザスクリプト系アドオンの立場は微妙なものとなっています。Manifest v3では基本的なポリシーとしてリモートな (アドオンのパッケージ内に存在しない) JavaSciptコードの実行が禁止されており、これに伴い`tabs.executeScript`は廃止され、文字列化したJavaScriptコードを渡して実行することが不可能な[`scripting` API](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/scripting)が新たに導入されています。前述のFirefox独自の`userScripts` APIもMV3では[使うことができません](https://hg.mozilla.org/mozilla-central/rev/0b44605d1049#l9.11)。MozillaはMV3でユーザスクリプト系のアドオンが実装不可になることを認識しており、そのようなアドオンに対してはリモートコードの実行を可能にする専用の権限を与える方向で[動いている](https://bugzilla.mozilla.org/show_bug.cgi?id=1787179)ようですが、`userScripts` APIの代替APIがどうなるのかは今の所明らかになっていません。WebExtensionsでは安全性のためにアドオンの権限を制限したのに、逆に安全性のために払うべき実装コストが大きくなるという自己矛盾に陥っている感があり、このような状況においてはuserChrome.jsのようなchrome権限を利用して動く簡素な実装があってもいいのでは、と思っています。

2022/11/13追記: MV3におけるユーザスクリプト向けAPIについてGoogleの中の人から[草案](https://docs.google.com/document/d/1ekeZ1hz6B1djnTpAjskd7_WAjWi7DM1TuAjWws1hyG4)の提示があり、Mozillaの中の人が上記問題 (MV2 APIの欠点・`userScripts` APIの利点・新 APIに求められる要素等) を[整理](https://github.com/w3c/webextensions/issues/279#issuecomment-1309614401)しています。これらを見る感じ事態は正しい方向に向かいつつあるようです。

DeepLTranslator.uc.js
=====================

Firefoxのコンテクストメニューに選択テキストをDeepLで翻訳するポップアップを表示する機能を追加します。

注意
----
* **スクリプトを動かすにはDeepLのAPIキーの入手を推奨します。**無料ですがDeepLのユーザ登録が必要です。入手したらスクリプト内の該当する場所にキーを入力してください。
* 無料版のキーは一ヶ月あたり50万文字が上限なのでヘビーな使い方には向かない可能性が高いです (上限までの数字はポップアップのツールチップで確認可)。
* APIキーの値を空(`""`)にすると邪悪な方法での動作を試みますが、Too many requests等のエラーが出やすくIPアドレス単位で一定時間BANされるリスクもあるので使用はおすすめしません。
* このスクリプトはかつて`.uc.js`単体で動作していましたが、現在は他のスクリプトと同じく`.mjs`の配置が必要です。

Tips
----
### ホットキー
デフォルトでは無効になっていますが、スクリプト内でホットキーを定義できます。定義部分は以下のようになります。
```javascript
const hotkey = {
    enabled: true,           // 有効/無効
    code:    "ControlLeft",  // KeyboardEvent.codeに相当するもの
    repeat:  2,              // 連続して何回キーを押すと発動するか
    timeout: 500,            // 指定したミリ秒以内を連続とみなす
};
```
この定義では「(テキストを選択して) 左コントロールキーを500ミリ秒以内に2連打するとポップアップが開く」になります。`hotkey.code`に指定できる値の一覧は[ここ](https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/code/code_values)にあり、実際にキーを押して調べるには[ここ](https://developer.mozilla.org/docs/Web/API/KeyboardEvent/code#try_it_out)とかが使えます。

### API
このスクリプトの利点は他のuserChrome.jsスクリプトやchrome権限で動く拡張 (FireGestures Quantum等) と連携できる点にあります。

スクリプトが読み込まれるとブラウザウィンドウには`DLTranslator`という名前のwindow actorが登録され、親プロセス側からは
```javascript
let actor = window.gBrowser.selectedBrowser.browsingContext.currentWindowGlobal.getActor("DLTranslator");
actor.sendAsyncMessage("DLT:CreatePopup", {
    sourceText: "Text to translate", /* 翻訳したいテキスト */
    fromLang: null,                  /* 翻訳元の言語 nullにすれば自動 */
    toLang: "JA",                    /* 翻訳先の言語 nullにすれば.uc.js内の既定値もしくはページで最後に指定した言語を使用 */
    screenX: 100,                    /* ポップアップを表示する座標 MouseEvent.screenXに相当するもの */
    screenY: 100,                    /* ポップアップを表示する座標 MouseEvent.screenYに相当するもの */
});
```
コンテンツプロセス側からは
```javascript
let sourceText = "Text to translate";  /* 翻訳したいテキスト */
let fromLang = null;                   /* 翻訳元の言語 nullにすれば自動*/
let toLang = "JA";                     /* 翻訳先の言語 nullにすれば.uc.js内の既定値もしくはページで最後に指定した言語を使用 */
let clientX = 100;                     /* ポップアップを表示する座標 MouseEvent.clientXに相当するもの */
let clientY = 100;                     /* ポップアップを表示する座標 MouseEvent.clientYに相当するもの */
let actor = contentWindow.windowGlobalChild.getActor("DLTranslator");
actor.createPopupWithClientCoordinate(clientX, clientY, sourceText).translate(fromLang, toLang);
```
という手続きでポップアップを呼び出すことができます。ジェスチャ系のスクリプト/拡張と組み合わせると強力で、テキストを選択→マウスを振る→ポップアップ表示ができるようになり捗ります。

#### 例1: FireGestures Quantumのジェスチャ用スクリプト (content環境向け)
```javascript
let text = FireGestures.getSelectedText();
if (text) {
    let actor = window.windowGlobalChild.getActor("DLTranslator");
    let {clientX, clientY} = FireGestures;
    actor.createPopupWithClientCoordinate(clientX, clientY, text).translate(null, null);
}
```

#### 例2: DragNGoModokiのジェスチャ定義
```javascript
{dir:'UD', modifier:'',name:'テキストをDeepLで翻訳',obj:'text',cmd:function(_actor,event,info) {
    let actor = _actor.contentWindow.windowGlobalChild.getActor("DLTranslator");
    actor.createPopupWithClientCoordinate(event.clientX, event.clientY, info.texts[0]).translate(null, null);
}},
```

#### 例3: MouseGestures2_e10s.uc.jsのジェスチャ定義
```javascript
['DR', '選択テキストをDeepLで翻訳', function() {
    let text = ucjsMouseGestures._selectedTXT;
    if (!text) return;
    let actor = gBrowser.selectedBrowser.browsingContext.currentWindowGlobal.getActor("DLTranslator");
    actor.sendAsyncMessage("DLT:CreatePopup", {
        sourceText: ucjsMouseGestures._selectedTXT,
        fromLang: null,
        toLang: null,
        screenX: ucjsMouseGestures._lastX,
        screenY: ucjsMouseGestures._lastY,
    });
}],
```

AboutAddonsPlus.uc.js
=====================

`about:addons`に表示されるアドオン等にバージョンと更新日時を追記するだけのもの。かつて某所に投下したものが1ファイル完結型のactorだったためFirefox 136をもって機能しなくなったので、よく考えたらこれactorである必要がないということで代わりに。
