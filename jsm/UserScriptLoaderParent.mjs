const lazy = {};
ChromeUtils.defineESModuleGetters(lazy, {
	NetUtil: "resource://gre/modules/NetUtil.sys.mjs",
	FileUtils: "resource://gre/modules/FileUtils.sys.mjs",
	BrowserUtils: "resource://gre/modules/BrowserUtils.sys.mjs",
	PanelMultiView: "resource:///modules/PanelMultiView.sys.mjs",
	require: "resource://devtools/shared/loader/Loader.sys.mjs"
});
const appLocaleShort = Services.locale.appLocaleAsBCP47.split("-")[0];
const appLocaleLong = appLocaleShort + "-" + Services.locale.appLocaleAsBCP47.split("-")[1];

function $(id, win) {return win.document.getElementById(id);};
function $C(name, attr, win) {
	const el = win.document.createXULElement(name);
	if (attr) Object.keys(attr).forEach(function(n) {el.setAttribute(n, attr[n]);});
	return el;
}

const css = '\
/* http://www.famfamfam.com/lab/icons/silk/preview.php */\
#UserScriptLoader-icon {\
	list-style-image: url(data:image/png;base64,\
		iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACOElEQVQ4ja3Q3UtTcRgH8N8f4K11\
		FaRrVGumlTXndPYiyQqkCyPoLroOCbyJSCGJUhOGUSnShVqtFpYlW/lCKiPmy5zinObZdJtn29nZ\
		cW7nnB39TapvF+WdI4W+95/n+zwPIf8zwnRFt+AyIj5VDn7CAN5ZiphDD25Mh+jIaUSGixEePAnW\
		XhTaeYCr/OdWogMZoR2Z2DPQyBNsrpqxEWiF4muG4LwK9nOhvCOOT5Y1iks3sSV0IP29CrLnAkS3\
		EalxPRR/CxJTN8Dai35kXZ+fNGQyfBs2Q7chz1dCcp9FasIAxd+E5GwtwoNl8H3QqnZuHy+tSc5f\
		RybejvTCRUiz55CaKoPsvQV5sR7ciAnBvoJLWdtjTn1aCTWARlshz52HOG1E0lkCxd+C+LdrCH7S\
		1mXHjhLd2nQ1MvxzyF4TxJlKpCYrsD6mQ3rpEUL92l+BPg1d6T1Kl98dpr43asq8OkSZ7nyeEEII\
		59DzElMHGm3DJmvGRvAxFH8TFF8T0osPIXkaIc7UI+W6i+TEHbD9VWC68hRPx4E//+BGz6QiX4tp\
		eOgUZQdO0FV7IQ3ZCqi8+ACC7TjWhkwQ3Q2IfrmCZcsxMF0HX2Q9ZzuBj9rRdVctpLn7EN33ELaZ\
		wPSoRE/nvv3/xIQQEnivgeRpBDdcg5W3BWB68s27gn/xDDdUjejAZfheqxOezrzdtRJCiNeamxPo\
		1WLFqgHzUtW8a7idZesRr9+i5r1Pc3P2jAkhhLGodXs1vwEkf3FKAtNVEwAAAABJRU5ErkJggg==\
		);\
}\
\
#UserScriptLoader-icon[state="disable"] {\
	list-style-image: url(data:image/png;base64,\
		iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACrElEQVQ4ja2QXUhTARzFb3f55kuB\
		2EOBplLJTCtrzs/pJNHEJ03orXyIHkQkFSvSSKTmB5hRKfWSVJZhWX5MvZIuiemc05zT3Obm3Ny8\
		m7rP6+7MdnoIQWF76zwe+J3z/x+C+J+yTWd02OTpsE6lgZ5MAS1Nxvo4HxYJD+bRi1gbSYRp+DyM\
		AwmGwAHytD87m+3w2drgW38Odu0pvKst2NY3g9E0wCYtglEc7w4IW2Wpdc6lEuzY2uH5lQO3Ugin\
		Ih2OCT4YbSM2p67DOJCwG/R8Wpbi89Gt8BrK4Z7PhkshgGMyBYxWBPtsGUzDqdB85kYFbp9ILrTP\
		X4PP2gbPwmW4ZjPhmEqFW1UK92INLKO5WOmJywvavi7lexhDLVhzM9xzWXBOp8MuTQKjbYT1RzFW\
		vnIrgsPjSbyN6QL46Bdwq3LhnMmGQ5aBLQkPnqXHMPRy/fqeWFbXfYZd/niK1byPYdVvo1l1x0ma\
		IAiCsIzzaZe6Aqy5FV5jC7ZXmsBoRWA0IngWH8GlrINzpgYO+T3YJ+/A2JsD9etIRtl+4t8elrFL\
		jrVviayJusAah86xqwPxrKE/jnUv1sPWfxYbVC6cilosNCThe/FRUJmHMZhNroqzyeqgb+m/cMe2\
		5GVwzT2EU3EfKlEift7mwdvXBP+CGExnOWS3uLtDWWRp4IBPsXAp62AZKYTuQxyovBDHdl8T8CQf\
		qDoC1EfAJsrAoJDUBYJnLFQBzENXoHkXs6l8GRlOCTh+/3Q39steEw5KwPEfgFVdYaH6bi50XbFQ\
		v4lq2PPFQtLoeXUDqAkHW0lgq5KA4SYHYiFpOhCw3HVape2MoVXPwkL3+5Krxx5MlET/NldFwFod\
		guWSQ6DyObsDQvLugQB1Zwwv2LCSouPVYiGppwQcv1hIGvfgv6X5zFaYeSAgAAAAAElFTkSuQmCC\
		);\
}\
#UserScriptLoader-panel-multiView {\
	font: menu;\
}\
#UserScriptLoader-panel-mainView {\
	min-width: 250px;\
}\
.subviewbutton.USL-command > .toolbarbutton-icon {\
	margin-inline-start: 16px !important;\
}\
.USL-script-desc {\
	padding-inline: calc(var(--arrowpanel-menuitem-padding-inline) + 16px);\
}\
'.replace(/[\r\n\t]/g, '');

class PrefManager {
	constructor(str) {
		let root = 'UserScriptLoader.';
		if (str)
			root += str;
		this.branch = Services.prefs.getBranch(root)
	}
	setValue(name, value) {
		try {
			switch(typeof value) {
				case 'string' : this.branch.setStringPref(name, value); break;
				case 'number' : this.branch.setIntPref(name, value); break;
				case 'boolean': this.branch.setBoolPref(name, value); break;
			}
		} catch(e) { }
	}
	getValue(name, defaultValue){
		let value = defaultValue;
		try {
			switch(this.branch.getPrefType(name)) {
				case Ci.nsIPrefBranch.PREF_STRING: value = this.branch.getStringPref(name); break;
				case Ci.nsIPrefBranch.PREF_INT   : value = this.branch.getIntPref(name); break;
				case Ci.nsIPrefBranch.PREF_BOOL  : value = this.branch.getBoolPref(name); break;
			}
		} catch(e) { }
		return value;
	}
}

class ResourceManager {
	constructor(requiresFolder) {
		this.requiresFolder = requiresFolder;
		this.resources = new Map();
	}
	getResourceFor(rawURL) {
		const fixedURL = new URL(rawURL);
		const isLocalFile = fixedURL.protocol === "file:";
		const url = isLocalFile ? fixedURL.href : fixedURL.origin + fixedURL.pathname;
		let promise = this.resources.get(url);
		if (promise) return promise;
		let aFile;
		if (isLocalFile) {
			aFile = Services.io.getProtocolHandler("file").QueryInterface(Ci.nsIFileProtocolHandler).getFileFromURLSpec(fixedURL.href);
		}
		else {
			aFile = this.requiresFolder.clone();
			aFile.QueryInterface(Ci.nsIFile);
			aFile.append(encodeURIComponent(url));
		}
		if (isLocalFile || (aFile.exists() && aFile.isFile())) {
			promise = new Promise((resolve, reject) => {
				const channel = lazy.NetUtil.newChannel({
					uri: Services.io.newFileURI(aFile),
					loadUsingSystemPrincipal: true
				});
				if (channel.URI.scheme !== 'file') {
					reject(new Error('getLocalFileContents is "file" only'));
					return;
				}
				const stream = channel.open();
				let text = null, bytes = null;
				const isText = lazy.BrowserUtils.mimeTypeIsTextBased(channel.contentType ?? "");
				if (isText) {
					text = lazy.NetUtil.readInputStreamToString(stream, stream.available());
				} else {
					const binaryStream = Cc['@mozilla.org/binaryinputstream;1'].createInstance(Ci.nsIBinaryInputStream);
					binaryStream.setInputStream(stream);
					bytes = new Uint8Array(binaryStream.readByteArray(stream.available()));
					binaryStream.close();
				}
				stream.close();
				resolve({
					text,
					bytes,
					contentType: channel.contentType,
				});
			});
			this.resources.set(url, promise);
		}
		else {
			promise = new Promise((resolve, reject) => {
				const channel = lazy.NetUtil.newChannel({
					uri: lazy.NetUtil.newURI(url),
					loadUsingSystemPrincipal: true,
					contentPolicyType: Ci.nsIContentPolicy.TYPE_OTHER,
					securityFlags: Ci.nsILoadInfo.SEC_REQUIRE_CORS_INHERITS_SEC_CONTEXT,
				});
				channel.QueryInterface(Ci.nsIHttpChannel);
				channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS;
				channel.requestMethod = "GET";
				lazy.NetUtil.asyncFetch(channel, (stream, status, channel) => {
					if (!Components.isSuccessCode(status)) {
						let e = Components.Exception("", status);
						reject(new Error(`Error while loading ${url}: ${e.name}`));
						return;
					}
					channel.QueryInterface(Ci.nsIHttpChannel);
					if (!channel.requestSucceeded) {
						reject(new Error(`Server returned ${channel.responseStatus} / ${channel.responseStatusText
}: ${url}`));
						return;
					}
					let text = null, bytes = null;
					const isText = lazy.BrowserUtils.mimeTypeIsTextBased(channel.contentType ?? "");
					if (isText) {
						const charset = channel.contentCharset || "UTF-8";
						try {
							text = lazy.NetUtil.readInputStreamToString(stream, stream.available(), {charset});
							UserScriptLoader.saveText(aFile, text);
						} catch (e) {
							reject(new Error(`Error while reading data as text from ${url} (possibly invalid character encoding; ${charset} is used): ${e.message}`));
							return;
						}
					} else {
						const binaryStream = Cc['@mozilla.org/binaryinputstream;1'].createInstance(Ci.nsIBinaryInputStream);
						binaryStream.setInputStream(stream);
						bytes = new Uint8Array(binaryStream.readByteArray(stream.available()));
						binaryStream.close();
						UserScriptLoader.saveFile(aFile, bytes);
					}
					stream.close();
					resolve({
						text,
						bytes,
						contentType: channel.contentType,
					});
				});
			});
			this.resources.set(url, promise);
		}
		return promise;
	}
}

class ScriptEntry {
	constructor(aFile, resourceManager, database, deferred=false) {
		this.includeRegExp = null;
		this.excludeRegExp = null;
		this.includeMatchPatternSet = null;
		this.excludeMatchPatternSet = null;
		this.includeMatchPatternStrings = null; // for child actor
		this.excludeMatchPatternStrings = null; // for child actor
		this.file = aFile;
		this.resourceManager = resourceManager;
		this.leafName = aFile.leafName;
		this.path = aFile.path;
		this.lastModifiedTime = aFile.lastModifiedTime;
		this.code = UserScriptLoader.loadText(aFile);
		this.getMetadata();
		this._disabled = false;
		this.requireSrc = "";
		this.resources = {};
		this.run_at = "run-at" in this.metadata ? this.metadata["run-at"][0] : "document-end";
		this.name = "name" in this.metadata ? this.metadata.name[0] : this.leafName;
		this.prefName = (this.metadata.namespace || 'nonamespace/') + '/' + this.name;
		this.uuid = database.uuid[this.prefName] || ScriptEntry.generateUUID();
		database.uuid[this.prefName] = this.uuid;
		this.prefStorage = database.pref[this.uuid] || {};
		database.pref[this.uuid] = this.prefStorage;
		this.localizedName = this.metadata[`name:${appLocaleLong}`] || this.metadata[`name:${appLocaleShort}`] || this.name;

		if (this.metadata.delay) {
			let delay = parseInt(this.metadata.delay[0], 10);
			this.delay = isNaN(delay) ? 0 : Math.max(delay, 0);
		} else if (this.run_at === "document-idle") {
			this.delay = 0;
		}

		if (this.metadata.match) {
			try {
				this.includeMatchPatternSet = new MatchPatternSet(this.metadata.match);
				this.includeTLD = this.isTLD(this.metadata.match);
				this.includeMatchPatternStrings = this.metadata.match;
			} catch (e) {}
		} else if (this.metadata.include) {
			try {
				this.includeRegExp = new RegExp(this.createRegExp(this.metadata.include));
				this.includeTLD = this.isTLD(this.metadata.include);
				this.includeMatchPatternSet = null;
			} catch (e) {}
		}
		if (!this.includeMatchPatternSet && !this.includeRegExp) {
			try {
				this.includeMatchPatternSet = new MatchPatternSet(["*://*/*"]);
				this.includeTLD = false;
				this.includeMatchPatternStrings = ["*://*/*"];
			} catch (e) {}
		}
		if (this.metadata["exclude-match"]) {
			try {
				this.excludeMatchPatternSet = new MatchPatternSet(this.metadata["exclude-match"]);
				this.excludeTLD = this.isTLD(this.metadata["exclude-match"]);
				this.excludeMatchPatternStrings = this.metadata["exclude-match"];
			} catch (e) {}
		} else if (this.metadata.unmatch) {
			try {
				this.excludeMatchPatternSet = new MatchPatternSet(this.metadata.unmatch);
				this.excludeTLD = this.isTLD(this.metadata.unmatch);
				this.excludeMatchPatternStrings = this.metadata.unmatch;
			} catch (e) {}
		} else if (this.metadata.exclude) {
			try {
				this.excludeRegExp = new RegExp(this.createRegExp(this.metadata.exclude));
				this.excludeTLD = this.isTLD(this.metadata.exclude);
			} catch (e) {}
		}

		if (this.metadata.resource) {
			for (const r of this.metadata.resource) {
				let res = r.split(/\s+/);
				if (!res[1]?.startsWith("http")) continue;
				this.resources[res[0]] = { url: res[1] };
			}
		}

		new Promise(resolve => {
			if (deferred) this._deferredResolver = resolve;
			else resolve();
		}).then(() => {
			this.requirePromise = this.getRequire();
			this.resourcePromise = this.getResource();
		});
	}
	get isReady() {
		return Promise.all([this.requirePromise, this.resourcePromise]).then(() => this);
	}
	get homepageURL() {
		let homepageURL;
		if (this.metadata?.homepageurl) {
			homepageURL = this.metadata.homepageurl[0];
		} else if (this.metadata?.downloadurl) {
			homepageURL = this.metadata.downloadurl[0];
		} else if (this.metadata?.updateurl) {
			homepageURL = this.metadata.updateurl[0];
		}
		return homepageURL;
	}
	get disabled() {
		return this._disabled;
	}
	set disabled(value) {
		this._disabled = value;
		if (!value) {
			if (this._deferredResolver) {
				this._deferredResolver();
				this._deferredResolver = null;
			}
		}
	}
	getMetadata() {
		this.metadata = {};
		let m = this.code.match(/\/\/\s*==UserScript==[\s\S]+?\/\/\s*==\/UserScript==/);
		if (!m)
			return;
		m = (m+'').split(/[\r\n]+/);
		for (let i = 0; i < m.length; i++) {
			if (!/\/\/\s*?@(\S+)($|\s+([^\r\n]+))/.test(m[i]))
				continue;
			let name  = RegExp.$1.toLowerCase().trim();
			let value = RegExp.$3;
			if (this.metadata[name]) {
				this.metadata[name].push(value);
			} else {
				this.metadata[name] = [value];
			}
		}
	}
	createRegExp(urlarray, isMatch) {
		let regstr = urlarray.map(function(url) {
			if (url.startsWith("/") && url.endsWith("/")) {
				return url.slice(1, -1);
			}
			url = url.replace(/([()[\]{}|+.,^$?\\])/g, "\\$1");
			if (isMatch) {
				url = url.replace(/\*+|:\/\/\*\\\./g, function(str, index, full){
					if (str === "\\^") return "(?:^|$|\\b)";
					if (str === "://*\\.") return "://(?:[^/]+\\.)?";
					if (str[0] === "*" && index === 0) return "(?:https?|ftp|file)";
					if (str[0] === "*") return ".*";
					return str;
				});
			} else {
				url = url.replace(/\*+/g, ".*");
				url = url.replace(/^\.\*\:?\/\//, "https?://");
				url = url.replace(/^\.\*/, "https?:.*");
			}
			//url = url.replace(/^([^:]*?:\/\/[^\/\*]+)\.tld\b/,"$1\.(?:com|net|org|info|(?:(?:co|ne|or)\\.)?jp)");
			//url = url.replace(/\.tld\//,"\.(?:com|net|org|info|(?:(?:co|ne|or)\\.)?jp)/");
			return "^" + url + "$";
		}).join('|');
		return regstr;
	}
	isTLD(urlarray) {
		return urlarray.some(function(url) {return /^.+?:\/{2,3}?[^\/]+\.tld\b/.test(url);});
	}
	makeTLDURL(aURL) {
		try {
			const uri = Services.io.newURI(aURL, null, null);
			uri.host = uri.host.slice(0, -Services.eTLD.getPublicSuffix(uri).length) + "tld";
			return uri.spec;
		} catch (e) {}
		return "";
	}
	isURLMatching(url) {
		try {
			if (this.disabled) return false;
			if (this.excludeMatchPatternSet?.matches(url)) return false;
			if (this.excludeRegExp?.test(url)) return false;
			
			const tldurl = (this.excludeTLD || this.includeTLD) ? this.makeTLDURL(url) : "";
			if (this.excludeTLD && tldurl) {
				if (this.excludeMatchPatternSet?.matches(tldurl)) return false;
				if (this.excludeRegExp?.test(tldurl)) return false;
			}
			if (this.includeMatchPatternSet?.matches(url)) return true;
			if (this.includeRegExp?.test(url)) return true;
			if (this.includeTLD && tldurl) {
				if (this.includeMatchPatternSet?.matches(tldurl)) return true;
				if (this.includeRegExp?.test(tldurl)) return true;
			}
		} catch (e) {}
		return false;
	}
	getResource() {
		if (!this.metadata.resource) return Promise.resolve();
		const promises = [];
		for (let name in this.resources) {
			const obj = this.resources[name];
			const url = obj.url;
			promises.push(new Promise(resolve => {
				this.resourceManager.getResourceFor(url).then(ret => {
					obj.text = ret.text;
					obj.bytes = ret.bytes;
					obj.contentType = ret.contentType;
					resolve();
				}).catch(e => {
					console.error(e);
					resolve();
				});
			}));
		}
		return Promise.all(promises);
	}
	getRequire() {
		if (!this.metadata.require) return Promise.resolve();
		const promises = [];
		for (const url of this.metadata.require) {
			promises.push(new Promise(resolve => {
				this.resourceManager.getResourceFor(url).then(ret => {
					if (ret.text) {
						this.requireSrc += ret.text + ";\r\n";
					}
					resolve();
				}).catch(e => {
					console.error(e);
					resolve();
				});
			}));
		}
		return Promise.all(promises);
	}
	setPref(name, value) {
		const oldValue = this.prefStorage[name];
		this.prefStorage[name] = value;
		return oldValue;
	}
	getPref(name, defaultValue) {
		return this.prefStorage[name] ?? defaultValue;
	}
	deletePref(name) {
		const oldValue = this.prefStorage[name];
		delete this.prefStorage[name];
		return oldValue;
	}
	listPref() {
		return Object.keys(this.prefStorage);
	}
	static generateUUID() {
		const { generateUUID } = Services.uuid;
		return generateUUID().toString();
	}
}

class UserScriptLoader {
	constructor() {
		this.database = { pref: {}, uuid: {}, disabledScripts: [] };
		this.readScripts = [];
		this.pref = new PrefManager();
		this.resourceManager = new ResourceManager(this.REQUIRES_FOLDER);
		this._disabled = this.pref.getValue('disabled', false);
		this._debug = this.pref.getValue('DEBUG', false);
		this._hideExclude = this.pref.getValue('HIDE_EXCLUDE', false);
		this._cacheScript = this.pref.getValue('CACHE_SCRIPT', true);
		this.globalExcludes = null;
		this.loadSetting();
		this.rebuild(true);
		this.migratePrefs();
		this.pref.branch.addObserver("", this, true);
		/* Since we require synchronous communication between child and parent process in some GM_* APIs
		   and JSWindowActor does not allow this, a legacy message manager is used here */
		Services.ppmm.addMessageListener("USL:GetScriptPref", this);
		Services.ppmm.addMessageListener("USL:ListScriptPref", this);
	}
	get SCRIPTS_FOLDER() {
		if (!this._scriptsFolder) {
			const folderPath = this.pref.getValue('SCRIPTS_FOLDER', "");
			let aFolder;
			if (!folderPath) {
				aFolder = Services.dirsvc.get("UChrm", Ci.nsIFile);
				aFolder.append("UserScriptLoader");
			} else {
				aFolder = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsIFile);
				aFolder.initWithPath(folderPath);
			}
			if (!aFolder.exists() || !aFolder.isDirectory()) {
				aFolder.create(Ci.nsIFile.DIRECTORY_TYPE, 0o755);
			}
			this._scriptsFolder = aFolder;
		}
		return this._scriptsFolder;
	}
	get REQUIRES_FOLDER() {
		if (!this._requiresFolder) {
			let aFolder = this.SCRIPTS_FOLDER.clone();
			aFolder.QueryInterface(Ci.nsIFile);
			aFolder.append('require');
			if (!aFolder.exists() || !aFolder.isDirectory()) {
				aFolder.create(Ci.nsIFile.DIRECTORY_TYPE, 0o755);
			}
			this._requiresFolder = aFolder;
		}
		return this._requiresFolder;
	}
	get EDITOR() {
		return this.pref.getValue('EDITOR', "") || Services.prefs.getStringPref("view_source.editor.path");
	}
	get disabledScripts() {
		if (this._disabledScripts) return this._disabledScripts;
		const disabledScripts = new Set(this.database.disabledScripts);
		this._disabledScripts = disabledScripts;
		return disabledScripts;
	}
	get disabled() {
		return this._disabled;
	}
	set disabled(bool){
		this._disabled = !!bool;
		for (let win of Services.wm.getEnumerator("navigator:browser")) {
			let elem = $("UserScriptLoader-icon", win);
			if (!elem) continue;
			if (bool) {
				elem.setAttribute("state", "disable");
				$("UserScriptLoader-panel-toggleButton", win).label = "Enable UserScriptLoader";
				$("UserScriptLoader-panel-toggleButton", win).image = "chrome://browser/skin/trending.svg";
			} else {
				elem.setAttribute("state", "enable");
				$("UserScriptLoader-panel-toggleButton", win).label = "Disable UserScriptLoader";
				$("UserScriptLoader-panel-toggleButton", win).image = "chrome://browser/skin/addons/addon-install-blocked.svg";
			}
		}
		this.pref.setValue("disabled", !!bool);
	}
	get DEBUG() {
		return this._debug;
	}
	set DEBUG(bool) {
		this._debug = !!bool;
		for (let win of Services.wm.getEnumerator("navigator:browser")) {
			let elem = $("UserScriptLoader-debug-mode", win);
			if (elem) elem.setAttribute("checked", this._debug);
		}
		this.pref.setValue("DEBUG", !!bool);
	}
	get HIDE_EXCLUDE() {
		return this._hideExclude;
	}
	set HIDE_EXCLUDE(bool) {
		this._hideExclude = !!bool;
		for (let win of Services.wm.getEnumerator("navigator:browser")) {
			let elem = $("UserScriptLoader-hide-exclude", win);
			if (elem) elem.setAttribute("checked", this._hideExclude);
		}
		this.pref.setValue("HIDE_EXCLUDE", !!bool);
	}
	get CACHE_SCRIPT() {
		return this._cacheScript;
	}
	set CACHE_SCRIPT(bool) {
		this._cacheScript = !!bool;
		for (let win of Services.wm.getEnumerator("navigator:browser")) {
			let elem = $("UserScriptLoader-cache-script", win);
			if (elem) elem.setAttribute("checked", this._cacheScript);
		}
		if (!bool) {
			this.broadcastMessage("USL:InvalidateCache");
		}
		this.pref.setValue("CACHE_SCRIPT", !!bool);
	}
	handleEvent(event) {
		switch(event.type) {
			case "unload":
				if (Services.wm.getEnumerator("navigator:browser").hasMoreElements()) break;
				this.saveSetting();
				break;
		}
	}
	receiveMessage(message) {
		switch (message.name) {
			case "USL:GetScriptPref": {
				const script = this.getScriptForUuid(message.data.uuid);
				return script?.getPref(message.data.name, message.data.defaultValue) ?? message.data.defaultValue;
			}
			case "USL:ListScriptPref": {
				const script = this.getScriptForUuid(message.data.uuid);
				return script?.listPref(message.data.name, message.data.defaultValue) ?? [];
			}
		}
	}
	observe(subject, topic, data) {
		if (topic === "nsPref:changed") {
			switch (data) {
				case "GLOBAL_EXCLUDE_MATCHES": {
					const patterns = this.pref.getValue("GLOBAL_EXCLUDE_MATCHES", "");
					if (patterns) {
						try {
							this.globalExcludes = new MatchPatternSet(JSON.parse(patterns));
							this.broadcastMessage("USL:InvalidateCache");
						} catch (e) {
							this.globalExcludes = null;
							console.error(e);
						}
					}
					else this.globalExcludes = null;
					this.debug("Global exclude patterns updated");
					break;
				}
				default:
			}
		}
	}
	init(win) {
		win.windowUtils.loadSheetUsingURIString(`data:text/css;utf-8,${encodeURIComponent(css)}`, Ci.nsIDOMWindowUtils.USER_SHEET);

		let iconArea = $('urlbar-icons', win) || $('page-action-buttons', win);
		iconArea = iconArea.appendChild($C("hbox", {
			class: "urlbar-page-action",
			context: "UserScriptLoader-popup",
		}, win));
		iconArea.addEventListener("click", event => this.iconClick(event));
		iconArea.appendChild($C("image", {
			id: "UserScriptLoader-icon",
			class: "urlbar-icon",
		}, win));

		$('mainPopupSet', win).appendChild(win.MozXULElement.parseXULToFragment(`
			<menupopup id="UserScriptLoader-popup">
				<menuseparator id="UserScriptLoader-menuseparator"/>
				<menu label="User Script Command"
				      id="UserScriptLoader-register-menu"
				      accesskey="C">
					<menupopup id="UserScriptLoader-register-popup"/>
				</menu>
				<menuitem label="Save and Install Script..."
				          id="UserScriptLoader-saveMenu"
				          accesskey="S"/>
				<menu label="Menu" id="UserScriptLoader-submenu">
					<menupopup id="UserScriptLoader-submenu-popup">
						<menuitem label="Delete Prefs Storage..."
						          id="UserScriptLoader-delete-pref"/>
						<menuseparator/>
						<menuitem label="Hide Inactive Scripts"
						          id="UserScriptLoader-hide-exclude"
						          accesskey="N"
						          type="checkbox"
						          checked="${this.HIDE_EXCLUDE}"/>
						<menuitem label="Open Scripts Folder"
						          id="UserScriptLoader-openFolderMenu"
						          accesskey="O"/>
						<menuitem label="Rebuild"
						          id="UserScriptLoader-rebuild"
						          accesskey="R"/>
						<menuitem label="Cache Script"
						          id="UserScriptLoader-cache-script"
						          accesskey="C"
						          type="checkbox"
						          checked="${this.CACHE_SCRIPT}"/>
						<menuitem label="DEBUG MODE"
						          id="UserScriptLoader-debug-mode"
						          accesskey="D"
						          type="checkbox"
						          checked="${this.DEBUG}"/>
					</menupopup>
				</menu>
			</menupopup>
		`));
		$("UserScriptLoader-popup", win).addEventListener("popupshowing", event => this.onPopupShowing(event));
		$("UserScriptLoader-popup", win).addEventListener("popuphidden", event => this.onPopupHidden(event));
		$("UserScriptLoader-saveMenu", win).addEventListener("command", event => this.saveScript(event));
		$("UserScriptLoader-delete-pref", win).addEventListener("command", event => this.deleteStorage(event, "pref"));
		$("UserScriptLoader-hide-exclude", win).addEventListener("command", event => {this.HIDE_EXCLUDE = !this.HIDE_EXCLUDE;});
		$("UserScriptLoader-openFolderMenu", win).addEventListener("command", event => this.openFolder());
		$("UserScriptLoader-rebuild", win).addEventListener("command", event => this.rebuild());
		$("UserScriptLoader-cache-script", win).addEventListener("command", event => {this.CACHE_SCRIPT = !this.CACHE_SCRIPT;});
		$("UserScriptLoader-debug-mode", win).addEventListener("command", event => {this.DEBUG = !this.DEBUG;});
		$('mainPopupSet', win).appendChild(win.MozXULElement.parseXULToFragment(`
			<panel id="UserScriptLoader-panel"
				class="panel-no-padding"
				type="arrow"
				role="alertdialog"
				position="bottomleft topleft"
				noautofocus="true"
				orient="vertical">
				<panelmultiview id="UserScriptLoader-panel-multiView" mainViewId="UserScriptLoader-panel-mainView">
					<panelview id="UserScriptLoader-panel-mainView" class="PanelUI-subView" role="document" showheader="true">
						<box class="panel-header">
							<html:h1><html:span role="heading">UserScriptLoader</html:span></html:h1>
						</box>
						<toolbarseparator/>
						<vbox class="panel-subview-body">
							<toolbarbutton id="UserScriptLoader-panel-toggleButton" class="subviewbutton subviewbutton-iconic"/>
							<toolbarbutton id="UserScriptLoader-panel-openFolderButton" class="subviewbutton subviewbutton-iconic" label="Open Scripts Folder" image="chrome://browser/skin/open.svg"/>
							<toolbarbutton id="UserScriptLoader-panel-rebuildButton" class="subviewbutton subviewbutton-iconic" label="Rebuild" image="chrome://global/skin/icons/reload.svg"/>
							<toolbarseparator/>
							<html:h2 class="subview-subheader">Active Scripts</html:h2>
						</vbox>
					</panelview>
					<panelview id="UserScriptLoader-panel-infoView" class="PanelUI-subView" role="document" title="Script Info">
						<vbox class="panel-subview-body">
							<toolbarbutton id="UserScriptLoader-panel-editScriptButton" class="subviewbutton subviewbutton-iconic" label="Edit" image="chrome://global/skin/icons/edit.svg"/>
							<toolbarbutton id="UserScriptLoader-panel-disableScriptButton" class="subviewbutton subviewbutton-iconic" label="Disable" image="chrome://global/skin/icons/close.svg"/>
							<toolbarbutton id="UserScriptLoader-panel-scriptHomepageButton" class="subviewbutton subviewbutton-iconic" label="Go to Homepage" image="chrome://global/skin/icons/link.svg"/>
							<toolbarseparator/>
							<html:h2 class="subview-subheader">Run at</html:h2>
							<vbox class="USL-script-desc">
								<description id="UserScriptLoader-panel-scriptRunAt"/>
							</vbox>
							<html:h2 class="subview-subheader">Version</html:h2>
							<vbox class="USL-script-desc">
								<description id="UserScriptLoader-panel-scriptVersion"/>
							</vbox>
							<html:h2 class="subview-subheader">Description</html:h2>
							<vbox class="USL-script-desc">
								<description id="UserScriptLoader-panel-scriptDescription"/>
							</vbox>
						</vbox>
					</panelview>
				</panelmultiview>
			</panel>
		`));
		$("UserScriptLoader-panel", win).addEventListener("popuphidden", event => this.onPopupHidden(event));
		$("UserScriptLoader-panel-toggleButton", win).addEventListener("command", event => {
			this.disabled = !this.disabled;
			this.pref.setValue("disabled", this.disabled);
			event.target.closest("panel").hidePopup();
		});
		$("UserScriptLoader-panel-openFolderButton", win).addEventListener("command", event => {
			this.openFolder();
			event.target.closest("panel").hidePopup();
		});
		$("UserScriptLoader-panel-rebuildButton", win).addEventListener("command", event => {
			this.rebuild();
			event.target.closest("panel").hidePopup();
		});
		$("UserScriptLoader-panel-editScriptButton", win).addEventListener("command", event => {
			this.editScript(this.getScriptForUuid(event.target.closest("panelview").getAttribute("uuid")));
			event.target.closest("panel").hidePopup();
		});
		$("UserScriptLoader-panel-disableScriptButton", win).addEventListener("command", event => {
			let script = this.getScriptForUuid(event.target.closest("panelview").getAttribute("uuid"));
			script.disabled=true;
			this.broadcastMessage("USL:SetScriptState", {leafName: script.leafName, disabled: script.disabled});
			this.disabledScripts.add(script.leafName);
			event.target.closest("panel").hidePopup();
		});
		$("UserScriptLoader-panel-scriptHomepageButton", win).addEventListener("command", event => {
			event.target.ownerGlobal.openLinkIn(
				this.getScriptForUuid(event.target.closest("panelview").getAttribute("uuid")).homepageURL,
				"tab",
				{relatedToCurrent: true, triggeringPrincipal: Services.scriptSecurityManager.getSystemPrincipal()}
			);
		});
		$("UserScriptLoader-register-menu", win).hidden = true;
		this.createMenuitem(win);
		if (this.disabled) {
			$("UserScriptLoader-icon", win).setAttribute("state", "disable");
			$("UserScriptLoader-panel-toggleButton", win).label = "Enable UserScriptLoader";
			$("UserScriptLoader-panel-toggleButton", win).image = "chrome://browser/skin/trending.svg";
		} else {
			$("UserScriptLoader-panel-toggleButton", win).label = "Disable UserScriptLoader";
			$("UserScriptLoader-panel-toggleButton", win).image = "chrome://browser/skin/addons/addon-install-blocked.svg";
		}
		win.addEventListener('unload', this, {once: true});
	}
	createMenuitem(win) {
		let popup = $('UserScriptLoader-popup', win);
		let menuseparator = $('UserScriptLoader-menuseparator', win)
		if (popup.firstChild !== menuseparator) {
			const range = win.document.createRange();
			range.setStartBefore(popup.firstChild);
			range.setEndBefore(menuseparator);
			range.deleteContents();
			range.detach();
		}
		for (const script of this.readScripts) {
			let m = win.document.createXULElement('menuitem');
			m.setAttribute('label', script.localizedName);
			m.setAttribute("class", "UserScriptLoader-item");
			m.setAttribute('checked', !script.disabled);
			m.setAttribute('type', 'checkbox');
			m.addEventListener("command", event => {
				if (event.button === 2) {
					this.editScript(script, event.target.ownerGlobal);
				}
				else {
					script.disabled = !script.disabled;
					this.broadcastMessage("USL:SetScriptState", {
						leafName: script.leafName,
						disabled: script.disabled
					});
					if (script.disabled) this.disabledScripts.add(script.leafName);
					else this.disabledScripts.delete(script.leafName);
				}
			});
			m.script = script;
			popup.insertBefore(m, menuseparator);
		}
	}
	rebuild(fromConstructor = false) {
		const loadScript = (aFile) => {
			let script,
			    leafName = aFile.leafName,
			    lastModifiedTime = aFile.lastModifiedTime;
			for (const s of this.readScripts) {
				if (s.leafName === leafName) {
					if (s.lastModifiedTime !== lastModifiedTime && !fromConstructor) {
						this.log(s.name + " reload.");
						break;
					}
					script = s;
					break;
				}
			}
			if (!script) {
				const disabled = this.disabledScripts.has(leafName);
				script = new ScriptEntry(aFile, this.resourceManager, this.database, disabled);
				script.disabled = disabled;
			}
			return script;
		};

		let newScripts = [];
		let ext = /\.user\.js$/i;
		let files = this.SCRIPTS_FOLDER.directoryEntries.QueryInterface(Ci.nsISimpleEnumerator);

		while (files.hasMoreElements()) {
			let file = files.getNext().QueryInterface(Ci.nsIFile);
			if (!ext.test(file.leafName)) continue;
			let script = loadScript(file);
			newScripts.push(script);
		}
		this.readScripts = newScripts;
		if (!fromConstructor) {
			for (let win of Services.wm.getEnumerator("navigator:browser")) {
				if (!win.USL) continue;
				this.createMenuitem(win);
			}
			if (this.CACHE_SCRIPT) this.broadcastMessage("USL:InvalidateCache");
		}
	}
	openFolder() {
		this.SCRIPTS_FOLDER.launch();
	}
	saveScript(event) {
		const win = event.target.ownerGlobal;
		const filename = decodeURIComponent(win.gBrowser.selectedBrowser.documentURI.filePath.split("/").pop().replace(/\.user\.js$|$/i, ".user.js"));

		// https://developer.mozilla.org/ja/XUL_Tutorial/Open_and_Save_Dialogs
		const fp = Cc["@mozilla.org/filepicker;1"].createInstance(Ci.nsIFilePicker);
		if (parseInt(Services.appinfo.version) >= 125) fp.init(win.browsingContext, "", Ci.nsIFilePicker.modeSave);
		else fp.init(win, "", Ci.nsIFilePicker.modeSave);
		fp.appendFilter("JavaScript Files", "*.js");
		fp.appendFilters(Ci.nsIFilePicker.filterAll);
		fp.displayDirectory = this.SCRIPTS_FOLDER; // nsIFile
		fp.defaultExtension = "js";
		fp.defaultString = filename;
		fp.open(ret => {
			if (ret === Ci.nsIFilePicker.returnCancel || !fp.file) return;
			const channel = lazy.NetUtil.newChannel({
				uri: win.gBrowser.selectedBrowser.documentURI,
				loadingPrincipal: win.gBrowser.selectedBrowser.contentPrincipal,
				contentPolicyType: Ci.nsIContentPolicy.TYPE_OTHER,
				securityFlags: Ci.nsILoadInfo.SEC_REQUIRE_CORS_INHERITS_SEC_CONTEXT,
			});
			channel.QueryInterface(Ci.nsIHttpChannel);
			channel.requestMethod = "GET";
			new Promise((resolve, reject) => {
				lazy.NetUtil.asyncFetch(channel, (stream, status) => {
					if (!Components.isSuccessCode(status)) {
						let e = Components.Exception("", status);
						reject(new Error(`Error while loading ${channel.URI.spec}: ${e.name}`));
						return;
					}
					if (!channel.requestSucceeded) {
						reject(new Error(`Server returned ${channel.responseStatus} / ${channel.responseStatusText
	}: ${channel.URI.spec}`));
						return;
					}
					const isText = lazy.BrowserUtils.mimeTypeIsTextBased(channel.contentType ?? "");
					if (isText) {
						const charset = channel.contentCharset || "UTF-8";
						try {
							const text = lazy.NetUtil.readInputStreamToString(stream, stream.available(), {charset});
							UserScriptLoader.saveText(fp.file, text);
						} catch (e) {
							reject(new Error(`Error while reading data as text from ${channel.URI.spec} (possibly invalid character encoding; ${charset} is used): ${e.message}`));
							return;
						}
					}
					else {
						reject(new Error("This doesn't look like a text file."));
					}
					resolve(fp.file.parent.path);
				});
			}).then(path => {
				if (path === this.SCRIPTS_FOLDER.path) {
					this.rebuild();
					Services.prompt.alert(win, "Done", "Script installed successfully and ready to use.");
				}
			}).catch(e => {
				Services.prompt.alert(win, "Failed to install script", e.message);
			});
		});
	}
	deleteStorage(event, type) {
		if (type !== "pref") return;
		const win = event.target.ownerGlobal;
		const data = this.database[type];
		const list = [];
		const scriptList = [];
		for (const script of this.readScripts) {
			if (Object.keys(script.prefStorage).length === 0) continue;
			list.push(script.name);
			scriptList.push(script);
		}
		if (list.length === 0)
			return win.alert("Pref storage is empty.");

		list.push("[All scripts]");
		const selected = {};
		const ok = Services.prompt.select(
			win, "UserScriptLoader " + type, "Pref storage of the selected script will be deleted:", list, selected);

		if (!ok) return;
		if (selected.value === list.length -1) {
			for (const script of scriptList) {
				script.prefStorage = {};
				this.database.pref[script.uuid] = script.prefStorage;
			}
		}
		else {
			const script = scriptList[selected.value];
			script.prefStorage = {};
			this.database.pref[script.uuid] = script.prefStorage;
		}
	}
	onPopupShowing(event) {
		const win = event.target.ownerGlobal;
		const popup = event.target;

		switch(popup.id) {
			case 'UserScriptLoader-popup':
				let contextsToVisit = [win.gBrowser.selectedBrowser.browsingContext];
				let promises = [];
				while (contextsToVisit.length) {
					try {
						let currentContext = contextsToVisit.pop();
						let global = currentContext.currentWindowGlobal;
						if (!global) continue;
						contextsToVisit.push(...currentContext.children);
						let actor = global.getActor("UserScriptLoader");
						promises.push(actor.sendQuery("USL:GetRunningScripts"));
					} catch (e) {}
				}
				Promise.all(promises).then(result => {
					let runningScripts = [];
					for (let scripts of result) {
						runningScripts.push(...scripts);
					}
					for (const menuItem of Array.from(popup.children)) {
						if (!menuItem.classList.contains("UserScriptLoader-item")) continue;
						const enabled = runningScripts.includes(menuItem.script.leafName);
						menuItem.style.fontWeight = enabled ? "bold" : "";
						menuItem.hidden = win.USL.HIDE_EXCLUDE && !enabled;
						menuItem.setAttribute('checked', !menuItem.script.disabled);
					}
				});
				$("UserScriptLoader-saveMenu", win).hidden = true;
				$("UserScriptLoader-register-menu", win).disabled = win.USL.getCommandsForBrowser(win.gBrowser.selectedBrowser).size === 0;
				if (/^text\/plain|javascript/.test(win.gBrowser.selectedBrowser.documentContentType)) {
					try {
						const actor = win.gBrowser.selectedBrowser.browsingContext.currentWindowGlobal.getActor("UserScriptLoader");
						actor?.sendQuery("USL:IsUserScript").then(ret => {
							$("UserScriptLoader-saveMenu", win).hidden = !ret;
						});
					} catch (e) {}
				}
				break;

			case 'UserScriptLoader-register-popup':
				const commands = win.USL.getCommandsForBrowser(win.gBrowser.selectedBrowser);
				if (!commands.size) return;
				commands.forEach((value, key) => {
					const menuItem = win.document.createXULElement("menuitem");
					menuItem.setAttribute("label", value.label);
					menuItem.setAttribute("tooltiptext", value.tooltiptext);
					if (value.accessKey)
						menuItem.setAttribute("accesskey", value.accessKey);
					if (value.disabled)
						menuItem.setAttribute("disabled", value.disabled);
					const icon = this.getScriptForUuid(value.scriptUuid)?.metadata?.icon;
					if (icon) {
						menuItem.setAttribute("image", icon[0]);
					}
					menuItem.classList.add("menuitem-iconic");
					menuItem.addEventListener("command", event => {
						const win = event.target.ownerGlobal;
						const eventProps = {
							button: event.button,
							altKey: event.altKey,
							ctrlKey: event.ctrlKey,
							metaKey: event.metaKey,
							shiftKey: event.shiftKey,
						}
						this.broadcastMessageToBrowser("USL:InvokeCallback", {
							identifier: key,
							eventProps,
						}, win.gBrowser.selectedBrowser);
					}, {once: true});
					popup.appendChild(menuItem);
				});
				break;
		}
	}
	onPopupHidden(event) {
		const popup = event.target;
		switch(popup.id) {
			case 'UserScriptLoader-register-popup': {
				let child = popup.firstChild;
				while (child && child.localName == 'menuitem') {
					popup.removeChild(child);
					child = popup.firstChild;
				}
				break;
			}
			case 'UserScriptLoader-panel': {
				let container = popup.querySelector("#UserScriptLoader-panel-mainView .panel-subview-body")
				let child = container.lastChild;
				while (child) {
					if (child.localName !== "toolbarbutton") break;
					child.remove();
					child = container.lastChild;
				}
				break;
			}
		}
	}
	editScript(script, win) {
		const editor = this.EDITOR;
		if (!editor) return this.editByScratchpad(script);
		try {
			const process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
			let app, args;
			if (Services.appinfo.OS === "Darwin" && editor.endsWith(".app")) {
				app = new lazy.FileUtils.File("/usr/bin/open");
				args = ["-a", editor, script.path];
			} else {
				app = new lazy.FileUtils.File(editor);
				args = [script.path];
			}
			process.init(app);
			process.runw(false, args, args.length);
		} catch (e) {}
	}
	editByScratchpad(script) {
		// Unfortunately scratchpad is removed from Firefox...
		if (!Services.prefs.getBoolPref("devtools.chrome.enabled", false)) return;
		const {BrowserConsoleManager} = lazy.require("resource://devtools/client/webconsole/browser-console-manager.js");
		BrowserConsoleManager.openBrowserConsoleOrFocus().then(webConsole => {
			webConsole.ui.jsterm._setValue(script.code);
		});
	}
	iconClick(event) {
		if (!event) {
			this.disabled = !this.disabled;
			this.pref.setValue('disabled', this.disabled);
		} else if (event.button === 1) {
			this.rebuild();
		} else if (event.button === 0) {
			const win = event.target.ownerGlobal;
			let panel = $('UserScriptLoader-panel', win);
			if (panel.anchorNode) {
				panel.hidePopup();
				return;
			}
			let anchor = event.currentTarget;
			let popupContainer = panel.querySelector("#UserScriptLoader-panel-mainView .panel-subview-body");
			let contextsToVisit = [win.gBrowser.selectedBrowser.browsingContext];
			let promises = [];
			while (contextsToVisit.length) {
				try {
					let currentContext = contextsToVisit.pop();
					let global = currentContext.currentWindowGlobal;
					if (!global) continue;
					contextsToVisit.push(...currentContext.children);
					let actor = global.getActor("UserScriptLoader");
					promises.push(actor.sendQuery("USL:GetRunningScripts"));
				} catch (e) {}
			}
			Promise.all(promises).then(result => {
				let runningScripts = new Set();
				for (let scripts of result) {
					scripts.forEach(runningScripts.add, runningScripts)
				}
				if (runningScripts.size) {
					for (const leafName of runningScripts) {
						const script = this.getScriptForLeafName(leafName);
						const commands = win.USL.getCommandsForBrowserAndScript(win.gBrowser.selectedBrowser, script);
						const icon = script.metadata?.icon;
						const item = win.document.createXULElement("toolbarbutton");
						item.label = script.name;
						item.className = "subviewbutton subviewbutton-iconic subviewbutton-nav";
						if (icon) item.image = icon[0];
						else item.image = "chrome://browser/skin/sort.svg";
						item.addEventListener("command", event => {
							const subView = win.document.querySelector("#UserScriptLoader-panel-infoView");
							subView.setAttribute("title", script.localizedName);
							subView.setAttribute("uuid", script.uuid);
							subView.querySelector("#UserScriptLoader-panel-scriptRunAt").textContent = script.run_at;
							subView.querySelector("#UserScriptLoader-panel-scriptVersion").textContent = script.metadata.version ? script.metadata.version[0] : "unknown";
							subView.querySelector("#UserScriptLoader-panel-scriptDescription").textContent = script.metadata.description?.join("\n") || "";
							if (script.homepageURL) {
								subView.querySelector("#UserScriptLoader-panel-scriptHomepageButton").removeAttribute("disabled");
							} else {
								subView.querySelector("#UserScriptLoader-panel-scriptHomepageButton").setAttribute("disabled", true);
							}
							event.target.closest("#UserScriptLoader-panel-multiView").showSubView(subView);
						});
						item.addEventListener("click", event => {
							if (event.button === 2) {
								this.editScript(script, event.target.ownerGlobal);
							}
						});
						popupContainer.appendChild(item);
						if (commands.size) {
							commands.forEach((value, key) => {
								const item = win.document.createXULElement("toolbarbutton");
								item.setAttribute("label", value.label);
								item.setAttribute("tooltiptext", value.tooltiptext);
								if (value.accessKey)
									item.setAttribute("accesskey", value.accessKey);
								if (value.disabled)
									item.setAttribute("disabled", value.disabled);
								item.className = "subviewbutton subviewbutton-iconic USL-command";
								item.image = "chrome://global/skin/icons/settings.svg";
								item.addEventListener("command", event => {
									const win = event.target.ownerGlobal;
									const eventProps = {
										button: event.button,
										altKey: event.altKey,
										ctrlKey: event.ctrlKey,
										metaKey: event.metaKey,
										shiftKey: event.shiftKey,
									}
									this.broadcastMessageToBrowser("USL:InvokeCallback", {
										identifier: key,
										eventProps,
									}, win.gBrowser.selectedBrowser);
									event.target.closest("panel").hidePopup();
								}, {once: true});
								popupContainer.appendChild(item);
							});
						}
					}
				} else {
					const item = win.document.createXULElement("toolbarbutton");
					item.label = "No scripts running in this tab.";
					item.className = "subviewbutton subviewbutton-iconic";
					item.image = "chrome://browser/skin/zoom-out.svg";
					item.setAttribute("disabled", true);
					popupContainer.appendChild(item);
				}
				lazy.PanelMultiView.openPopup(panel, anchor, {triggerEvent: event});
			});
		}
	}
	loadSetting() {
		const patterns = this.pref.getValue("GLOBAL_EXCLUDE_MATCHES", "");
		if (patterns) {
			try {
				this.globalExcludes = new MatchPatternSet(JSON.parse(patterns));
			} catch (e) {}
		}
		try {
			const aFile = Services.dirsvc.get('UChrm', Ci.nsIFile);
			aFile.append("UserScriptLoader.json");
			let data = UserScriptLoader.loadText(aFile);
			data = JSON.parse(data);
			if (data.pref) this.database.pref = data.pref;
			if (data.uuid) this.database.uuid = data.uuid;
			if (data.disabledScripts) this.database.disabledScripts = data.disabledScripts;
			//this.database.resource = data.resource;
			this.debug('loaded UserScriptLoader.json');
		} catch(e) {
			this.debug('can not load UserScriptLoader.json');
		}
	}
	saveSetting() {
		this.database.disabledScripts = Array.from(this.disabledScripts);
		const aFile = Services.dirsvc.get('UChrm', Ci.nsIFile);
		aFile.append("UserScriptLoader.json");
		UserScriptLoader.saveText(aFile, JSON.stringify(this.database, null, 2));
	}
	broadcastMessageToBrowser(name, args, browser, excludeContext = null) {
		let contextsToVisit = [browser.browsingContext];
		while (contextsToVisit.length) {
			try {
				let currentContext = contextsToVisit.pop();
				let global = currentContext.currentWindowGlobal;
				if (!global) continue;
				contextsToVisit.push(...currentContext.children);
				if (currentContext === excludeContext) continue;
				let actor = global.getActor("UserScriptLoader");
				actor.sendAsyncMessage(name, args);
			} catch (e) {}
		}
	}
	broadcastMessage(name, args, excludeContext = null) {
		for (let win of Services.wm.getEnumerator("navigator:browser")) {
			if (!win.USL) continue;
			for (let browser of win.gBrowser.browsers) {
				if (!browser.browsingContext) continue;
				/* browser.sendMessageToActor(name, args, "UserScriptLoader", "all"); */
				this.broadcastMessageToBrowser(name, args, browser, excludeContext);
			}
		}
	}
	getCommandsForBrowser(browser) {
		const commandList = new Map();
		const contextsToVisit = [browser.browsingContext];
		while (contextsToVisit.length) {
			try {
				let currentContext = contextsToVisit.pop();
				let global = currentContext.currentWindowGlobal;
				if (!global) continue;
				contextsToVisit.push(...currentContext.children);
				let actor = global.getActor("UserScriptLoader");
				actor?.activeCommands.forEach((value, key) => {
					commandList.set(key, value);
				});
			} catch (e) {}
		}
		return commandList;
	}
	getCommandsForBrowserAndScript(browser, script) {
		const commandList = new Map();
		const contextsToVisit = [browser.browsingContext];
		while (contextsToVisit.length) {
			try {
				let currentContext = contextsToVisit.pop();
				let global = currentContext.currentWindowGlobal;
				if (!global) continue;
				contextsToVisit.push(...currentContext.children);
				let actor = global.getActor("UserScriptLoader");
				actor?.activeCommands.forEach((value, key) => {
					if (value.scriptUuid === script.uuid) {
						commandList.set(key, value);
					}
				});
			} catch (e) {}
		}
		return commandList;
	}
	getScriptForUuid(uuid) {
		for (const script of this.readScripts) {
			if (script.uuid === uuid) return script;
		}
		return null;
	}
	getScriptForLeafName(leafName) {
		for (const script of this.readScripts) {
			if (script.leafName === leafName) return script;
		}
		return null;
	}
	migratePrefs() {
		if (!this.pref.getValue("scriptival.migrated", false)) {
			for (const script of this.readScripts) {
				if (Object.keys(script.prefStorage).length !== 0) continue;
				const branch = Services.prefs.getBranch(`UserScriptLoader.scriptival.${script.prefName}.`);
				for (const key of branch.getChildList("")) {
					let value;
					try {
						switch (branch.getPrefType(key)) {
							case Ci.nsIPrefBranch.PREF_STRING: value = branch.getStringPref(key); break;
							case Ci.nsIPrefBranch.PREF_INT   : value = branch.getIntPref(key); break;
							case Ci.nsIPrefBranch.PREF_BOOL  : value = branch.getBoolPref(key); break;
						}
					} catch (e) {}
					if (value != null) {
						script.setPref(key, value);
					}
				}
			}
			this.pref.setValue("scriptival.migrated", true);
		}
		if (!this.pref.getValue("script.disabled.migrated", false)) {
			const ds = this.pref.getValue('script.disabled', '');
			if (ds) {
				ds.split('|').forEach(name => this.disabledScripts.add(name));
				for (const script of this.readScripts) {
					if (this.disabledScripts.has(script.leafName)) script.disabled = true;
				}
			}
			this.pref.setValue("script.disabled.migrated", true);
		}
	}
	static loadText(aFile) {
		try {
			const fstream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
			const cis = Cc["@mozilla.org/intl/converter-input-stream;1"].createInstance(Ci.nsIConverterInputStream);
			const str = {};
			fstream.init(aFile, -1, 0, 0);
			cis.init(fstream, null, fstream.available(), 0);
			cis.readString(-1, str);
			cis.close();
			fstream.close();
			return str.value;
		} catch (e) {
			console.error(e);
		}
		return null;
	}
	static saveText(aFile, text) {
		const bytes = new TextEncoder().encode(text);
		return this.saveFile(aFile, bytes);
	}
	static saveFile(aFile, uint8Array) {
		try {
			const foStream = Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
			const bstream = Cc["@mozilla.org/binaryoutputstream;1"].createInstance(Ci.nsIBinaryOutputStream);
			foStream.init(aFile, 0x02 | 0x08 | 0x20, 0o644, 0);
			bstream.setOutputStream(foStream);
			bstream.writeByteArray(uint8Array);
			bstream.close();
			foStream.close();
		} catch (e) {
			console.error(e);
		}
		return uint8Array;
	}
	log(...args) {
		console.log.apply(console, args);
	}
	debug(...args) {
		if (!this.DEBUG) return;
		console.log.call(console, '[USL DEBUG]', ...args);
	}
	error(...args) {
		console.error.apply(console, args);
	}
}
UserScriptLoader.prototype.QueryInterface = ChromeUtils.generateQI([Ci.nsIObserver, Ci.nsISupportsWeakReference]);

export const USL = new UserScriptLoader();

/* "this" points to JSWindowActor in these helpers */
const APIHelper = {
	GM_openInTab(url, options) {
		const win = this.browsingContext.top.embedderElement.ownerGlobal;
		const {inBackground, relatedToCurrent, userContextId} = options;
		win.openLinkIn(url, "tab", {
			inBackground,
			relatedToCurrent,
			userContextId,
			triggeringPrincipal: Services.scriptSecurityManager.getSystemPrincipal(),
		});
	},
	GM_notification(options) {
		const alertSvc = Cc["@mozilla.org/alerts-service;1"].getService(Ci.nsIAlertsService);
		try {
			alertSvc.showAlertNotification(
				options.icon,
				options.title,
				options.text,
				true,
				null,
				{
					observe: (subject, topic, data) => {
						switch (topic) {
							case "alertclickcallback":
								try {
									this.sendAsyncMessage("USL:InvokeCallback", {identifier: options.clickIdentifier});
								} catch (e) {}
								break;
							case "alertfinished":
								try {
									this.sendAsyncMessage("USL:InvokeCallback", {identifier: options.doneIdentifier});
								} catch (e) {}
								break;
							case "alertshow":
								break;
						}
					}
				},
				`USL_notification_${notificationID++}`,
			);
		} catch (e) {
			console.error(e);
		}
	},
	GM_getValue(scriptUuid, name, defaultValue) {
		const script = USL.getScriptForUuid(scriptUuid);
		return Promise.resolve(script?.getPref(name, defaultValue) ?? defaultValue);
	},
	GM_setValue(scriptUuid, name, value) {
		const script = USL.getScriptForUuid(scriptUuid);
		const oldValue = script?.setPref(name, value);
		const messageData = {
			identifier: `${script.uuid}:${name}`,
			isPrefCallback: true,
			args: [name, oldValue, value, true]
		};
		USL.broadcastMessage("USL:InvokeCallback", messageData, this.browsingContext);
		messageData.args[3] = false;
		this.sendAsyncMessage("USL:InvokeCallback", messageData);
		return Promise.resolve();
	},
	GM_listValues(scriptUuid) {
		const script = USL.getScriptForUuid(scriptUuid);
		return Promise.resolve(script?.listPref() ?? []);
	},
	GM_deleteValue(scriptUuid, name) {
		const script = USL.getScriptForUuid(scriptUuid);
		const oldValue = script?.deletePref(name);
		const messageData = {
			identifier: `${script.uuid}:${name}`,
			isPrefCallback: true,
			args: [name, oldValue, void 0, true]
		};
		USL.broadcastMessage("USL:InvokeCallback", messageData, this.browsingContext);
		messageData.args[3] = false;
		this.sendAsyncMessage("USL:InvokeCallback", messageData);
		return Promise.resolve();
	},
};

export class UserScriptLoaderParent extends JSWindowActorParent {
	actorCreated() {
		this.activeCommands = new Map();
	}
	receiveMessage(message) {
		if (message.name.startsWith("USL:API:")) {
			return APIHelper[message.name.slice(8)]?.apply(this, message.data);
		}
		switch (message.name) {
			case "USL:GetMatchingScriptsForURL": {
				const {url, excludes} = message.data;
				const promises = [];
				const props = ["name", "disabled", "leafName", "delay", "run_at", "metadata", "code", "requireSrc", "resources", "uuid", "includeRegExp", "excludeRegExp", "includeMatchPatternStrings", "excludeMatchPatternStrings", "includeTLD", "excludeTLD"];
				if (USL.globalExcludes?.matches(url)) return Promise.resolve([]);
				for (const script of USL.readScripts) {
					if (excludes.includes(script.leafName)) continue;
					if (script.isURLMatching(url)) promises.push(script.isReady);
				}
				if (promises.length) {
					return Promise.all(promises).then(matchingScripts => {
						const scripts = [];
						for (const script of matchingScripts) {
							const tmp = {};
							for (const prop of props) {
								tmp[prop] = script[prop];
							}
							scripts.push(tmp);
						}
						return scripts;
					});
				}
				return Promise.resolve([]);
			}
			case "USL:RegisterCommand":
				this.activeCommands.set(message.data.identifier, message.data.options);
				break;
			case "USL:UnregisterCommand":
				this.activeCommands.delete(message.data.identifier);
				break;
		}
	}
}