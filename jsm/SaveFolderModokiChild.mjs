const targetMouseButton = Services.appinfo.OS === "WINNT" ? 2 : 1;

function doubleClickDetectedOnImage(sourceNode, actor) {
    let messageData = {
        mediaURL: sourceNode.src,
        imageText: sourceNode.title || sourceNode.alt,
        docLocation: sourceNode.ownerDocument.location.href,
        charSet: sourceNode.ownerDocument.characterSet,
        contentType: null,
        contentDisposition: null
    };
    let doc = sourceNode.ownerDocument;
    try {
        let imageCache = Cc["@mozilla.org/image/tools;1"].getService(Ci.imgITools)
                                                         .getImgCacheForDocument(doc);
        let props = imageCache.findEntryProperties(sourceNode.currentURI, doc);
        try {
            messageData.contentType = props.get("type", Ci.nsISupportsCString).data;
        } catch (e) {}
        try {
            messageData.contentDisposition = props.get("content-disposition", Ci.nsISupportsCString).data;
        } catch (e) {}
    } catch (e) {}
    actor.sendAsyncMessage("SFM:DoubleClickDetectedOnImage", messageData);
}

export class SaveFolderModokiChild extends JSWindowActorChild {
    handleEvent(aEvent) {
        switch (aEvent.type) {
            case "click":
                if (aEvent.button != targetMouseButton || aEvent.detail != 2) break;
                if (aEvent.target.tagName.toLowerCase() === "img" && aEvent.target.src) {
                    aEvent.preventDefault();
                    aEvent.stopPropagation();
                    doubleClickDetectedOnImage(aEvent.target, this);
                }
                break;
        }
    }
}