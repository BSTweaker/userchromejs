const childModuleStat = IOUtils.stat(PathUtils.join(
    PathUtils.join(PathUtils.profileDir, "chrome", "jsm"),
    "DeepLTranslatorChild.mjs"
));

export const DLTranslator = new class {
    constructor() {
        this.actorRegistered = false;
        this.config = null;
    }
    attachToWindow(win, config) {
        let menuPopup = win.document.getElementById("contentAreaContextMenu");
        let menuItem = win.document.createXULElement("menuitem");
        menuItem.label = "選択テキストを DeepL で翻訳";
        menuItem.id = "menu-deepl-translate";
        menuItem.addEventListener("command", this);
        menuPopup.appendChild(menuItem);
        menuPopup.addEventListener("popupshowing", this);
        win.addEventListener("unload", this, {once:true});
        childModuleStat.then(childInfo => {
            if (!this.actorRegistered) {
                const childURL = `resource://deepl-ucjs/DeepLTranslatorChild.mjs?${childInfo.lastModified}`;
                const actorParams = {
                    parent: {
                        esModuleURI: Components.stack.filename,
                    },
                    child: {
                        esModuleURI: childURL,
                        events: {},
                    },
                    allFrames: true,
                    messageManagerGroups: ["browsers"],
                    matches: [`*://*/*`],
                };
                if (config.hotkey.enabled) {
                    if (config.hotkey.repeat <= 0) config.hotkey.repeat = 2;
                    if (config.hotkey.timeout <= 0) config.hotkey.timeout = 500;
                    actorParams.child.events.keyup = {};
                }
                this.config = config;
                try {
                    ChromeUtils.registerWindowActor("DLTranslator", actorParams);
                    this.actorRegistered = true;
                } catch(e) {console.error(e);}
            }
        }).catch(e => {
            console.error("DeepLTranslator: required .mjs files are not placed correctly!");
        });
    }
    detachFromWindow(win) {
        win.removeEventListener("unload", this, {once:true}); // this might be unnecessary, but do anyway
        const menu = win.document.getElementById("menu-deepl-translate");
        menu.parentNode.removeEventListener("popupshowing", this);
        menu.parentNode.removeChild(menu);
    }
    handleEvent({type, target}) {
        switch(type) {
            case "popupshowing":
                this.handlePopup(target.ownerGlobal);
                break;
            case "command":
                this.beginTranslate(target.ownerGlobal.gContextMenu?.contentData);
                break;
            case "unload":
                this.detachFromWindow(target.ownerGlobal);
                break;
        }
    }
    handlePopup(win) {
        let selectionText = win.gContextMenu?.contentData?.selectionInfo?.text;
        win.document.getElementById("menu-deepl-translate").hidden = !selectionText;
    }
    beginTranslate(contextMenuContentData) {
        if (!contextMenuContentData) return;
        const win = contextMenuContentData.browser.ownerGlobal;
        const selectionText = contextMenuContentData.selectionInfo?.fullText;
        const targetIdentifier = contextMenuContentData.context?.targetIdentifier;
        const screenX = contextMenuContentData.context?.screenX ?? contextMenuContentData.context?.screenXDevPx / win.devicePixelRatio;
        const screenY = contextMenuContentData.context?.screenY ?? contextMenuContentData.context?.screenYDevPx / win.devicePixelRatio;
        const browser = contextMenuContentData.browser;
        const browserBoundingRect = browser.getBoundingClientRect();
        const fixupX = browser.ownerGlobal.outerWidth - browserBoundingRect.left - browserBoundingRect.width;
        const fixupY = 20 + browser.ownerGlobal.outerHeight - browserBoundingRect.top - browserBoundingRect.height;
        const actor = contextMenuContentData.frameBrowsingContext.currentWindowGlobal.getActor("DLTranslator");
        actor.sendAsyncMessage("DLT:CreatePopup", {
            targetIdentifier,
            screenX, screenY,
            fixupX, fixupY,
            fromLang: null,
            toLang: null,
            sourceText: selectionText,
        });
    }
}();

export class DLTranslatorParent extends JSWindowActorParent {
    receiveMessage({name, data}) {
        switch(name) {
            case "DLT:OpenTranlatorInTab":
                const win = this.browsingContext.top.embedderElement.ownerGlobal;
                win.openLinkIn(`https://www.deepl.com/translator#${data.sourceLang}/${data.targetLang}/${encodeURIComponent(data.sourceText)}`,
                    "tab", {
                        relatedToCurrent: true,
                        triggeringPrincipal: Services.scriptSecurityManager.getSystemPrincipal(),
                    }
                );
                break;
            case "DLT:GetConfig":
                return DLTranslator.config;
        }
    }
}
