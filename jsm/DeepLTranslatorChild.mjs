const lazy = {};
ChromeUtils.defineESModuleGetters(lazy, {
    NetUtil: "resource://gre/modules/NetUtil.sys.mjs",
});

class DLPopupTranslator {
    constructor(win, x, y, sourceText) {
        this.actor = win.windowGlobalChild.getActor("DLTranslator");
        this.sourceLang = "EN";
        this.targetLang = null;
        this.sourceText = sourceText.trim();
        const popup = this.popup = win.document.createElement("div");
        Object.assign(popup.style, {
            position: "absolute",
            top: `${win.scrollY + y}px`,
            left: `${win.scrollX + x}px`,
            width: "400px",
            maxHeight: "200px",
            fontFamily: "sans-serif",
            fontSize: "16px",
            color: "black",
            background: "floralwhite",
            border: "1px solid darkgray",
            borderRadius: "3px",
            boxShadow: "3px 3px 5px lightgray",
            transition: "opacity 0.2s ease",
            zIndex: "1000",
        });
        const flex = win.document.createElement("div");
        Object.assign(flex.style, {
            display: "flex",
            maxHeight: "200px",
            flexDirection: "column",
        });
        const header = win.document.createElement("div");
        Object.assign(header.style, {
            display: "flex",
            height: "auto",
            margin: "2px 5px 1px",
            fontSize: "smaller",
            alignItems: "center",
        });
        const logo = win.document.createElement("div");
        logo.textContent = "DeepL ほんやくくん";
        Object.assign(logo.style, {
            width: "auto",
            fontWeight: "bold",
            flexGrow: "1",
        });
        header.appendChild(logo);
        const langSelector = win.document.createElement("select");
        this.actor.configAvailable.then(config => {
            for (let [lang, desc] of Object.entries(config.supportedLangs)) {
                const option = win.document.createElement("option");
                option.value = lang;
                option.textContent = desc;
                langSelector.appendChild(option);
            }
            langSelector.value = this.actor.defaultLang;
        });
        Object.assign(langSelector.style, {
            width: "auto",
            marginRight: "5px",
        });
        langSelector.addEventListener("change", this);
        header.appendChild(langSelector);
        const more = win.document.createElement("div");
        more.className = "deepl-translator-more";
        more.textContent = "more";
        Object.assign(more.style, {
            width: "auto",
            cursor: "pointer",
        });
        more.addEventListener("click", this);
        header.appendChild(more);
        flex.appendChild(header);
        const box = win.document.createElement("div");
        box.className = "deepl-translator-box";
        Object.assign(box.style, {
            height: "auto",
            overflow: "auto",
            background: "white",
            padding: "2px",
            margin: "1px 5px 5px",
            border: "1px solid darkgray",
            flexGrow: "1",
            whiteSpace: "pre-wrap",
        });
        flex.appendChild(box);
        this.popup.appendChild(flex);
        win.document.body.appendChild(popup);
        win.setTimeout(() => win.addEventListener("click", this), 0);
    }
    handleEvent(event) {
        const {type, target} = event;
        switch(type) {
            case "click":
                if (!this.popup.contains(target)) {
                    target.ownerGlobal.removeEventListener("click", this);
                    this.popup.addEventListener("transitionend", ({target}) => {
                        target.parentNode.removeChild(target);
                    }, {once:true});
                    this.popup.style.opacity = 0;
                }
                else if (target.className === "deepl-translator-more") {
                    this.actor.sendAsyncMessage("DLT:OpenTranlatorInTab", {
                        sourceText: this.sourceText,
                        sourceLang: this.sourceLang,
                        targetLang: this.targetLang,
                    });
                    event.stopPropagation();
                }
                break;
            case "change":
                this.targetLang = target.value;
                this.translate(null, this.targetLang);
                break;
        }
    }
    setText(text, color) {
        let box = this.popup.querySelector(".deepl-translator-box");
        if (color) box.style.color = color;
        else box.style.color = null;
        box.textContent = text;
    }
    fetchWithPrivilege(url, request) {
        return new Promise((resolve, reject) => {
            const {method, headers, body, referrer} = request;
            let origin = referrer ? referrer : url;
            const channel = lazy.NetUtil.newChannel({
                uri: lazy.NetUtil.newURI(url),
                loadingPrincipal: Services.scriptSecurityManager.createContentPrincipal(lazy.NetUtil.newURI(origin), {}),
                contentPolicyType: Ci.nsIContentPolicy.TYPE_OTHER,
                securityFlags: Ci.nsILoadInfo.SEC_REQUIRE_CORS_INHERITS_SEC_CONTEXT || Ci.nsILoadInfo.SEC_REQUIRE_CORS_DATA_INHERITS,
            });
            channel.QueryInterface(Ci.nsIHttpChannel);
            channel.loadFlags |= Ci.nsIRequest.LOAD_ANONYMOUS;
            channel.requestMethod = method || "GET";
            if (headers) {
                for (const [name, value] of Object.entries(headers)) {
                    if (channel.setNewReferrerInfo && name.toLowerCase() === "referer") {
                        channel.setNewReferrerInfo(
                            value,
                            Ci.nsIReferrerInfo.UNSAFE_URL,
                            true
                        );
                    }
                    else {
                        channel.setRequestHeader(name, value, false);
                    }
                }
            }
            if (body) {
                let stream = Cc["@mozilla.org/io/string-input-stream;1"].createInstance(Ci.nsIStringInputStream);
                stream.setUTF8Data(body);
                channel.QueryInterface(Ci.nsIUploadChannel2);
                channel.explicitSetUploadStream(stream, null, -1, method, false);
            }
            lazy.NetUtil.asyncFetch(channel, (stream, status) => {
                if (!Components.isSuccessCode(status)) {
                    let e = Components.Exception("", status);
                    reject(new Error(`Error while loading ${url}: ${e.name}`));
                }
                let text = lazy.NetUtil.readInputStreamToString(stream, stream.available());
                stream.close();
                resolve({
                    ok: channel.requestSucceeded,
                    status: channel.responseStatus,
                    statusText: channel.responseStatusText,
                    contentType: channel.getResponseHeader("content-type"),
                    text
                });
            });
        });
    }
    translateUsingBackdoor(from, to) {
        const getTimestamp = text => {
            let now = new Date().getTime();
            let count = 1;
            let match = text.match(/i/g);
            if (match) {
                count += match.length;
            }
            return now + count - now % count;
        };
        this.fetchWithPrivilege("https://www2.deepl.com/jsonrpc", {
            method: "POST",
            referrer: "https://www.deepl.com/translator",
            headers: {
                "Content-Type": "application/json",
                Referer: "https://www.deepl.com/translator",
            },
            body: JSON.stringify({
                jsonrpc: "2.0",
                method: "LMT_handle_texts",
                params: {
                    texts: [{text: this.sourceText}],
                    splitting: "newlines",
                    lang: {
                        target_lang: to.slice(0, 2),
                        source_lang_user_selected: from ? from : "auto",
                    },
                    timestamp: getTimestamp(this.sourceText),
                },
                id: 1000000+Math.floor(Math.random()*99000000),
            }, null, 4),
        }).then(resp => {
            if (!resp.ok && !resp.contentType.startsWith("application/json")) {
                throw new Error(`Server returned ${resp.status} ${resp.statusText}`);
            }
            return JSON.parse(resp.text);
        }).then(json => {
            if (json.error) throw new Error(`Error code ${json.error.code}: ${json.error.message}`);
            if (!json.result?.texts) throw new Error("Unknown error occurred");
            this.setText(json.result.texts[0].text);
            this.sourceLang = json.result.lang;
        }).catch(e => {
            this.setText(e.message, "red");
            console.error(e);
        });
    }
    async translate(from, to) {
        await this.actor.configAvailable;
        this.setText("翻訳中...", "lightgray");
        if (to) {
            this.popup.querySelector("select").value = this.targetLang = to;
            this.actor.defaultLang = to;
        }
        else if (!this.targetLang) {
            this.targetLang = this.actor.defaultLang;
        }
        if (!this.actor.config.apiKey) return this.translateUsingBackdoor(from, this.targetLang);
        const body = new URLSearchParams();
        body.append("text", this.sourceText);
        body.append("target_lang", this.targetLang);
        if (from) body.append("source_lang", from);
        return fetch(`${this.actor.config.apiEndpoint}/translate`, {
            method: "POST",
            referrerPolicy: "no-referrer",
            credentials: "omit",
            headers: {
                Authorization: `DeepL-Auth-Key ${this.actor.config.apiKey}`,
            },
            body,
        }).then(resp => {
            if (!resp.ok && resp.status != 400) {
                throw new Error(`Server returned ${resp.status} ${resp.statusText}`);
            }
            return resp.json();
        }).then(json => {
            if (!json.translations) throw new Error(`${json.message}: ${json.detail}`);
            this.setText(json.translations[0].text);
            this.sourceLang = json.translations[0].detected_source_language;
            fetch(`${this.actor.config.apiEndpoint}/usage`, {
                referrerPolicy: "no-referrer",
                credentials: "omit",
                headers: {
                    Authorization: `DeepL-Auth-Key ${this.actor.config.apiKey}`,
                },
            }).then(resp => {
                if (!resp.ok) throw new Error();
                return resp.json();
            }).then(json => {
                this.popup.title = `Quota: ${(100*json.character_count/json.character_limit).toPrecision(2)}% (${json.character_count} / ${json.character_limit})`;
            }).catch(()=>{});
        }).catch(e => {
            this.setText(e.message, "red");
            console.error(e);
        });
    }
}

export class DLTranslatorChild extends JSWindowActorChild {
    actorCreated() {
        this.defaultLang = null;
        this.keyRepeat = 0;
        this.config = null;
        this.configAvailable = this.sendQuery("DLT:GetConfig").then(config => {
            this.config = config;
            this.defaultLang = config.defaultLang;
            return this.config;
        });
    }
    createPopupWithScreenCoordinate(screenX, screenY, sourceText) {
        let x = screenX - this.contentWindow.screenX - this.contentWindow.outerWidth + this.contentWindow.innerWidth;
        let y = screenY - this.contentWindow.screenY - this.contentWindow.outerHeight + this.contentWindow.innerHeight;
        return this.createPopupWithClientCoordinate(x, y, sourceText);
    }
    createPopupWithClientCoordinate(clientX, clientY, sourceText) {
        let x = clientX;
        let y = clientY;
        let clientWidth = this.contentWindow.document.documentElement.clientWidth;
        let clientHeight = this.contentWindow.document.documentElement.clientHeight;
        if (x + 400 > clientWidth) x = clientWidth - 400;
        if (y + 200 > clientHeight) y = clientHeight - 200;
        x = Math.max(x, 0);
        y = Math.max(y, 0);
        return new DLPopupTranslator(this.contentWindow, x, y, sourceText);
    }
    createPopupWithSelection() {
        const selection = this.contentWindow.getSelection();
        const text = selection.toString().trim();
        if (text) {
            let rect = selection.getRangeAt(0).getBoundingClientRect();
            return this.createPopupWithClientCoordinate(rect.left, rect.top+rect.height, text);
        }
        return null;
    }
    receiveMessage({name, data}) {
        switch(name) {
            case "DLT:CreatePopup":
                let fixupX = 0;
                let fixupY = 0;
                if (data.fixupX) fixupX = data.fixupX;
                if (data.fixupY) fixupY = data.fixupY;
                this.createPopupWithScreenCoordinate(data.screenX+fixupX, data.screenY+fixupY, data.sourceText).translate(data.fromLang, data.toLang);
                break;
            case "DLT:CreatePopupWithClientCoordinate":
                this.createPopupWithClientCoordinate(data.clientX, data.clientY, data.sourceText).translate(data.fromLang, data.toLang);
                break;
        }
    }
    async handleEvent(event) {
        await this.configAvailable;
        switch (event.type) {
            case "keyup":
                if (event.code === this.config.hotkey.code && this.contentWindow.getSelection()?.toString()) {
                    if (!this.keyRepeat) {
                        new Promise((resolve, reject) => {
                            this.hotkeyResolver = resolve;
                            this.hotkeyRejector = reject;
                            this.contentWindow.setTimeout(() => reject(), this.config.hotkey.timeout);
                        }).then(() => {
                            this.keyRepeat = 0;
                            this.hotkeyResolver = null;
                            this.hotkeyRejector = null;
                            this.createPopupWithSelection()?.translate(null, this.defaultLang);
                        }).catch(() => {
                            this.keyRepeat = 0;
                            this.hotkeyResolver = null;
                            this.hotkeyRejector = null;
                        });
                    }
                    if (++this.keyRepeat === this.config.hotkey.repeat) {
                        this.hotkeyResolver();
                    }
                }
                else if (this.keyRepeat) {
                    this.hotkeyRejector();
                }
                break;
        }
    }
}
