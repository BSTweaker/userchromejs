const lazy = {};
ChromeUtils.defineESModuleGetters(lazy, {
	PrivateBrowsingUtils: "resource://gre/modules/PrivateBrowsingUtils.sys.mjs",
});

class ScriptAPI {
	constructor(script, sandbox, win) {
		this._script = script;
		/* Although a reference to window or sandbox should be holded in other places
		   these WeakRefs mysteriously return undefined after finishing page loading
		   at least on ESR91. So stop using at the moment. Possibly bug 1755725 */
		if (false && "WeakRef" in globalThis) {
			this._weakWin = new WeakRef(win);
			this._weakSandbox = new WeakRef(sandbox);
		} else {
			this._win = win;
			this._sandbox = sandbox;
		}
	}
	get window() {
		if (this._weakWin) return this._weakWin.deref();
		return this._win;
	}
	get sandbox() {
		if (this._weakSandbox) return this._weakSandbox.deref();
		return this._sandbox;
	}
	get actor() {
		return this.window.windowGlobalChild.getActor("UserScriptLoader");
	}
	get bypassCspSandbox() {
		/* An expanded principal (an array of principals or DOM windows) should be set to sandbox
		   to inject inline style tags bypassing the page CSP (bug 1407056/1415352).
		   Since objects exported to unsafeWindow (window.wrappedJSObject) are somewhat protected from
		   the page context in this case(*), we make a separate sandbox here only for GM_* APIs.
		   (*)Code in the sandbox must use exportFunction()/createObjectIn() (available via wantExportHelpers)
		   explicitly to poke with unsafeWindow, which is unacceptable for userscripts. */
		if (!this._bypassCspSandbox) {
			this._bypassCspSandbox = new Cu.Sandbox([this.window], {
				sandboxName: `USL sandbox for ${this._script.leafName}`,
				sandboxPrototype: this.window,
				sameZoneAs: this.window,
			});
			UserScriptLoader.getContextForWindow(this.window)?.USL_sandboxes.add(this._bypassCspSandbox);
		}
		return this._bypassCspSandbox;
	}
	static hasGM4Variant(name) {
		return ["GM.getValue", "GM.setValue", "GM.listValues", "GM.deleteValue", "GM.getResourceUrl", "GM.xmlHttpRequest"].includes(name);
	}
	/* works */
	GM_log(...args) {
		const line = Components.stack.caller.lineNumber - this._script.lineNumberOffset;
		this.window.console.log.call(this.window.console, `[${this._script.name}:%c${line}%c]`, "font-style: italic", "font-style: inherited;", ...args);
	}
	/* works */
	GM_xmlhttpRequest(obj) {
		if (!obj) return;
		if (typeof(obj) !== 'object' || (typeof(obj.url) !== 'string' && !(obj.url instanceof String))) return;

		const baseURI = Services.io.newURI(this.window.location.href, null, null);
		obj.url = Services.io.newURI(obj.url, null, baseURI).spec;
		const user = (obj.user && typeof obj.user === "string") ? obj.user : null;
		const password = (obj.password && typeof obj.password === "string") ? obj.password : null;
		const xhr = new XMLHttpRequest({
			mozAnon: (typeof obj.anonymous === "boolean") ? obj.anonymous : false,
		});
		xhr.open(obj.method || 'GET', obj.url, true, user, password);
		xhr.setOriginAttributes({
			userContextId: this.window.windowGlobalChild.browsingContext.originAttributes.userContextId
		});
		if (typeof(obj.headers) === 'object') {
			for (const key in obj.headers) xhr.setRequestHeader(key, obj.headers[key]);
		}
		if (obj.overrideMimeType) xhr.overrideMimeType(obj.overrideMimeType);
		switch (obj.responseType) {
			case "arraybuffer":
			case "blob":
				xhr.responseType = "arraybuffer";
				break;
			default:
				xhr.responseType = "text";
		}
		if (typeof obj.timeout === "number") xhr.timeout = obj.timeout;
		for (const type of ["onload", "onloadstart", "onloadend", "onprogress", "onabort", "ontimeout", "onerror", "onreadystatechange"]) {
			const callbackFunc = (obj.wrappedJSObject) ? ChromeUtils.unwaiveXrays(obj.wrappedJSObject[type]) : obj[type];
			if (callbackFunc && (typeof(callbackFunc) === 'function' || callbackFunc instanceof Function)) {
				xhr[type] = () => {
					const resp = Cu.createObjectIn(callbackFunc);
					/* We do not need Cu.cloneInto() for primitives */
					resp.context = obj.context;
					resp.status = (xhr.readyState === 4) ? xhr.status : 0;
					resp.statusText = (xhr.readyState === 4) ? xhr.statusText : '';
					resp.responseHeaders = (xhr.readyState >= 2) ? xhr.getAllResponseHeaders() : '';
					resp.readyState = xhr.readyState;
					resp.finalUrl = (xhr.readyState === 4) ? xhr.channel.URI.spec : '';
					if (xhr.responseType === "text") {
						resp.responseText = xhr.responseText;
						if (!obj.responseType || obj.responseType === "text") resp.response = resp.responseText;
					}
					if (xhr.readyState === 4) {
						if (xhr.responseType === "arraybuffer" && xhr.response instanceof ArrayBuffer) {
							try {
								const safeUint8Arr = new this.window.Uint8Array(Cu.cloneInto(xhr.response, resp));
								if (obj.responseType === "arraybuffer")
									resp.response = safeUint8Arr.buffer;
								else if (obj.responseType === "blob")
									resp.response = new this.window.Blob([safeUint8Arr], {type: xhr.channel.contentType});
							} catch (e) {console.error(e)}
						}
						else if (obj.responseType === "json" && xhr.responseText) {
							try {
								resp.response = Cu.cloneInto(JSON.parse(xhr.responseText), resp);
							} catch (e) {console.error(e)}
							delete resp.responseText;
						}
						else if (obj.responseType === "document" && xhr.responseText) {
							try {
								resp.response = new this.window.DOMParser().parseFromString(xhr.responseText, xhr.channel.contentType);
							} catch (e) {console.error(e)}
							delete resp.responseText;
						}
					}
					callbackFunc(resp);
				};
			}
		}
		
		xhr.send(obj.data || null);
		UserScriptLoader.debug(this._script.name + ' GM_xmlhttpRequest ' + obj.url);
	}
	GM4_xmlHttpRequest(obj) {
		/* just a difference of xml*H*ttp vs xml*h*ttp */
		this.GM_xmlhttpRequest(obj);
	}
	/* works */
	GM_addStyle(code) {
		/* call function in a special sandbox to bypass CSP */
		if (!this._addStyle) {
			this._addStyle = new this.bypassCspSandbox.Function("css", `
				const parent = document.querySelector('head') || document.body || document.documentElement;
				if (parent) {
					const style = document.createElement('style');
					style.type = 'text/css';
					style.textContent = css;
					parent.appendChild(style);
					return style;
				}
			`);
		}
		return this._addStyle(code + "");
	}
	/* works */
	GM_setValue(name, value) {
		/* When a value is an object created in the sandbox we should waive
		   x-ray vision to access all properties defined in the object, but
		   since properties which are not accessible are not serializable
		   (e.g. function or getter), unwaived value can be directly passed to
		   sendAsyncMessage here. */
		this.actor.sendAsyncMessage("USL:API:GM_setValue", [
			this._script.uuid,
			name,
			value
		]);
	}
	GM4_setValue(name, value) {
		return new this.window.Promise((resolve, reject) =>
			this.actor.sendQuery("USL:API:GM_setValue", [
				this._script.uuid,
				name,
				value
			]).then(resolve, reject)
		);
	}
	/* works */
	GM_getValue(name, defaultValue) {
		try {
			return Cu.cloneInto(Services.cpmm.sendSyncMessage("USL:GetScriptPref", {
				uuid: this._script.uuid,
				name,
				defaultValue
			})[0], this.sandbox);
		} catch (e) {}
		return defaultValue;
	}
	GM4_getValue(name, defaultValue) {
		return new this.window.Promise((resolve, reject) =>
			this.actor.sendQuery("USL:API:GM_getValue", [
				this._script.uuid,
				name,
				defaultValue
			]).then(ret =>
				Cu.cloneInto(ret, this.sandbox)
			).then(resolve, reject)
		);
	}
	/* works */
	GM_listValues() {
		let ret = [];
		try {
			ret = Services.cpmm.sendSyncMessage("USL:ListScriptPref", {
				uuid: this._script.uuid
			})[0];
		} catch (e) {}
		return Cu.cloneInto(ret, this.sandbox);
	}
	GM4_listValues() {
		return new this.window.Promise((resolve, reject) => {
			new Promise(res => {
				this.actor.sendQuery("USL:API:GM_listValues", [
					this._script.uuid
				]).then(ret =>
					res(Cu.cloneInto(ret, this.sandbox))
				);
			}).then(resolve, reject);
		});
	}
	/* works */
	GM_deleteValue(name) {
		this.actor.sendAsyncMessage("USL:API:GM_deleteValue", [
			this._script.uuid,
			name
		]);
	}
	GM4_deleteValue(name) {
		return new this.window.Promise((resolve, reject) =>
			this.actor.sendQuery("USL:API:GM_deleteValue", [
				this._script.uuid,
				name
			]).then(resolve, reject)
		);
	}
	/* works */
	GM_getResourceText(name) {
		const obj = this._script.resources[name];
		try {
			if (obj) {
				if (obj.text) return obj.text;
				if (obj.bytes) return new TextDecoder().decode(obj.bytes);
			}
		} catch (e) {
			console.error(e);
		}
		return "";
	}
	/* works */
	GM_getResourceURL(name, isBlob = false) {
		const obj = this._script.resources[name];
		try {
			let bytes = null;
			if (obj?.bytes) bytes = obj.bytes;
			else if (obj?.text) bytes = new TextEncoder().encode(obj.text);
			if (bytes) {
				if (isBlob) {
					const scope = this.sandbox;
					const blobURL = scope.URL.createObjectURL(new scope.Blob([bytes], {type: obj.contentType}))
					return blobURL;
				}
				else {
					const stream = Cc["@mozilla.org/io/arraybuffer-input-stream;1"].createInstance(Ci.nsIArrayBufferInputStream);
					const b64enc = Cc["@mozilla.org/scriptablebase64encoder;1"].createInstance(Ci.nsIScriptableBase64Encoder);
					stream.setData(bytes.buffer, 0, bytes.length);
					return `data:${obj.contentType};base64,${b64enc.encodeToString(stream, bytes.length)}`;
				}
			}
		} catch (e) {
			console.error(e);
		}
		return "";
	}
	GM4_getResourceUrl(name) {
		return this.window.Promise.resolve(this.GM_getResourceURL(name));
	}
	/* works */
	GM_getMetadata(key) {
		return this._script.metadata[key] ? this._script.metadata[key].slice() : void 0;
	}
	/* works */
	GM_openInTab(url, _options) {
		const options = {
			inBackground: false,
			relatedToCurrent: true,
		};
		if (typeof _options === "boolean") {
			options.inBackground = _options;
		}
		else if (_options && typeof _options === "object") {
			if (_options.active === false) options.inBackground = true;
			if (_options.insert === false) options.relatedToCurrent = false;
			if (typeof _options.container === "number") options.userContextId = _options.container;
		}
		this.actor.sendAsyncMessage("USL:API:GM_openInTab", [url, options]);
	}
	/* works */
	GM_setClipboard(str) {
		try {
			Cc['@mozilla.org/widget/clipboardhelper;1'].getService(Ci.nsIClipboardHelper).copyString(str);
		} catch (e) {
			console.error(e);
		}
	}
	/* works */
	GM_generateUUID() {
		const { generateUUID } = Services.uuid;
		return generateUUID().toString();
	}
	/* works */
	GM_registerMenuCommand(caption, func, aAccelKey, aAccelModifiers, aAccessKey) {
		const identifier = `${this._script.leafName}/:/${caption}`;
		this.actor.callbackManager.registerCommand(identifier, func, {
			label: caption,
			accelKey: aAccelKey,
			accelModifiers: aAccelModifiers,
			accessKey: aAccessKey,
			tooltiptext: this._script.name,
			scriptUuid: this._script.uuid,
		});
		return caption;
	}
	/* works */
	GM_unregisterMenuCommand(caption) {
		const identifier = `${this._script.leafName}/:/${caption}`;
		this.actor.callbackManager.unregisterCommand(identifier);
	}
	/* works */
	GM_notification(...args) {
		let options = {};
		if (typeof args[0] === "object" && args[0] !== null) options = ChromeUtils.waiveXrays(args[0]);
		else {
			if (typeof args[0] === "string") options.title = args[0];
			if (typeof args[1] === "string") options.text = args[1];
			if (typeof args[2] === "string") options.icon = args[2];
			if (typeof args[3]?.wrappedJSObject === "function") options.onclick = args[3].wrappedJSObject;
		}
		if (options.title) {
			if (typeof options.onclick === "function") {
				const identifier = this.GM_generateUUID();
				this.actor.callbackManager.registerCallback(identifier, ChromeUtils.unwaiveXrays(options.onclick));
				options.clickIdentifier = identifier;
				delete options.onclick;
			}
			if (typeof options.ondone === "function") {
				const identifier = this.GM_generateUUID();
				this.actor.callbackManager.registerCallback(identifier, ChromeUtils.unwaiveXrays(options.ondone));
				options.doneIdentifier = identifier;
				delete options.ondone;
			}
			this.actor.sendAsyncMessage("USL:API:GM_notification", [options]);
		}
	}
	/* works */
	GM_addElement(...args) {
		let parent, tagName, props;
		if (typeof args[0] === "string") {
			tagName = args[0];
			props = args[1];
		} else {
			parent = args[0];
			tagName = args[1];
			props = args[2];
		}
		if (typeof tagName !== "string") return;
		if (typeof props !== "object") props = null;
		tagName = tagName.toLowerCase();

		/* call function in a special sandbox to bypass CSP */
		if (!this._addElement) {
			this._addElement = new this.bypassCspSandbox.Function("parent", "tagName", "props", `
				if (!parent) {
					if (['script', 'link', 'style', 'meta'].includes(tagName))
						parent = document.head || document.body || document.documentElement;
					else
						parent = document.body || document.documentElement;
				}
				const elem = document.createElement(tagName);
				for (const key in props) {
					if (key === "textContent") elem.textContent = props[key];
					else elem.setAttribute(key, props[key]);
				}
				return parent.appendChild(elem);
			`);
		}
		return this._addElement(parent, tagName, Cu.cloneInto(props, this.bypassCspSandbox));
	}
	/* works */
	GM_addValueChangeListener(name, callback) {
		const identifier = this.GM_generateUUID();
		this.actor.callbackManager.registerPrefListener(identifier, this._script.uuid, name, callback);
		return identifier + "/:/" + name;
	}
	/* works */
	GM_removeValueChangeListener(listenerId) {
		const [identifier, name] = listenerId.split("/:/");
		this.actor.callbackManager.unregisterPrefListener(identifier, this._script.uuid, name);
	}
}

class ScriptEntry {
	constructor(script) {
		this.code = script.code;
		this.disabled = script.disabled;
		this.requireSrc = script.requireSrc;
		this.resources = script.resources;
		this.run_at = script.run_at;
		this.metadata = script.metadata;
		this.name = script.name;
		this.leafName = script.leafName;
		if (typeof script.delay === "number") this.delay = script.delay;
		this.uuid = script.uuid;
		this.matchPatternInitialized = false;
		this.includeRegExp = script.includeRegExp;
		this.excludeRegExp = script.excludeRegExp;
		this.includeMatchPatternSet = null;
		this.excludeMatchPatternSet = null;
		this.includeMatchPatternStrings = script.includeMatchPatternStrings;
		this.excludeMatchPatternStrings = script.excludeMatchPatternStrings;
		this.includeTLD = script.includeTLD;
		this.excludeTLD = script.excludeTLD;
	}
	get info() {
		if (!this._infoDictionary) {
			const metadata = this.metadata;
			const resources = [];
			for (const name in this.resources) {
				resources.push({name, url:this.resources[name].url});
			}
			this._infoDictionary = {
				scriptHandler: "UserScriptLoader",
				version: "under development forever",
				script: {
					description: metadata.description?.join("\n") || "",
					excludes: metadata.unmatch || metadata.exclude || [],
					includes: metadata.include || [],
					matches: metadata.match || [],
					name: this.name,
					namespace: metadata.namespace ? metadata.namespace[0] : "",
					resources,
					runAt: this.run_at,
					version: metadata.version ? metadata.version[0] : "",
				}
			};
		}
		return this._infoDictionary;
	}
	runInWindow(win) {
		const context = UserScriptLoader.getContextForWindow(win, true);
		if (context.USL_run.has(this.leafName)) {
			UserScriptLoader.debug(`DABUTTAYO!!!!! ${this.name} @ ${win.location.href}`);
			return;
		}
		if ("bookmarklet" in this.metadata) {
			const func = new Function(this.code);
			win.location.href = "javascript:" + encodeURIComponent(func.toSource()) + "();";
			context.USL_run.add(this.leafName);
			UserScriptLoader.debug(`Running ${this.leafName} @ ${win.location.href}`);
			return;
		}

		let sandbox, GM_obj;
		if (this.metadata?.grant?.includes("none")) {
			/* Make sandbox directly accessible to main world context in case "@grant none" is specified */
			sandbox = new Cu.Sandbox(win, {
				sandboxName: `USL sandbox for ${this.leafName}`,
				sandboxPrototype: win,
				sameZoneAs: win,
				wantXrays: false,
			});
			const unsafeWindowGetter = new sandbox.Function('return window;');
			Object.defineProperty(sandbox, 'unsafeWindow', {get: unsafeWindowGetter});
		} else {
			sandbox = new Cu.Sandbox(win, {
				sandboxName: `USL sandbox for ${this.leafName}`,
				sandboxPrototype: win,
				sameZoneAs: win,
				wantExportHelpers: true, /* for backward compatibility - greasemonkey 2.0 era */
			});
			const unsafeWindowGetter = new sandbox.Function('return window.wrappedJSObject || window;');
			Object.defineProperty(sandbox, 'unsafeWindow', {get: unsafeWindowGetter});
			if (!this.metadata?.grant) {
				/* no grant - export all GM_* functions for compatibility */
				const apiCan = new ScriptAPI(this, sandbox, win);
				for (const apiName of Object.getOwnPropertyNames(ScriptAPI.prototype)) {
					if (apiName.startsWith("GM_")) {
						Cu.exportFunction(apiCan[apiName].bind(apiCan), sandbox, {defineAs:apiName});
					}
				}
			}
			else {
				/* otherwise export granted functions unless "@grant none" is specified */
				const apiCan = new ScriptAPI(this, sandbox, win);
				const grantedAPIs = Cu.createObjectIn(sandbox);
				for (const apiName of this.metadata.grant) {
					let targetFunc, targetScope, defineAs;
					if (apiName.startsWith("GM.")) {
						defineAs = apiName.slice(3);
						if (ScriptAPI.hasGM4Variant(apiName))
							targetFunc = apiCan[`GM4_${defineAs}`];
						else
							targetFunc = apiCan[`GM_${defineAs}`];
						targetScope = grantedAPIs;
					}
					else if (apiName.startsWith("GM_")) {
						defineAs = apiName;
						targetFunc = apiCan[apiName];
						targetScope = sandbox;
					}
					if (typeof targetFunc === "function") {
						Cu.exportFunction(targetFunc.bind(apiCan), targetScope, {defineAs});
					}
				}
				if (Object.keys(grantedAPIs).length) {
					GM_obj = grantedAPIs;
				}
			}
		}
		Object.defineProperty(sandbox, "globalThis", {
			get: () => win
		});
		Object.defineProperty(sandbox, "GM_info", {
			get: () => {
				let info = Cu.cloneInto(this.info, sandbox);
				info.isIncognito = lazy.PrivateBrowsingUtils.isContentWindowPrivate(win);
				return info;
			}
		});
		if (GM_obj) {
			Object.defineProperty(GM_obj, "info", {
				get: () => sandbox.GM_info
			});
			sandbox.GM = GM_obj;
		}

		this.evalInSandbox(sandbox);
		context.USL_run.add(this.leafName);
		context.USL_sandboxes.add(sandbox);
		UserScriptLoader.debug(`Running ${this.leafName} @ ${win.location.href}`);
	}
	evalInSandbox(aSandbox) {
		this.lineNumberOffset = new Error().lineNumber + this.requireSrc.split("\n").length + 1;
		try {
			Cu.evalInSandbox(`(function() {${this.requireSrc}\n${this.code}\n})();`, aSandbox);
		} catch (e) {
			const line = e.lineNumber - this.lineNumberOffset;
			console.error(`${this.name} / line: ${line}\n${e}`);
		}
	}
	makeTLDURL(aURL) {
		try {
			const uri = Services.io.newURI(aURL, null, null);
			uri.host = uri.host.slice(0, -Services.eTLD.getPublicSuffix(uri).length) + "tld";
			return uri.spec;
		} catch (e) {}
		return "";
	}
	isURLMatching(url) {
		if (this.disabled) return false;
		if (!this.matchPatternInitialized) {
			try {
				if (this.includeMatchPatternStrings) this.includeMatchPatternSet = new MatchPatternSet(this.includeMatchPatternStrings);
				if (this.excludeMatchPatternStrings) this.excludeMatchPatternSet = new MatchPatternSet(this.excludeMatchPatternStrings);
			} catch (e) {}
			this.matchPatternInitialized = true;
		}
		try {
			if (this.excludeMatchPatternSet?.matches(url)) return false;
			if (this.excludeRegExp?.test(url)) return false;
			
			const tldurl = (this.excludeTLD || this.includeTLD) ? this.makeTLDURL(url) : "";
			if (this.excludeTLD && tldurl) {
				if (this.excludeMatchPatternSet?.matches(tldurl)) return false;
				if (this.excludeRegExp?.test(tldurl)) return false;
			}
			if (this.includeMatchPatternSet?.matches(url)) return true;
			if (this.includeRegExp?.test(url)) return true;
			if (this.includeTLD && tldurl) {
				if (this.includeMatchPatternSet?.matches(tldurl)) return true;
				if (this.includeRegExp?.test(tldurl)) return true;
			}
		} catch (e) {}
		return false;
	}
}

const windowContexts = new WeakMap();

class UserScriptLoader {
	constructor() {
		this.cachedScripts = [];
	}
	static get DEBUG() {
		return Services.prefs.getBoolPref("UserScriptLoader.DEBUG", false);
	}
	static get disabled() {
		return Services.prefs.getBoolPref("UserScriptLoader.disabled", false);
	}
	static get CACHE_SCRIPT() {
		return Services.prefs.getBoolPref("UserScriptLoader.CACHE_SCRIPT", true);
	}
	static debug(...args) {
		if (!this.DEBUG) return;
		console.log.call(console, '[USL DEBUG]', ...args);
	}
	static getContextForWindow(win, forceCreate = false) {
		let context = windowContexts.get(win);
		if (!context && forceCreate) {
			context = {
				USL_run: new Set(),
				USL_sandboxes: new Set(),
			};
			windowContexts.set(win, context);
		}
		return context;
	}
	injectToWindow(win) {
		if (UserScriptLoader.disabled) return;
		const locationHref = win.location.href;
		if (/^(?:chrome|resource|jar|about|moz-extension):/.test(locationHref)) return;

		const excludes = [];
		const matches = [];
		for (const script of this.cachedScripts) {
			excludes.push(script.leafName);
			if (script.isURLMatching(locationHref)) {
				matches.push(script);
			}
		}
		if (matches.length) this.runScripts(matches, win);
		win.windowGlobalChild.getActor("UserScriptLoader").sendQuery("USL:GetMatchingScriptsForURL", {
			url: locationHref,
			excludes,
		}).then(scripts => {
			const activeScripts = [];
			if (Array.isArray(scripts)) {
				for (const script of scripts) {
					activeScripts.push(new ScriptEntry(script));
				}
			}
			if (activeScripts.length) {
				this.runScripts(activeScripts, win);
				if (UserScriptLoader.CACHE_SCRIPT) this.cachedScripts.push(...activeScripts);
			}
		});
	}
	runScripts(scripts, win) {
		const documentEnds = [];
		const windowLoads = [];
		const documentStarts = [];
		const documentBodies = [];
		let hasMatch = false;
		/* Scripts might be executed immediately when document.readyState is not "loading"
		   (possibly due to slow response from a parent process) since DOMContentLoaded or
		   load event is not triggered in this case */
		const documentLoaded = win.document.readyState === "complete";
		const domAnalyzed = win.document.readyState === "interactive";
		const documentElementInserted = win.document.documentElement != null;
		const documentBodyInserted = win.document.body != null;

		for (const script of scripts) {
			if ("noframes" in script.metadata && win.windowGlobalChild.browsingContext.parent) continue;
			let shouldRunWithoutEvents = false;
			hasMatch = true;

			if (documentLoaded || script.run_at === "asap") {
				shouldRunWithoutEvents = true;
			} else if (script.run_at === "document-start") {
				if (documentElementInserted) shouldRunWithoutEvents = true;
				else documentStarts.push(script);
			} else if (script.run_at === "document-body") {
				if (documentBodyInserted) shouldRunWithoutEvents = true;
				else documentBodies.push(script);
			} else if (script.run_at === "window-load") {
				windowLoads.push(script);
			} else if (domAnalyzed) {
				shouldRunWithoutEvents = true;
			} else {
				documentEnds.push(script);
			}

			if (shouldRunWithoutEvents) {
				"delay" in script ? win.setTimeout(() => script.runInWindow(win), script.delay)
				                  : script.runInWindow(win);
			}
		}
		if (hasMatch) {
			win.addEventListener("unload", event => {
				const aWin = event.currentTarget;
				const sandboxes = UserScriptLoader.getContextForWindow(aWin)?.USL_sandboxes;
				if (sandboxes?.size) {
					const timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
					timer.initWithCallback(() => {
						for (const sandbox of sandboxes) {
							Cu.nukeSandbox(sandbox);
						}
						sandboxes.clear();
					}, 0, Ci.nsITimer.TYPE_ONE_SHOT);
				}
				windowContexts.delete(aWin);
			}, {once: true});
		}
		/* Use load event for ImageDocument since DOMContentLoaded is not triggered */
		if (ImageDocument.isInstance(win.document) && documentEnds.length) {
			windowLoads.push(...documentEnds);
			documentEnds.splice(0);
		}
		if (documentEnds.length) {
			win.addEventListener("DOMContentLoaded", () => {
				for (const s of documentEnds) {
					"delay" in s ? win.setTimeout(() => s.runInWindow(win), s.delay)
					             : s.runInWindow(win);
				}
			}, {once: true});
		}
		if (windowLoads.length) {
			win.addEventListener("load", () => {
				for (const s of windowLoads) {
					"delay" in s ? win.setTimeout(() => s.runInWindow(win), s.delay)
					             : s.runInWindow(win);
				}
			}, {once: true});
		}
		if (documentStarts.length) {
			const observer = new win.MutationObserver((records, observer) => {
				if (win.document.documentElement) {
					observer.disconnect();
					for (const s of documentStarts) {
						"delay" in s ? win.setTimeout(() => s.runInWindow(win), s.delay)
						             : s.runInWindow(win);
					}
				}
			});
			observer.observe(win.document, {
				childList: true
			});
		}
		if (documentBodies.length) {
			const observer = new win.MutationObserver((records, observer) => {
				if (win.document.body) {
					observer.disconnect();
					for (const s of documentBodies) {
						"delay" in s ? win.setTimeout(() => s.runInWindow(win), s.delay)
						             : s.runInWindow(win);
					}
				}
			});
			observer.observe(documentElementInserted ? win.document.documentElement : win.document, {
				childList: true,
				subtree: !documentElementInserted
			});
		}
	}
}

class UserCallbackManager {
	constructor(actor) {
		this._callbacks = new Map();
		if ("WeakRef" in globalThis)
			this._weakActor = new WeakRef(actor);
		else
			this._actor = actor;
	}
	get actor() {
		if (this._weakActor) return this._weakActor.deref();
		return this._actor;
	}
	registerCommand(identifier, func, options) {
		this._callbacks.set(identifier, func);
		this.actor.sendAsyncMessage("USL:RegisterCommand", {identifier, options});
	}
	registerCallback(identifier, func) {
		this._callbacks.set(identifier, func);
	}
	registerPrefListener(identifier, prefUuid, name, func) {
		let listeners = this._callbacks.get(prefUuid + ":" + name);
		if (!listeners) {
			listeners = new Map();
			this._callbacks.set(prefUuid + ":" + name, listeners);
		}
		listeners.set(identifier, func);
	}
	unregisterCommand(identifier) {
		this._callbacks.delete(identifier);
		this.actor.sendAsyncMessage("USL:UnregisterCommand", {identifier});
	}
	unregisterPrefListener(identifier, prefUuid, name) {
		this._callbacks.get(prefUuid + ":" + name)?.delete(identifier);
	}
	invoke({identifier, isPrefCallback, args, eventProps}) {
		const win = this.actor.contentWindow;
		if (isPrefCallback === true) {
			this._callbacks.get(identifier)?.forEach(func => {
				func.apply(win, Cu.cloneInto(args, func));
			});
		} else {
			const func = this._callbacks.get(identifier);
			func?.call(win, eventProps ? new win.MouseEvent("command", eventProps) : void 0);
		}
	}
}

const USL = new UserScriptLoader();

export class UserScriptLoaderChild extends JSWindowActorChild {
	actorCreated() {
		this.callbackManager = new UserCallbackManager(this);
		this.pendingWindows = new WeakSet();
	}
	handleEvent(aEvent) {
		const win = this.contentWindow;
		switch (aEvent.type) {
			case "DOMWindowCreated":
				/* window.location.href is not an actual page URL when DOMWindowCreated is triggered in case
				     - window is created by a link with target="_blank" (set to "about:blank")
				     - subframe is created (set to "")
				   Then USL.injectToWindow() should be deferred until DOMDocElementInserted is triggered */
				if (!win.location.href || win.location.href === "about:blank") {
					this.pendingWindows.add(win);
				}
				else USL.injectToWindow(win);
				break;
			case "DOMDocElementInserted":
				if (this.pendingWindows.has(win)) {
					USL.injectToWindow(win);
				}
				break;
		}
	}
	receiveMessage(message) {
		switch (message.name) {
			case "USL:GetRunningScripts":
				return Promise.resolve(Array.from(UserScriptLoader.getContextForWindow(this.contentWindow)?.USL_run || []));
			case "USL:InvokeCallback":
				this.callbackManager.invoke(message.data);
				break;
			case "USL:IsUserScript": {
				const text = this.contentWindow.document.querySelector("pre")?.textContent;
				if (text) {
					return Promise.resolve(/\/\/\s*==UserScript==\s+/.test(text));
				}
				return Promise.resolve(false);
			}
			case "USL:InvalidateCache":
				USL.cachedScripts.splice(0);
				break;
			case "USL:SetScriptState":
				for (const script of USL.cachedScripts) {
					if (script.leafName === message.data.leafName) {
						script.disabled = message.data.disabled;
						break;
					}
				}
				break;
		}
	}
}
