var GESTURES = [
    // dir     : 'UDLR',
    // modifier: 'shift,ctrl,alt,meta(cmd)', //altは文字列の選択になるので実質使えない
    // name    : 'hoge'
    // obj     : 'link, textlink, text, image, file' ドロップの対象
    // cmd     : function(actor, event, info) {}
    //             actor: ドロップイベントが発生したcontent windowに対応するJSWindowActorChild
    //             info: { urls:[], texts:[], nodes:[], files:[], fname:[] }
    //               urls:link,image,fileおよびtextlinkのurlを格納
    //               texts:linkのリンクテキストやalt文字, imageのtitle,alt文字, textはRESTRICT_SELECTED_TEXTによる
    //               nodes:ドロップしたDOMノード
    //               fname:linkやimageのファイル名の候補, textはRESTRICT_SELECTED_TEXTによる
    //             関数内のthisはDragNGoModokiChild.jsm内のcommandHelpersオブジェクトを指す

    /*=== From external browser / window ===*/
    {dir:'', modifier:'',name:'新しいタブ前面に開く',obj:'link, textlink',cmd(actor,event,info){this.openUrls(actor, info.urls, 'tab', null);}},
    {dir:'', modifier:'',name:'新しいタブでGoogle検索',obj:'text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Google'], 'tab');}},

    /*=== リンク ===*/
    // does not work {dir:'U', modifier:'',name:'xpi/jarインストール',obj:'xpi,jar',cmd(actor,event,info){this.installXpi(info.urls);}},
    {dir:'U', modifier:'',name:'リンクを新しいタブ前面に開く',obj:'link, textlink',cmd(actor,event,info){this.openUrls(actor, info.urls, 'tab', null);}},
    //{dir:'D', modifier:'',name:'リンクを新しいタブ後面に開く',obj:'link, textlink',cmd(actor,event,info){this.openUrls(actor, info.urls, 'tabshifted', null);}},
    {dir:'D', modifier:'',name:'リンクを新しいタブでaguse.jp検索',obj:'link, textlink',cmd(actor,event,info){this.searchWithEngine(actor, info.urls, ['aguse.jp'], 'tab');}},
    {dir:'L', modifier:'',name:'リンクを現在のタブに開く',obj:'link, textlink',cmd(actor,event,info){this.openUrls(actor, info.urls, 'current', null);}},

    /*=== 画像 ===*/
    {dir:'U', modifier:'',name:'画像を新しいタブ前面に開く',obj:'image',cmd(actor,event,info){this.openUrls(actor, info.urls, 'tab', null);}},
    {dir:'D', modifier:'',name:'画像を新しいタブ後面に開く',obj:'image',cmd(actor,event,info){this.openUrls(actor, info.urls, 'tabshifted', null);}},
    {dir:'L', modifier:'',name:'画像を現在のタブに開く',obj:'image',cmd(actor,event,info){this.openUrls(actor, info.urls, 'current', null);}},
    {dir:'LD', modifier:'',name:'Google 類似画像検索',obj:'image',cmd(actor,event,info){var TargetImage=info.urls[0];var URL="https://lens.google.com/uploadbyurl?url="+TargetImage;if(TargetImage)this.openUrls(actor, [URL], "tab", null);}},

    /*=== Web Search ===*/
    {dir:'R', modifier:'',name:'テキストをConQueryで検索',obj:'text',cmd(actor,event,info){this.openConQueryPopup(actor, event);}},
    {dir:'UL', modifier:'',name:'テキストを現在のタブでgooウェブ検索(Green Label)',obj:'link, text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['gooウェブ検索(Green Label)'], 'current');}},
    {dir:'U', modifier:'',name:'テキストを新しいタブでGoogle検索',obj:'text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Google'], 'tab');}},
    {dir:'D', modifier:'',name:'テキストを現在のタブでGoogle検索',obj:'text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Google'], 'current');}},
    {dir:'DL', modifier:'',name:'リンクテキストを新しいタブでGoogle検索',obj:'link',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Google'], 'tab');}},
    {dir:'UL', modifier:'',name:'テキストを新しいタブでAmazon.com検索',obj:'link, text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Amazon.com'], 'tab');}},
    {dir:'UR', modifier:'',name:'テキストを新しいタブでYahoo! JAPAN検索',obj:'link, text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Yahoo! JAPAN'], 'tab');}},

    /*=== ページ内検索 ===*/
    {dir:'L', modifier:'',name:'テキストをページ内検索',obj:'link, text',cmd(actor,event,info){this.findWord(actor, info.texts[0]);}},

    /*=== クリップボード ===*/
    {dir:'UD', modifier:'',name:'リンクurl/テキストをクリップボードにコピー',obj:'text',cmd(actor,event,info){this.copyToClipboard(info.texts[0]);}},
    {dir:'LR', modifier:'',name:'リンクテキスト/テキストをクリップボードにコピー',obj:'link, text',cmd(actor,event,info){this.copyToClipboard(info.texts[0]);}},
    {dir:'UDU', modifier:'',name:'URLをクリップボードにコピー',obj:'link',cmd(actor,event,info){this.copyToClipboard(info.urls[0]);}},
    {dir:'DR', modifier:'',name:'テキストを検索バーにコピー',obj:'link, text',cmd(actor,event,info){this.copyToSearchBar(actor, info.texts[0].replace(/\n/mg,' '));}},
    {dir:'DR', modifier:'ctrl',name:'テキストを検索バーに追加コピー',obj:'link, text',cmd(actor,event,info){this.appendToSearchBar(actor, info.texts[0].replace(/\n/mg,' '));}},

    /*=== 保存 ===*/
    {dir:'RU', modifier:'',name:'リンク/画像をSaveFileModoki(SF)で保存',obj:'image, link',cmd(actor,event,info){this.openSaveFileModokiPopup(actor, event, info);}},
    //{dir:'RD', modifier:'',name:'画像をD:/hogeに保存(SF)',obj:'image',cmd(actor,event,info){this.saveWithSaveFolderModoki(actor, info.urls[0], info.texts[0], 'D:\\hoge');}},
    //{dir:'RD', modifier:'',name:'リンクを名前を付けて保存(SF)',obj:'link',cmd(actor,event,info){this.saveWithSaveFolderModoki(actor, info.urls[0], info.texts[0], null);}},
    {dir:'RD', modifier:'',name:'画像を名前を付けて保存'  ,obj:'image',cmd(actor,event,info){this.saveAs(actor, info);}},
    {dir:'RD', modifier:'',name:'リンクを名前を付けて保存',obj:'link' ,cmd(actor,event,info){this.saveAs(actor, info);}},

    /*=== テキストをえでぃたーで開く ===*/
    {dir:'DL', modifier:'',name:'テキストをエディターで開く',obj:'text',cmd(actor,event,info){this.editText(actor, null, info.texts[0]);}}, // 引数 null: view_source.editor.pathのエディターを使う

    /*=== appPathをparamsで開く, paramsはtxtで置き換えcharsetに変換される ===*/
    {dir:'U', modifier:'shift,ctrl',name:'リンクをInternet Explorerで開く',obj:'link',cmd(actor,event,info){this.launch(actor, info.urls[0], "C:\\Program Files\\Internet Explorer\\iexplore.exe",["%%URL%%"],"Shift_JIS");}},
    {dir:'R', modifier:'shift,ctrl',name:'テキストをDDwinで開く',obj:'text',cmd(actor,event,info){this.launch(actor, info.texts[0], "c:\\Program Files\\DDwin\\ddwin.exe", [",2,,G1,%%SEL%%"], "Shift_JIS");}},

    /*=== Utility ===*/
    {dir:'RDR', modifier:'',name:'Eijiro',obj:'text',cmd(actor){var TERM=actor.contentWindow.getSelection().toString();var URL="http://eow.alc.co.jp/"+TERM+"/UTF-8/";if(TERM)this.openUrls(actor, [URL], "tab", null);}},
    /* does not work
    {dir:'RDRD', modifier:'',name:'Excite で英和',obj:'text', // 要popupTranslate.uc.xul
        cmd: function(actor, event, info) {
            var UI = Cc["@mozilla.org/intl/scriptableunicodeconverter"].
            createInstance(Ci.nsIScriptableUnicodeConverter);
            UI.charset = "UTF-8";

            var text = info.texts[0];
            var engine = popupTranslate.selectEngineByDescription(UI.ConvertToUnicode("Excite 英日"));
            if (engine)
                popupTranslate.getTranslateResult(text, engine, null);
        }
    },*/
    {dir:'RLU', modifier:'',name:'選択テキスト(プロンプト)を指定ドメイン内で検索',obj:'link, text',
        cmd(actor, event, info) {
            var _document = actor.contentWindow.document;
            var p = actor.contentWindow.prompt('Input word to search under the domain(' + _document.location.hostname + '):', info.texts[0]);
            if (p)
                _document.location.href = 'http://www.google.com/search?as_qdr=y15&q=site:' +
                _document.location.href.split('/')[2] +
                ' ' + encodeURIComponent(p);
        }
    },
    {dir:'UDUD', modifier:'',name:'選択範囲をテキストファイルとして保存',obj:'text',
        cmd(actor) {
            // 選択範囲をテキストファイルとして保存する。
            var sel = actor.contentWindow.getSelection();
            if (sel && !sel.isCollapsed) {
                var fname = actor.contentWindow.location.href.match(/[^\/]+$/) + '.txt';
                fname = decodeURIComponent(fname);
                fname = fname.replace(/[\*\:\?\"\|\/\\<>]/g, '_');
                this.saveTextToLocal(actor, sel.toString(), fname, false);
            } else {
                actor.contentWindow.alert('No Selection!');
            }
        }
    },
];
