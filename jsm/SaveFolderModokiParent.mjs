const lazy = {};
ChromeUtils.defineESModuleGetters(lazy, {
    FileUtils: "resource://gre/modules/FileUtils.sys.mjs",
    PrivateBrowsingUtils: "resource://gre/modules/PrivateBrowsingUtils.sys.mjs"
});

const sfmInternal = new class {
    constructor() {
        this.useContentDispositionHelper = true;
        this.imageDoubleClickedFolder = null;
        Services.prefs.addObserver("userChrome.save", {
            observe: (aSubject, aTopic, aPrefstring) => {
                if (aTopic === "nsPref:changed") {
                    // 設定が変更された時の処理
                    sfmInternal.initPrefs(null);
                }
            },
        });
    }
    get firefoxVer() {
        return Services.appinfo.version.split(".");
    }
    initPrefs(options) {
        if (options) {
            this.useContentDispositionHelper = options.CONTENTDISPOSITION ?? this.useContentDispositionHelper;
            this.imageDoubleClickedFolder = options.IMAGE_DBLCLICK;
        }
        this.imageDoubleClickedFolder = Services.prefs.getStringPref('userChrome.save.folderOnImageDblclick', this.imageDoubleClickedFolder);
    }
    /**
     * internalSave: Used when saving a document or URL. This method:
     *  - Determines a local target filename to use (unless parameter
     *    aChosenData is non-null)
     *  - Determines content-type if possible
     *  - Prompts the user to confirm the destination filename and save mode
     *    (content-type affects this)
     *  - Creates a 'Persist' object (which will perform the saving in the
     *    background) and then starts it.
     *
     * @param aURL The String representation of the URL of the document being saved
     * @param aDocument The document to be saved
     * @param aDefaultFileName The caller-provided suggested filename if we don't
     *        find a better one
     * @param aContentDisposition The caller-provided content-disposition header
     *         to use.
     * @param aContentType The caller-provided content-type to use
     * @param aShouldBypassCache If true, the document will always be refetched
     *        from the server
     * @param aFilePickerTitleKey Alternate title for the file picker
     * @param aChosenData If non-null this contains an instance of object AutoChosen
     *        (see below) which holds pre-determined data so that the user does not
     *        need to be prompted for a target filename.
     * @param aReferrer the referrer URI object (not URL string) to use, or null
              if no referrer should be sent.
     * @param aSkipPrompt If true, the file will be saved to the default download folder.
     */
    async internalSave(window, aURL, aDocument, aDefaultFileName, aContentDisposition,
        aContentType, aShouldBypassCache, aFilePickerTitleKey,
        aChosenData, aReferrer, aSkipPrompt, aFolderName) {
        const win = window || Services.wm.getMostRecentBrowserWindow();
        if (aSkipPrompt == undefined)
            aSkipPrompt = false;

        // Note: aDocument == null when this code is used by save-link-as...
        let saveMode = win.GetSaveModeForContentType(aContentType, aDocument);
        const isDocument = aDocument != null && saveMode !== win.SAVEMODE_FILEONLY;
        let saveAsType = win.kSaveAsType_Complete;

        let file, fileURL;
        // Find the URI object for aURL and the FileName/Extension to use when saving.
        // FileName/Extension will be ignored if aChosenData supplied.
        const fileInfo = new win.FileInfo(aDefaultFileName);
        if (aChosenData)
            file = aChosenData.file;
        else {
            let charset = null;
            if (aDocument) charset = aDocument.characterSet;
            win.initFileInfo(fileInfo, aURL, charset, aDocument,
                aContentType, aContentDisposition);
            const fpParams = {
                fpTitleKey: aFilePickerTitleKey,
                isDocument: isDocument,
                fileInfo: fileInfo,
                contentType: aContentType,
                saveMode: saveMode,
                saveAsType: saveAsType,
                file: file,
                fileURL: fileURL
            };
            if (!await this.getTargetFile(win, fpParams, aSkipPrompt, aFolderName))
                // If the method returned false this is because the user cancelled from
                // the save file picker dialog.
                return;

            saveAsType = fpParams.saveAsType;
            saveMode = fpParams.saveMode;
            file = fpParams.file;
            fileURL = fpParams.fileURL;
        }

        if (!fileURL)
            fileURL = Services.io.newFileURI(file);

        // XXX We depend on the following holding true in appendFiltersForContentType():
        // If we should save as a complete page, the saveAsType is kSaveAsType_Complete.
        // If we should save as text, the saveAsType is kSaveAsType_Text.
        const useSaveDocument = isDocument &&
            (((saveMode & win.SAVEMODE_COMPLETE_DOM) && (saveAsType == win.kSaveAsType_Complete)) ||
                ((saveMode & win.SAVEMODE_COMPLETE_TEXT) && (saveAsType == win.kSaveAsType_Text)));
        // If we're saving a document, and are saving either in complete mode or
        // as converted text, pass the document to the web browser persist component.
        // If we're just saving the HTML (second option in the list), send only the URI.
        const source = useSaveDocument ? aDocument : fileInfo.uri;
        const persistArgs = {
            source: source,
            contentType: (!aChosenData && useSaveDocument &&
                    saveAsType === win.kSaveAsType_Text) ?
                "text/plain" : null,
            target: fileURL,
            postData: isDocument ? win.getPostData() : null,
            bypassCache: aShouldBypassCache
        };

        const persist = win.makeWebBrowserPersist();

        // Calculate persist flags.
        const nsIWBP = Ci.nsIWebBrowserPersist;
        const flags = nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES;
        if (aShouldBypassCache)
            persist.persistFlags = flags | nsIWBP.PERSIST_FLAGS_BYPASS_CACHE;
        else
            persist.persistFlags = flags | nsIWBP.PERSIST_FLAGS_FROM_CACHE;

        // Leave it to WebBrowserPersist to discover the encoding type (or lack thereof):
        persist.persistFlags |= nsIWBP.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;

        try {
            var isPrivate = lazy.PrivateBrowsingUtils.isWindowPrivate(win);
        } catch (e) {
            isPrivate = false;
        }
        // Create download and initiate it (below)
        const [firefoxVer, firefoxMinorVer] = this.firefoxVer;
        const tr = Cc["@mozilla.org/transfer;1"].createInstance(Ci.nsITransfer);
        let referrerInfo = null;
        if (aReferrer instanceof Ci.nsIReferrerInfo) {
            referrerInfo = aReferrer;
        } else {
            referrerInfo = Cc["@mozilla.org/referrer-info;1"].createInstance(Ci.nsIReferrerInfo);
            referrerInfo.init(
                Ci.nsIHttpChannel.REFERRER_POLICY_NO_REFERRER_WHEN_DOWNGRADE,
                true,
                aReferrer
            );
        }

        if (useSaveDocument) {
            // Saving a Document, not a URI:
            let filesFolder = null;
            if (persistArgs.contentType !== "text/plain") {
                // Create the local directory into which to save associated files.
                filesFolder = file.clone();

                const nameWithoutExtension = win.getFileBaseName(filesFolder.leafName);
                const filesFolderLeafName = win.ContentAreaUtils.stringBundle
                    .formatStringFromName("filesFolder", [nameWithoutExtension], 1);

                filesFolder.leafName = filesFolderLeafName;
            }

            let encodingFlags = 0;
            if (persistArgs.contentType === "text/plain") {
                encodingFlags |= nsIWBP.ENCODE_FLAGS_FORMATTED;
                encodingFlags |= nsIWBP.ENCODE_FLAGS_ABSOLUTE_LINKS;
                encodingFlags |= nsIWBP.ENCODE_FLAGS_NOFRAMES_CONTENT;
            } else {
                encodingFlags |= nsIWBP.ENCODE_FLAGS_ENCODE_BASIC_ENTITIES;
            }

            const kWrapColumn = 80;
            tr.init((aChosenData ? aChosenData.uri : fileInfo.uri), null,
                persistArgs.target, "", null, null, null, persist, isPrivate, Ci.nsITransfer.DOWNLOAD_ACCEPTABLE, referrerInfo);
            try {
                persist.progressListener = new win.DownloadListener(win, tr);
            } catch (e) {
                persist.progressListener = tr;
            }
            persist.saveDocument(persistArgs.source, persistArgs.target, filesFolder,
                persistArgs.contentType, encodingFlags, kWrapColumn);
        } else {
            tr.init((aChosenData ? aChosenData.uri : source), null,
                persistArgs.target, "", null, null, null, persist, isPrivate, Ci.nsITransfer.DOWNLOAD_ACCEPTABLE, referrerInfo);
            try {
                persist.progressListener = new win.DownloadListener(win, tr);
            } catch (e) {
                persist.progressListener = tr;
            }
            let privacyContext = win.docShell.QueryInterface(Ci.nsILoadContext);
            const targetURI = (aChosenData ? aChosenData.uri : source);
            const principal = Services.scriptSecurityManager.createContentPrincipal(targetURI, {});
            let saveURIArgs = [
                targetURI, principal, null, referrerInfo, null,
                persistArgs.postData, null, persistArgs.target,
                Ci.nsIContentPolicy.TYPE_SAVEAS_DOWNLOAD, isPrivate
            ];
            persist.saveURI(...saveURIArgs);
        }
    }
    async getTargetFile(win, aFpP, aSkipPrompt, aFolderPath) {
        const prefs = Services.prefs.getBranch("browser.download.");
        const nsIFile = Ci.nsIFile;

        // For information on download folder preferences, see
        // mozilla/browser/components/preferences/main.js

        let useDownloadDir = prefs.getBoolPref("useDownloadDir");
        let dir = null;

        // Default to lastDir if useDownloadDir is false, and lastDir
        // is configured and valid. Otherwise, use the user's default
        // downloads directory configured through download prefs.
        //フォルダパスが設定されているなら, dirとしてセットする。
        //aSkipPromptがtrueならファイルピッカを起動しないようにuseDownloadDirをtrueにしておく
        if (aFolderPath) {
            try {
                dir = new lazy.FileUtils.File(aFolderPath);
                if (dir && !dir.exists())
                    dir = null;
                if (aSkipPrompt)
                    useDownloadDir = true;
            } catch (ex) {}
        }
        //
        if (!dir) {
            const defaultPath = await ChromeUtils.importESModule("resource://gre/modules/Downloads.sys.mjs").Downloads.getPreferredDownloadsDirectory();
            const userDownloadsDirectory = new lazy.FileUtils.File(defaultPath);
            try {
                var lastDir = prefs.getComplexValue("lastDir", nsIFile);
                if ((!aSkipPrompt || !useDownloadDir) && lastDir.exists())
                    dir = lastDir;
                else
                    dir = userDownloadsDirectory;
            } catch (ex) {
                dir = userDownloadsDirectory;
            }
        }
        if (!aSkipPrompt || !useDownloadDir || !dir || (dir && !dir.exists())) {
            if (!dir || (dir && !dir.exists())) {
                // Default to desktop.
                dir = Services.dirsvc.get("Desk", nsIFile);
            }

            const fp = Cc["@mozilla.org/filepicker;1"].createInstance(Ci.nsIFilePicker);
            const titleKey = aFpP.fpTitleKey || "SaveLinkTitle";
            if (this.firefoxVer[0] >= 125) fp.init(win.browsingContext, win.ContentAreaUtils.stringBundle.GetStringFromName(titleKey),
                Ci.nsIFilePicker.modeSave);
            else fp.init(win, win.ContentAreaUtils.stringBundle.GetStringFromName(titleKey),
                Ci.nsIFilePicker.modeSave);

            fp.defaultExtension = aFpP.fileInfo.fileExt;
            fp.defaultString = aFpP.fileInfo.fileName;
            win.appendFiltersForContentType(fp, aFpP.contentType, aFpP.fileInfo.fileExt,
                aFpP.saveMode);

            if (dir)
                fp.displayDirectory = dir;

            if (aFpP.isDocument) {
                try {
                    fp.filterIndex = prefs.getIntPref("save_converter_index");
                } catch (e) {}
            }

            return await new Promise(resolve => {
                fp.open(rv => {
                    if (rv === Ci.nsIFilePicker.returnCancel || !fp.file) {
                        resolve(false);
                        return;
                    }

                    const directory = fp.file.parent.QueryInterface(nsIFile);
                    prefs.setComplexValue("lastDir", nsIFile, directory);

                    fp.file.leafName = win.validateFileName(fp.file.leafName);
                    aFpP.saveAsType = fp.filterIndex;
                    aFpP.file = fp.file;
                    aFpP.fileURL = fp.fileURL;

                    if (aFpP.isDocument)
                        prefs.setIntPref("save_converter_index", aFpP.saveAsType);
                    resolve(true);
                });
            });
        } else {
            dir.append(aFpP.fileInfo.fileName);
            const file = dir;

            // Since we're automatically downloading, we don't get the file picker's
            // logic to check for existing files, so we need to do that here.
            //
            // Note - this code is identical to that in
            //   mozilla/toolkit/mozapps/downloads/src/nsHelperAppDlg.js.in
            // If you are updating this code, update that code too! We can't share code
            // here since that code is called in a js component.
            let collisionCount = 0;
            while (file.exists()) {
                collisionCount++;
                if (collisionCount === 1) {
                    // Append "(2)" before the last dot in (or at the end of) the filename
                    // special case .ext.gz etc files so we don't wind up with .tar(2).gz
                    if (file.leafName.match(/\.[^\.]{1,3}\.(gz|bz2|Z)$/i))
                        file.leafName = file.leafName.replace(/\.[^\.]{1,3}\.(gz|bz2|Z)$/i, "(2)$&");
                    else
                        file.leafName = file.leafName.replace(/(\.[^\.]*)?$/, "(2)$&");
                } else {
                    // replace the last (n) in the filename with (n+1)
                    file.leafName = file.leafName.replace(/^(.*\()\d+\)/, "$1" + (collisionCount + 1) + ")");
                }
            }
            aFpP.file = file;
        }

        return true;
    }
    //Documentlinkの保存
    saveDocFolder(win, afolder) {
        win.gBrowser.selectedBrowser.frameLoader.startPersistence(null, {
            onDocumentReady(aDocument) {
                if (!aDocument || !(aDocument instanceof Ci.nsIWebBrowserPersistDocument))
                    throw new Error("Must have an nsIWebBrowserPersistDocument!");
                sfmInternal.internalSave(win, aDocument.documentURI, aDocument, null, aDocument.contentDisposition,
                    aDocument.contentType, false, null, null,
                    aDocument.referrerInfo, !afolder ? false : true, afolder);
            },
            onError(status) {
                throw new Components.Exception("saveBrowser failed asynchronously in startPersistence",
                                               status, Components.stack.caller);
            }
        });
    }
    //画像urlの保存
    saveImageURL(aURL, aFileName, aFilePickerTitleKey, aShouldBypassCache,
        aSkipPrompt, aReferrer, aFolder, aDoc, aContentType = null, aContentDisposition = null) {
        const win = aDoc?.defaultView;
        const imgICache = Ci.imgICache;
        const nsISupportsCString = Ci.nsISupportsCString;
        let contentType = aContentType;
        let contentDisposition = aContentDisposition;

        if (!aShouldBypassCache && aDoc instanceof HTMLDocument && contentType == null && contentDisposition == null) {
            try {
                const imageCache = Cc["@mozilla.org/image/tools;1"]
                    .getService(Ci.imgITools)
                    .getImgCacheForDocument(aDoc);
                const props =
                    imageCache.findEntryProperties(Services.io.newURI(aURL, win.getCharsetforSave(aDoc)), aDoc);
                if (props) {
                    contentType = props.get("type", nsISupportsCString).data;
                    contentDisposition = props.get("content-disposition", nsISupportsCString).data;
                }
            } catch (e) {
                // Failure to get type and content-disposition off the image is non-fatal
            }
        }
        if (aFolder) {
            this.internalSave(win, aURL, null, aFileName, contentDisposition, contentType,
                aShouldBypassCache, aFilePickerTitleKey, null, aReferrer, aSkipPrompt, aFolder);
        } else {
            this.internalSave(win, aURL, null, aFileName, contentDisposition, contentType,
                aShouldBypassCache, aFilePickerTitleKey, null, aReferrer, false);
        }
    }
    //リンク等の保存
    saveLinkFolder(win, property, afolder) {
        const referrer = property.referrerInfo || property.documentURIObject;
        const helper = new ContentDispositionHelper(win, property.linkURL, property.linkTextStr, null, true,
                                   afolder ? true : false, referrer, afolder);
        helper.saveURL();
    }
    //画像の保存
    saveImageFolder(win, property, afolder) {
        const url = property.mediaURL;
        if (!url) return;
        const text = property.imageText;
        const referrer = property.referrerInfo || property.documentURIObject;

        let aShouldBypassCache = false;
        if (/^data:/.test(url)) {
            text = "index.png";
            // Bypass cache, since it's a data: URL.
            aShouldBypassCache = true;
        }

        this.saveImageURL(url, text, "SaveImageTitle", aShouldBypassCache,
            afolder ? true : false, referrer, afolder, win?.document,
            property.contentType, property.contentDisposition);
    }
    //イメージおよびリンク用ポップアップアップ
    makePopup(menupopup, folders, contentProperty) {
        const doc = menupopup.ownerDocument;
        if (contentProperty.onImage) {
            //フォルダに画像を保存
            for (const folder of folders) {
                if (!folder) continue;
                const menuitem = doc.createXULElement("menuitem");
                menuitem.setAttribute("label", "Save Image to " + folder);
                menuitem.addEventListener("command", () => {
                    this.saveImageFolder(doc.defaultView, contentProperty, folder);
                });
                menupopup.appendChild(menuitem);
            }
            //フォルダを選択して画像を保存
            const menuitem = doc.createXULElement("menuitem");
            menuitem.setAttribute("label", 'Save Image As...');
            menuitem.addEventListener("command", () => {
                this.saveImageFolder(doc.defaultView, contentProperty, null);
            });
            menupopup.appendChild(menuitem);
            if (contentProperty.onLink) {
                //セパセータ
                const menusepa = doc.createXULElement("menuseparator");
                menupopup.appendChild(menusepa);
            }
        }
        if (contentProperty.onLink) {
            //フォルダにリンクを保存
            for (const folder of folders) {
                if (!folder) continue;
                const menuitem = doc.createXULElement("menuitem");
                menuitem.setAttribute("label", "Save Link to " + folder);
                menuitem.addEventListener("command", () => {
                    this.saveLinkFolder(doc.defaultView, contentProperty, folder);
                });
                menupopup.appendChild(menuitem);
            }
            //フォルダを選択してリンクを保存
            const menuitem = doc.createXULElement("menuitem");
            menuitem.setAttribute("label", 'Save Link As...');
            menuitem.addEventListener("command", () => {
                this.saveLinkFolder(doc.defaultView, contentProperty, null);
            });
            menupopup.appendChild(menuitem);
        }
    }
}();

export class SaveFolderModoki {
    constructor(win, options) {
        if ("WeakRef" in globalThis)
            this._weakWin = new WeakRef(win);
        else
            this._win = win;
        win.document.getElementById("menu_ToolsPopup")?.insertBefore(win.MozXULElement.parseXULToFragment(`
            <menu id="SaveFolderToolsMenu" label="Save Folder" accesskey="S">
                <menupopup>
                    <menuitem label="保存するフォルダの登録"
                              accesskey="S"/>
                    <menuseparator/>
                    <menuitem label="保存するフォルダの修正"
                              accesskey="a"/>
                </menupopup>
            </menu>
        `), win.document.getElementById("menu_preferences"));
        win.document.getElementById("contentAreaContextMenu")?.appendChild(win.MozXULElement.parseXULToFragment(`
            <menu label="Save Folder Modoki" accesskey="S">
                <menupopup id="saveFolderModokiContextMenu"/>
            </menu>
        `));
        win.document.getElementById("SaveFolderToolsMenu")?.addEventListener("command", event => {
            let key = event.target.getAttribute("accesskey");
            if (key === "S") this.addFolderPath();
            else if (key === "a") this.editFolderPath();
        });
        win.document.getElementById("saveFolderModokiContextMenu")?.addEventListener("popupshowing", event => {
            this.popupContextMenu(event.currentTarget);
            event.stopPropagation();
        });
        sfmInternal.initPrefs(options);
        win.addEventListener("unload", ({currentTarget}) => {
            delete this._win;
            delete currentTarget.saveFolderModoki;
        }, {once: true});
    }
    get window() {
        if (this._weakWin) return this._weakWin.deref();
        return this._win;
    }
    get document() {
        if (this._weakWin) return this._weakWin.deref()?.document;
        return this._win.document;
    }
    //メニュー用
    //保存用フォルダを新規追加する
    addFolderPath() {
        const fp = this.getFolderPath(fp => {
            let folders = Services.prefs.getStringPref('userChrome.save.folders', '') + ',';
            if (fp && fp.file) {
                if ((',' + folders).indexOf(',' + fp.file.path + ',') < 0) {
                    folders = folders + fp.file.path;
                    Services.prefs.setStringPref('userChrome.save.folders', folders);
                }
            }
        });
    }
    //保存用フォルダを編集する
    editFolderPath() {
        let folders = Services.prefs.getStringPref('userChrome.save.folders', '');
        folders = this.window.prompt('Edit Folders', folders);
        if (folders)
            Services.prefs.setStringPref('userChrome.save.folders', folders);
    }
    //保存用フォルダをファイルピッカにより選択する
    getFolderPath(callback) {
        const nsIFilePicker = Ci.nsIFilePicker;
        const fp = Cc["@mozilla.org/filepicker;1"]
            .createInstance(nsIFilePicker);
        if (sfmInternal.firefoxVer[0] >= 125) fp.init(this.window.browsingContext, "Select a Folder", nsIFilePicker.modeGetFolder);
        else fp.init(this.window, "Select a Folder", nsIFilePicker.modeGetFolder);
        const file = Services.dirsvc.get("Desk", Ci.nsIFile);
        fp.displayDirectory = file;
        fp.open(rv => {
            if (rv === nsIFilePicker.returnOK) {
                callback(fp);
            } else
                callback(null);
        });
    }
    //コンテキストメニューポップアップ
    popupContextMenu(popup) {
        const win = popup.ownerGlobal;
        //メニュー構築
        const folders = Services.prefs.getStringPref('userChrome.save.folders', '').split(',');

        const menupopup = win.document.getElementById('saveFolderModokiContextMenu');
        while (menupopup.lastChild) {
            menupopup.removeChild(menupopup.lastChild);
        }

        for (const folder of folders) {
            if (!folder) continue;
            const menuitem = win.document.createXULElement("menuitem");
            menuitem.setAttribute("label", "Save Document to " + folder);
            menuitem.addEventListener("command", () => {
                sfmInternal.saveDocFolder(win, folder);
            });
            menupopup.appendChild(menuitem);
        }
        //フォルダを選択して保存
        const menuitem = win.document.createXULElement("menuitem");
        menuitem.setAttribute("label", 'Save Document As...');
        menuitem.addEventListener("command", () => {
            sfmInternal.saveDocFolder(win, null);
        });
        menupopup.appendChild(menuitem);
        if (win.gContextMenu.onImage || win.gContextMenu.onLink) {
            //セパセータ
            const menusepa = win.document.createXULElement("menuseparator");
            menupopup.appendChild(menusepa);
        }
        //イメージおよびリンク用ポップアップアップ をappendする
        const {contentData} = win.gContextMenu;
        sfmInternal.makePopup(menupopup, folders, {
            onImage: win.gContextMenu.onImage,
            onLink: win.gContextMenu.onLink,
            mediaURL: win.gContextMenu.mediaURL,
            linkURL: win.gContextMenu.linkURL,
            imageText: win.gContextMenu.imageInfo ? win.gContextMenu.imageInfo.imageText : "",
            linkTextStr: win.gContextMenu.linkTextStr,
            contentType: contentData.contentType,
            contentDisposition: contentData.contentDisposition,
            documentURIObject: contentData.documentURIObject,
            referrerInfo: win.gContextMenu.onLink ? contentData.linkReferrerInfo : contentData.referrerInfo,
        });
    }
    //urlの保存 DragNgoModoki.uc.js から呼び出される
    saveLink(url, text, afolder) {
        const helper = new ContentDispositionHelper(this.window, url, text, null, true,
            afolder ? true : false, this.window.gBrowser.selectedBrowser.currentURI, afolder);
        helper.saveURL();
    }
    //rebuild_userChrome.uc.xulから呼び出される
    directSaveLink(dummy, aURL, aDefaultFileName, aDocumentURI, aFolder) {
        sfmInternal.internalSave(this.window, aURL, null, aDefaultFileName, null, null, false,
            null, null, aDocumentURI, true, aFolder.path);
    }
    //パス文字列からnsIFileを返す rebuild_userChrome.uc.xul から呼び出される
    initFileWithPath(aPath) {
        return new lazy.FileUtils.File(aPath);
    }
    //D&Dホットメニュー DragNgoModoki.uc.js から呼び出される
    showHotMenu(x, y, property) {
        let hotmenu = this.document.getElementById("saveFolderModokihotmenu");
        if (!hotmenu) {
            hotmenu = this.document.createXULElement("menupopup");
            hotmenu.setAttribute("id", "saveFolderModokihotmenu");
            this.document.getElementById("mainPopupSet").appendChild(hotmenu);
        }
        while (hotmenu.lastChild) {
            hotmenu.removeChild(hotmenu.lastChild);
        }

        //ポップアップメニュー構築
        const folders = Services.prefs.getStringPref('userChrome.save.folders', '').split(',');
        sfmInternal.makePopup(hotmenu, folders, property);
        hotmenu.openPopupAtScreen(x, y, true);
    }
}

class ContentDispositionHelper {
    constructor(win, aURL, aFileName, aFilePickerTitleKey, aShouldBypassCache,
        aSkipPrompt, aReferrer, aFolder) {
        this.url = aURL;
        this.fileName = aFileName;
        this.shouldBypassCache = aShouldBypassCache;
        this.filePickerTitleKey = aFilePickerTitleKey;
        this.referrer = aReferrer;
        this.skipPrompt = aSkipPrompt;
        this.folder = aFolder;
        this.window = win;
        this.timer = null;
        this.contentDisposititon = null;
        this.contentType = null;
    }
    save() {
        if (this.timer) {
            this.timer.cancel();
            this.timer = null;
        }
        sfmInternal.internalSave(this.window, this.url, null, this.fileName, this.contentDisposititon, this.contentType,
            this.shouldBypassCache, this.filePickerTitleKey, null, this.referrer, this.skipPrompt, this.folder)
    }
    saveURL() {
        if (!sfmInternal.useContentDispositionHelper) {
            this.save();
            return;
        }
        try {
            const timeout = 15000;
            const http_request = new XMLHttpRequest();
            http_request.withCredentials = true;
            http_request.setOriginAttributes({
                userContextId: this.window.gBrowser.contentPrincipal.userContextId,
                privateBrowsingId: this.window.gBrowser.contentPrincipal.privateBrowsingId,
            });
            // ******  if reponse recieved and finnished
            http_request.onreadystatechange = () => {
                if (http_request.readyState === 4) {
                    // ****** get "Content-disposition" and "Content-Type"
                    let disposititon = http_request.getResponseHeader("Content-disposition");
                    let type = http_request.getResponseHeader("Content-Type");
                    if (!disposititon) disposititon = null;
                    if (!type) type = null;
                    // ****** don't include characterset, better matching
                    if ((/text\/html/).test(type)) type = "text/html";
                    // ****** default to .htm for links instead of .com, .asp, .php, etc
                    if (disposititon == null) {
                        this.fileName += ".htm";
                    }
                    this.contentDisposititon = disposititon;
                    this.contentType = type;
                    // ****** clear timer
                    if (this.timer) {
                        this.timer.cancel();
                        this.timer = null;
                        this.save();
                    }
                }
            };
            // ******  send request, asyncronous true, works in background
            http_request.open("HEAD", this.url, true);
            http_request.send(null);
            // ****** set reponse timeout
            this.timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
            this.timer.initWithCallback(() => {
                this.timer = null;
                this.save();
            }, timeout, Ci.nsITimer.TYPE_ONE_SHOT);

        } catch (e) {
            this.save();
        }
    }
}

export class SaveFolderModokiParent extends JSWindowActorParent {
    receiveMessage(message) {
        let browser = this.browsingContext.top.embedderElement;
        let win = browser.ownerGlobal;
        switch (message.name) {
            case "SFM:DoubleClickDetectedOnImage": {
                win.document.getElementById("contentAreaContextMenu").hidePopup();
                let aFolder = sfmInternal.imageDoubleClickedFolder;
                if (!aFolder) aFolder = null;
                message.data.documentURIObject = Services.io.newURI(message.data.docLocation, message.data.charSet);
                sfmInternal.saveImageFolder(win, message.data, aFolder);
                break;
            }
        }
    }
}