const config = {
    RESTRICT_SELECTED_TEXT: true, //textは選択文字列のみ:true, ドロップした文字列(リンク等はurl):false
    MESSAGE_UNKNOWN_GESTURE: '未定義',
    RESET_GESTURE: "RDLU",
    EXTERNAL_GESTURE_URL: "resource://dng-ucjs/DNGGestureDefinitions.js",
    // ここにGESTURESを定義した場合ジェスチャ定義にはGESTURESが使われる
    // そうでない場合は上のEXTERNAL_GESTURE_URLに指定したファイルから読まれる
    /*
    GESTURES: [
        {dir:'U', modifier:'',name:'テキストを新しいタブでGoogle検索',obj:'text',cmd(actor,event,info){this.searchWithEngine(actor, info.texts, ['Google'], 'tab');}},
    ]
    */
}

const contexts = new WeakMap();

const lazy = {};
ChromeUtils.defineLazyGetter(lazy, "emptyMatchPatternSet", () => new MatchPatternSet([]));

/* Obtained from utilityOverlay.js */
function gatherTextUnder(root) {
  var text = "";
  var node = root.firstChild;
  var depth = 1;
  while (node && depth > 0) {
    // See if this node is text.
    if (node.nodeType == Node.TEXT_NODE) {
      // Add this text to our collection.
      text += " " + node.data;
    } else if (HTMLImageElement.isInstance(node)) {
      // If it has an "alt" attribute, add that.
      var altText = node.getAttribute("alt");
      if (altText && altText != "") {
        text += " " + altText;
      }
    }
    // Find next node to test.
    // First, see if this node has children.
    if (node.hasChildNodes()) {
      // Go to first child.
      node = node.firstChild;
      depth++;
    } else {
      // No children, try next sibling (or parent next sibling).
      while (depth > 0 && !node.nextSibling) {
        node = node.parentNode;
        depth--;
      }
      if (node.nextSibling) {
        node = node.nextSibling;
      }
    }
  }
  // Strip leading and tailing whitespace.
  text = text.trim();
  // Compress remaining whitespace.
  text = text.replace(/\s+/g, " ");
  return text;
}

/* Glue functions for gesture commands */
const commandHelpers = {
    searchWithEngine(actor, texts, engines, where, addHistoryEntry) {
        const messageData = {
            command: "searchWithEngine",
            args: [texts, engines, where, addHistoryEntry]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    openUrls(actor, urls, where, referrer) {
        let messageData = {
            command: "openUrls",
            args: [urls, where, referrer]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    openSaveFileModokiPopup(actor, event, info) {
        let i = 0;
        const doc = event.dataTransfer.mozSourceNode?.ownerDocument || event.target.ownerDocument;
        const messageData = {
            command: "openSaveFileModokiPopup",
            args: [{
                screenX: event.screenX,
                screenY: event.screenY,
                onImage: false,
                onLink: false,
                mediaURL: "",
                linkURL: "",
                imageText: "",
                linkTextStr: "",
                docLocation: doc.location.href,
                charSet: doc.characterSet,
                contentType: null,
                contentDisposition: null
            }]
        }
        for (const node of info.nodes) {
            const url = info.urls[i];
            const text = info.texts[i++];
            if (HTMLAnchorElement.isInstance(node)) {
                messageData.args[0].onLink = true;
                messageData.args[0].linkURL = url;
                messageData.args[0].linkTextStr = text;
            } else {
                let imageInfo = this.getContentTypeAndDisposition(node);
                messageData.args[0].onImage = true;
                messageData.args[0].mediaURL = url;
                messageData.args[0].imageText = text;
                messageData.args[0].contentType = imageInfo.contentType;
                messageData.args[0].contentDisposition = imageInfo.contentDisposition;
            }
        }
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    saveWithSaveFolderModoki(actor, url, text, folder) {
        const messageData = {
            command: "saveWithSaveFolderModoki",
            args: [url, text, folder]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    copyToClipboard(text) {
        Cc["@mozilla.org/widget/clipboardhelper;1"].getService(Ci.nsIClipboardHelper).copyString(text);
    },
    copyToSearchBar(actor, searchText) {
        const messageData = {
            command: "copyToSearchBar",
            args: [searchText]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    appendToSearchBar(actor, searchText) {
        const messageData = {
            command: "appendToSearchBar",
            args: [searchText]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    findWord(actor, word) {
        const messageData = {
            command: "findWord",
            args: [word]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    editText(actor, editor, text) {
        const messageData = {
            command: "editText",
            args: [editor, text]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    saveTextToLocal(actor, text, fpath, skipPrompt) {
        const messageData = {
            command: "saveTextToLocal",
            args: [text, fpath, skipPrompt]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    openConQueryPopup(actor, event) {
        const win = actor.contentWindow;
        const sel = win.getSelection();
        if (!sel.toString()) {
            const sourceNode = event.dataTransfer.mozSourceNode;
            if (HTMLAnchorElement.isInstance(sourceNode)) {
                const range = win.document.createRange()
                range.selectNodeContents(sourceNode)
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
        const messageData = {
            command: "openConQueryPopup",
            args: [{screenX: event.screenX, screenY: event.screenY}]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    launch(actor, txt, appPath, params, charset) {
        const messageData = {
            command: "launch",
            args: [txt, appPath, params, charset]
        };
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    saveAs(actor, info) {
        const url = info.urls[0];
        const text = info.texts[0];
        const node = info.nodes[0];
        const imageInfo = this.getContentTypeAndDisposition(node);
        const messageData = {
            command: "saveAs",
            args: [url, text, node.ownerDocument.location.href, null,
                   imageInfo.contentType, imageInfo.contentDisposition]
        }
        actor.sendAsyncMessage("DNG:ExecuteCommand", messageData);
    },
    getContentTypeAndDisposition(node) {
        let contentType = null;
        let contentDisposition = null;
        if (node instanceof Ci.nsIImageLoadingContent) {
            let doc = node.ownerDocument;
            try {
                let imageCache = Cc["@mozilla.org/image/tools;1"].getService(Ci.imgITools)
                    .getImgCacheForDocument(doc);
                let props = imageCache.findEntryProperties(node.currentURI, doc);
                try {
                    contentType = props.get("type", Ci.nsISupportsCString).data;
                } catch (e) {}
                try {
                    contentDisposition = props.get("content-disposition", Ci.nsISupportsCString).data;
                } catch (e) {}
            } catch (e) {}
        }
        return {contentType, contentDisposition};
    },
}

class DragObjectInfo {
    constructor() {
        this.urls = [];
        this.texts = [];
        this.nodes = [];
        this.files = [];
        this.fname = [];
    }
    addEntry(url, text, node, file, fname, cacheInfo) {
        this.urls.push(url);
        this.texts.push(text);
        this.nodes.push(node);
        this.files.push(file);
        this.fname.push(fname);
        if (cacheInfo?.dict) {
            let entry = cacheInfo.dict.get(cacheInfo.name);
            if (!entry) {
                entry = {
                    urls: [],
                    texts: [],
                    nodes: [],
                    files: [],
                    fname: []
                };
                cacheInfo.dict.set(cacheInfo.name, entry);
            }
            entry.urls.push(url);
            entry.texts.push(text);
            entry.nodes.push(node);
            entry.files.push(file);
            entry.fname.push(fname);
        }
    }
}

const DragNGo = new class {
    constructor() {
        if (config.GESTURES) this._gestures = config.GESTURES;
    }
    get GESTURES() {
        if (!this._gestures) this.initGestures();
        return this._gestures;
    }
    initGestures() {
        const sandbox = new Cu.Sandbox(Services.scriptSecurityManager.getSystemPrincipal(), {
            wantComponents: false,
        });
        try {
            Services.scriptloader.loadSubScriptWithOptions(config.EXTERNAL_GESTURE_URL, {
                target: sandbox,
                //ignoreCache: true,
            });
            this._gestures = sandbox.GESTURES || [];
        } catch (e) {
            if (!this._gestures) this._gestures = [];
            console.error(e);
        }
    }
    //選択文字列を得る
    getSelection(win) {
        const sel = win.getSelection();
        if (sel && !sel.toString()) {
            const node = Services.focus.focusedElement;
            if (node &&
                ((typeof node.mozIsTextField === 'function' && node.mozIsTextField(true)) ||
                 node.type === "search" || node.type === "text" || node.type === "textarea") &&
                'selectionStart' in node && node.selectionStart !== node.selectionEnd) {
                const offsetStart = Math.min(node.selectionStart, node.selectionEnd);
                const offsetEnd = Math.max(node.selectionStart, node.selectionEnd);
                return node.value.substr(offsetStart, offsetEnd - offsetStart);
            }
        }
        return sel?.toString() ?? "";
    }
    // D&Dの方向を得る
    getDirection(event, actor) {
        const context = contexts.get(event.target.ownerGlobal);
        // 認識する最小のマウスの動き
        const tolerance_x = context.directionChain === "" ? 10 /*30*/ : 10;
        const tolerance_y = context.directionChain === "" ? 10 /*30*/ : 10;
        const x = event.screenX;
        const y = event.screenY;

        if (context.lastX == null) {
            context.lastX = x;
            context.lastY = y;
            return context.directionChain;
        }
        // 直前の座標と比較, 移動距離が極小のときは無視する
        const distanceX = Math.abs(x - context.lastX);
        const distanceY = Math.abs(y - context.lastY);
        if (distanceX < tolerance_x && distanceY < tolerance_y)
            return context.directionChain;

        // 方向の決定
        let direction;
        if (distanceX * 1.5 >= distanceY)
            direction = x < context.lastX ? "L" : "R";
        else if (distanceX * 1.5 < distanceY)
            direction = y < context.lastY ? "U" : "D";
        else {
            context.lastX = x;
            context.lastY = y;
            return context.directionChain;
        }
        // 前回の方向と比較して異なる場合はdirectionChainに方向を追加
        const lastDirection = context.directionChain.slice(-1);
        if (direction !== lastDirection) {
            context.directionChain += direction;
        }
        // 今回の位置を保存
        context.lastX = x;
        context.lastY = y;

        //directionChainの最後が RDLU ならdirectionChainをリセットする
        if (context.directionChain.endsWith(config.RESET_GESTURE)) {
            context.directionChain = '';
            this.setStatusMessage(actor, '', 0);
            if (!config.GESTURES) actor.sendAsyncMessage("DNG:ReloadGestureDefinitions");
            return context.directionChain;
        }

        return context.directionChain;
    }
    getElementsByXPath(aXPath, aContextNode) {
        try {
            const xpe = new XPathEvaluator();
            const resolver = xpe.createNSResolver(aContextNode.ownerDocument?.documentElement || aContextNode.documentElement);
            const result = xpe.evaluate(aXPath, aContextNode, resolver, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
            const arr = [];
            for (let i = 0; i < result.snapshotLength; i++) {
                arr.push(result.snapshotItem(i));
            }
            return arr;
        } catch (e) {console.error(e);}
        return [];
    }
    isParentEditableNode(node) {
        //if (node.ownerDocument.designMode == 'on')
        //  return node;
        while (node) {
            if (/input|textarea/.test(node.localName))
                return node;
            if (node.isContentEditable || node.contentEditable === 'true')
                return node;
            node = node.parentNode;
        }
        return null;
    }
    getSupportedFlavoursForNode(node) {
        if (HTMLInputElement.isInstance(node) && node.type === "file") {
            return ["application/x-moz-file"];
        } else if (HTMLInputElement.isInstance(node) && node.type === "text" ||
            HTMLTextAreaElement.isInstance(node)) {
            return [
                "text/uri-list",
                "text/html",
                "text/plain",
                "text/unicode"
            ];
        } else {
            return [
                "application/x-moz-node",
                "text/x-moz-url",
                "text/uri-list",
                "text/html",
                "text/plain",
                "text/unicode"
            ];
        }
    }
    // キーチェック
    keyCheck(event, gesture) {
        const keys = gesture.modifier.replace(/\s+/g, '').toLowerCase().split(',');
        let flg = 0
        if (event.altKey)
            flg += 1;
        if (event.shiftKey)
            flg += 2;
        if (event.ctrlKey)
            flg += 4;
        if (event.metaKey)
            flg += 8;
        let flg1 = 0;
        for (const key of keys) {
            switch (key) {
                case 'alt':
                    flg1 += 1;
                    break;
                case 'shift':
                    flg1 += 2;
                    break;
                case 'ctrl':
                    flg1 += 4;
                    break;
                case 'meta':
                case 'cmd':
                    flg1 += 8;
                    break;
            }
        }
        return (flg === flg1);
    }
    matchCheck(event, gesture) {
        if (!gesture.match) return true;
        if (!(gesture.match instanceof MatchPatternSet)) {
            if (Array.isArray(gesture.match) && gesture.match.length > 0) {
                try {
                    gesture.match = new MatchPatternSet(gesture.match);
                } catch (e) {
                    console.error("failed to convert to MatchPatternSet:", e, gesture.match);
                    gesture.match = lazy.emptyMatchPatternSet;
                }
            }
            else gesture.match = lazy.emptyMatchPatternSet;
        }
        if (!gesture.match?.matches(event.target.ownerGlobal.location.href)) {
            return false;
        }
        return true;
    }
    dragDropSecurityCheck(event, dragSession, url) {
        if (!url) return false;

        // need to do a security check to make
        // sure the source document can load the dropped URI.
        url = url.replace(/^\s*|\s*$/g, '');

        if (url.startsWith('chrome://') || url.startsWith('file://')) return url;

        // urlSecurityCheck
        try {
            Services.scriptSecurityManager.checkLoadURIStrWithPrincipal(event.target.ownerDocument.nodePrincipal, url, Ci.nsIScriptSecurityManager.DISALLOW_INHERIT_PRINCIPAL);
        } catch (e) {
            event.stopPropagation();
            return false;
        }
        return url;
    }
    // リンクや画像のリンクからファイル名にする文字列を得る
    // hrefまたはsrc属性のパスを返す
    // 上記以外のとき, リンクテキストまたはtitle,alt属性の文字列を返す
    candidateFname(target, url) {
        if (!target) {
            let text = url;
            if (text.match(/.*\/(.+)$/)) text = RegExp.$1;
            return text.replace(/[\\\/:\*\?"<>\|]/g, ' ');
        }

        let text = gatherTextUnder(target);
        if (!text || text && !text.match(/\S/)) {
            text = target.getAttribute("title");
            if (!text || !text.match(/\S/)) {
                if (target.hasAttribute("alt")) {
                    text = target.getAttribute("alt");
                    //fx3
                    if (!!text && text.match(/.*\/(.+)$/)) text = RegExp.$1;
                }

                if (!text || !text.match(/\S/)) {
                    if (target.hasAttribute("href")) text = target.href;
                    if (target.hasAttribute("src")) text = target.src;
                    if (text.match(/.*\/(.+)$/)) text = RegExp.$1;
                }
            }
        }
        return text.replace(/[\\\/:\*\?"<>\|]/g, ' ');
    }
    getDroppedURL_Fixup(url) {
        if (!url) return null;
        if (/^h?.?.p(s?):(.+)$/i.test(url)) {
            url = "http" + RegExp.$1 + ':' + RegExp.$2;
            if (!RegExp.$2) return null;
        }
        try {
            url = Services.uriFixup.getFixupURIInfo(url, Services.uriFixup.FIXUP_FLAG_ALLOW_KEYWORD_LOOKUP).preferredURI.spec;
            // valid urls don't contain spaces ' '; if we have a space it
            // isn't a valid url, or if it's a javascript: or data: url,
            // bail out
            if (!url ||
                !url.length ||
                url.indexOf(" ", 0) !== -1 ||
                /^\s*javascript:/.test(url) ||
                /^\s*data:/.test(url) && !/^\s*data:image\//.test(url))
                return null;
            return url;
        } catch (e) {
            return null;
        }
    }
    getDragObject(event, objects, infoCache) {
        const dragService = Cc["@mozilla.org/widget/dragservice;1"].getService(Ci.nsIDragService);
        const dragSession = dragService.getCurrentSession(event.target.ownerGlobal);
        const sourceNode = dragSession.sourceNode;
        const info = new DragObjectInfo();

        for (const obj of objects) {
            //this.debug(obj);
            if (infoCache?.has(obj)) {
                const cachedInfo = infoCache.get(obj);
                if (cachedInfo) {
                    info.urls.push(...cachedInfo.urls);
                    info.texts.push(...cachedInfo.texts);
                    info.nodes.push(...cachedInfo.nodes);
                    info.files.push(...cachedInfo.files);
                    info.fname.push(...cachedInfo.fname);
                }
                continue;
            }
            switch (obj) {
                case 'xpi':
                case 'jar': {
                    const data = sourceNode;
                    if (HTMLAnchorElement.isInstance(data)) {
                        let url = data.href;
                        const baseURI = Services.io.newURI(data.ownerDocument.documentURI, null, null);
                        url = Services.io.newURI(url, null, baseURI).spec;
                        if (!this.xpiLinkRegExp.test(url)) break;
                        if (url && this.dragDropSecurityCheck(event, dragSession, url)) {
                            info.addEntry(
                                url,
                                gatherTextUnder(data),
                                data,
                                null,
                                this.candidateFname(data, url),
                                {name: obj, dict: infoCache}
                            );
                        }
                    }
                    if (info.urls.length > 0) infoCache.set(obj, info);
                    else infoCache.set(obj, null);
                    break;
                }
                case 'file': {
                    const dt = event.dataTransfer;
                    for (let j = 0; j < dt.mozItemCount; j++) {
                        if (dt.types.includes("application/x-moz-file")) {
                            const file = dt.mozGetDataAt("application/x-moz-file", j);
                            if (file instanceof Ci.nsIFile) {
                                const uri = Services.io.newFileURI(file);
                                if (this.dragDropSecurityCheck(event, dragSession, uri.spec)) {
                                    info.addEntry(
                                        uri.spec,
                                        uri.spec,
                                        null,
                                        file,
                                        this.candidateFname(null, uri.spec),
                                        {name: obj, dict: infoCache}
                                    );
                                }
                            }
                        }
                    }
                    if (infoCache && !infoCache.has(obj)) infoCache.set(obj, null);
                    break;
                }
                case 'link': {
                    if (!sourceNode) {
                        const dt = event.dataTransfer;
                        if (dt.types.includes("text/x-moz-url")) {
                            for (let j = 0; j < dt.mozItemCount; j++) {
                                let urlAndDesc = dt.mozGetDataAt("text/x-moz-url", j);
                                if (!urlAndDesc) continue;
                                let url = dt.mozGetDataAt("text/x-moz-url-data", j);
                                let desc = dt.mozGetDataAt("text/x-moz-url-desc", j);
                                if (!url) {
                                    url = urlAndDesc.split('\n')[0];
                                }
                                if (!desc) {
                                    desc = urlAndDesc.split('\n')[1];
                                }
                                if (dt.mozTypesAt(j).contains("application/x-moz-nativeimage")) {
                                    const imageURL = dt.mozGetDataAt("application/x-moz-file-promise-url", j);
                                    if (url === imageURL) continue;
                                }
                                info.addEntry(
                                    url,
                                    desc,
                                    event.target?.ownerDocument?.createElement("a"),
                                    null,
                                    this.candidateFname(null, url),
                                    {name: obj, dict: infoCache}
                                );
                            }
                        }
                    }
                    else if (HTMLAnchorElement.isInstance(sourceNode)) {
                        let url = sourceNode.href;
                        const baseURI = Services.io.newURI(sourceNode.ownerDocument.documentURI, null, null);
                        url = Services.io.newURI(url, null, baseURI).spec;
                        if (url && this.dragDropSecurityCheck(event, dragSession, url)) {
                            info.addEntry(
                                url,
                                gatherTextUnder(sourceNode),
                                sourceNode,
                                null,
                                this.candidateFname(sourceNode, url),
                                {name: obj, dict: infoCache}
                            );
                        } else if (url) {
                            info.addEntry(
                                null,
                                gatherTextUnder(sourceNode),
                                sourceNode,
                                null,
                                null,
                                {name: obj, dict: infoCache}
                            );
                        }
                    }
                    if (infoCache && !infoCache.has(obj)) infoCache.set(obj, null);
                    break;
                }
                case 'image': {
                    // 同一フレームでない場合はdataTransferにapplication/x-moz-nativeimageがセットされ
                    // application/x-moz-file-promise-urlから実体のURLが取得できる
                    if (!sourceNode) {
                        const dt = event.dataTransfer;
                        if (dt.types.includes("application/x-moz-nativeimage")) {
                            for (let j = 0; j < dt.mozItemCount; j++) {
                                const url = dt.mozGetDataAt("application/x-moz-file-promise-url", j);
                                if (!url) continue;
                                info.addEntry(
                                    url,
                                    dt.mozGetDataAt("text/x-moz-url-desc", j),
                                    null,
                                    null,
                                    this.candidateFname(null, url),
                                    {name: obj, dict: infoCache}
                                );
                            }
                        }
                    }
                    else do {
                        const data = this.getElementsByXPath('descendant-or-self::img', sourceNode);
                        if (data.length < 1) break;
                        if (event.dataTransfer.types.includes("text/plain")) {
                            const win = event.target.ownerGlobal;
                            const selection = this.getSelection(win);
                            if (!!selection && event.dataTransfer.getData(["text/plain"]) === selection)
                                break;
                        }
                        const node = data[data.length - 1]; //
                        if (node instanceof Ci.nsIImageLoadingContent || HTMLCanvasElement.isInstance(node)) {
                            let url;
                            if (node instanceof Ci.nsIImageLoadingContent) {
                                url = node.getAttribute('src');
                            } else if (HTMLCanvasElement.isInstance(node)) {
                                url = node.toDataURL();
                            }
                            if (url) {
                                const baseURI = Services.io.newURI(node.ownerDocument.documentURI, null, null);
                                url = Services.io.newURI(url, null, baseURI).spec;
                                if (this.dragDropSecurityCheck(event, dragSession, url)) {
                                    let title = url;
                                    if (node.hasAttribute('title')) {
                                        title = node.getAttribute('title');
                                    } else if (node.hasAttribute('alt')) {
                                        title = node.getAttribute('alt');
                                    }
                                    info.addEntry(
                                        url,
                                        title,
                                        node,
                                        null,
                                        this.candidateFname(node, url),
                                        {name: obj, dict: infoCache}
                                    );
                                }
                            }
                        }
                    } while (0);
                    if (infoCache && !infoCache.has(obj)) infoCache.set(obj, null);
                    break;
                }
                case 'textlink': {
                    do {
                        if (!sourceNode) {
                            const dt = event.dataTransfer;
                            if (dt.types.includes("text/x-moz-url")) break;
                        }
                        const node = sourceNode;
                        if (node && !Text.isInstance(node)) break;
                        if (node && Text.isInstance(node)) {
                            if (node.nodeValue == "") break;
                            const anc = node.ownerDocument.evaluate(
                                'ancestor-or-self::*[local-name()="a"]',
                                node,
                                null,
                                XPathResult.FIRST_ORDERED_NODE_TYPE,
                                null
                            ).singleNodeValue;
                            if (anc) break;
                        }
                        const supportedTypes = ["text/x-moz-url", "text/plain"];
                        for (const type of supportedTypes) {
                            if (event.dataTransfer.types.includes(type)) {
                                const data = event.dataTransfer.getData(type);
                                if (/^file:/.test(data)) break;
                                if (this.linkRegExp.test(data)) {
                                    let url = data.match(this.linkRegExp)[1];
                                    url = this.getDroppedURL_Fixup(url);
                                    if (url.trim() && this.dragDropSecurityCheck(event, dragSession, url)) {
                                        info.addEntry(
                                            url,
                                            url,
                                            node,
                                            null,
                                            this.candidateFname(null, url),
                                            {name: obj, dict: infoCache}
                                        );
                                    }
                                } else if (this.localLinkRegExp.test(data)) {
                                    let url = data.match(this.localLinkRegExp)[0];
                                    url = this.getDroppedURL_Fixup(url);
                                    if (url.trim() && this.dragDropSecurityCheck(event, dragSession, url)) {
                                        info.addEntry(
                                            url,
                                            url,
                                            node,
                                            null,
                                            this.candidateFname(null, url),
                                            {name: obj, dict: infoCache}
                                        );
                                    }
                                }
                                break;
                            }
                        }
                    } while (0);
                    if (infoCache && !infoCache.has(obj)) infoCache.set(obj, null);
                    break;
                }
                case 'text': {
                    if (config.RESTRICT_SELECTED_TEXT && sourceNode) {
                        const data = sourceNode.ownerGlobal.getSelection().toString();
                        if (data.trim()) {
                            info.addEntry(
                                null,
                                data,
                                null,
                                null,
                                this.candidateFname(null, data),
                                {name: obj, dict: infoCache}
                            );
                        } else {
                            const supportedTypes = ["text/plain"];
                            for (const type of supportedTypes) {
                                if (event.dataTransfer.types.includes(type)) {
                                    const data = event.dataTransfer.getData(type);
                                    if (!data.trim()) continue;
                                    info.addEntry(
                                        null,
                                        data,
                                        null,
                                        null,
                                        this.candidateFname(null, data),
                                        {name: obj, dict: infoCache}
                                    );
                                }
                            }
                        }
                    } else if (!config.RESTRICT_SELECTED_TEXT || !sourceNode) {
                        const supportedTypes = ["text/plain"];
                        for (const type of supportedTypes) {
                            if (event.dataTransfer.types.includes(type)) {
                                const data = event.dataTransfer.getData(type);
                                if (!data.trim()) continue;
                                info.addEntry(
                                    null,
                                    data,
                                    null,
                                    null,
                                    this.candidateFname(null, data),
                                    {name: obj, dict: infoCache}
                                );
                            }
                        }
                    }
                    if (infoCache && !infoCache.has(obj)) infoCache.set(obj, null);
                    break;
                }
                default:
                    break;
            }
        }
        if (info.urls.length > 0) {
            return info;
        }
        return null;
    }
    dragstart(event) {
        const context = contexts.get(event.target.ownerGlobal);
        // 座標を初期化
        context.lastX = event.screenX;
        context.lastY = event.screenY;
        // ドラッグ方向を初期化
        context.directionChain = "";
        context.dragInfo = new Map();

        // 転送データをセットする
        if (event.originalTarget instanceof Ci.nsIImageLoadingContent) {
            if (!event.dataTransfer.mozGetDataAt("application/x-moz-node", 0))
                event.dataTransfer.mozSetDataAt("application/x-moz-node", event.originalTarget, 0);
            if (!event.dataTransfer.mozGetDataAt("text/x-moz-url", 0))
                event.dataTransfer.mozSetDataAt("text/x-moz-url", event.originalTarget.src + "\n" + event.originalTarget.src, 0);
            if (!event.dataTransfer.mozGetDataAt("text/uri-list", 0))
                event.dataTransfer.mozSetDataAt("text/uri-list", event.originalTarget.src, 0);
            if (!event.dataTransfer.mozGetDataAt("text/plain", 0))
                event.dataTransfer.mozSetDataAt("text/plain", event.originalTarget.src, 0);
        }
    }
    dragover(event) {
        const dragService = Cc["@mozilla.org/widget/dragservice;1"].getService(Ci.nsIDragService);
        const dragSession = dragService.getCurrentSession(event.target.ownerGlobal);
        const sourceNode = dragSession.sourceNode;
        const target = event.target;
        const actor = target.ownerGlobal.windowGlobalChild.getActor("DragNGoModoki");
        const context = contexts.get(actor.contentWindow);
        const dragInfo = context.dragInfo;

        // D&Dの方向
        const direction = this.getDirection(event, actor);

        // 対象flavourか?
        const flavours = this.getSupportedFlavoursForNode(target);
        const supported = flavours.some(function(type) {
            return event.dataTransfer.types.includes(type);
        });
        if (!supported) {
            return;
        }
        //designModeなら何もしない
        if (HTMLDocument.isInstance(target.ownerDocument) && target.ownerDocument.designMode == 'on') {
            this.setStatusMessage(actor, '', 0, false);
            return;
        }
        // do nothing if event.defaultPrevented (maybe hosted d&d by web page)
        if (event.defaultPrevented)
            return;

        // xxx 2013/01/29
        if (sourceNode) {
            const elm = this.getElementsByXPath('ancestor-or-self::*[@draggable="true"]', sourceNode);
            if (elm.length > 0) return;
        }

        const isSameFrame = (sourceNode && sourceNode.ownerDocument === event.target.ownerDocument);
        const sourceWindowContext = dragSession.sourceWindowContext;
        const isSameBrowser = sourceWindowContext?.topWindowContext.innerWindowId === actor.browsingContext.top.currentWindowContext.innerWindowId;
        if (!isSameFrame && isSameBrowser) { // 同じブラウザの別フレームから
            if ("pending" in context) {
                if (context.pending) return;
            }
            else {
                context.pending = true;
                actor.sendQuery("DNG:GetCurrentDragSessionContext", {
                    starterWindowId: sourceWindowContext.innerWindowId,
                }).then(ret => {
                    if (ret) {
                        context.lastX = ret.lastX;
                        context.lastY = ret.lastY;
                        context.directionChain = ret.directionChain;
                    }
                    context.pending = false;
                    context.dragInfo = new Map();
                });
            }
        }

        dragSession.canDrop = false;
        if (!isSameBrowser) { // 外または別ブラウザから
            if (sourceWindowContext == null) { // 外から
                // fileのドロップは何もしない (Ci.nsIFileにはcontent process側からは触れない)
                if (event.dataTransfer.types.includes("application/x-moz-file")) {
                    return;
                }
            }
            // input/textarea何もしないで置く
            if (this.isParentEditableNode(target)) {
                this.setStatusMessage(actor, '', 0, false);
                return;
            }
            for (const gesture of this.GESTURES) {
                // 方向はないこと
                if (gesture.dir || typeof gesture.cmd !== 'function') {
                    continue;
                }
                // キーチェック
                if (!this.keyCheck(event, gesture)) continue;
                // match チェック
                if (!this.matchCheck(event, gesture)) continue;
                // cmd チェック
                const objs = gesture.obj.replace(/\s+/g, '').toLowerCase().split(',');
                const info = this.getDragObject(event, objs, dragInfo);
                if (info) {
                    if (/drop/.test(event.type)) {
                        gesture.cmd.call(commandHelpers, actor, event, info);
                    } else {
                        this.setStatusMessage(actor, gesture.name, 0, true);
                    }
                    dragSession.canDrop = true;
                    event.preventDefault();
                    break;
                }
            }
            return;
        }

        // 同じブラウザ内から
        // input/textarea何もしないで置く
        if (HTMLInputElement.isInstance(target) ||
            HTMLTextAreaElement.isInstance(target) ||
            this.isParentEditableNode(target)) {
            this.setStatusMessage(actor, '', 0, false);
            return;
        }
        for (const gesture of this.GESTURES) {
            // 方向チェック
            if (!gesture.dir || direction !== gesture.dir || typeof gesture.cmd !== 'function') {
                continue;
            }
            // キーチェック
            if (!this.keyCheck(event, gesture)) continue;
            // match チェック
            if (!this.matchCheck(event, gesture)) continue;
            // cmd チェック
            const objs = gesture.obj.replace(/\s+/g, '').toLowerCase().split(',');
            const info = this.getDragObject(event, objs, dragInfo);
            if (info) {
                if (/drop/.test(event.type)) {
                    gesture.cmd.call(commandHelpers, actor, event, info);
                } else {
                    this.setStatusMessage(actor, gesture.name, 0, false);
                }
                dragSession.canDrop = true;
                event.preventDefault();
                break;
            }
        } // GESTURES
        if (!dragSession.canDrop) {
            this.setStatusMessage(actor, config.MESSAGE_UNKNOWN_GESTURE, 0, false);
        }
    }
    dragend(event) {
        const context = contexts.get(event.target.ownerGlobal);
        context.lastX = null;
        context.lastY = null;
        context.directionChain = "";
        context.dragInfo = null;
        delete context.pending;
    }
    setStatusMessage(actor, message, timeToClear, hideDirectionChain) {
        const context = contexts.get(actor.contentWindow);
        const {directionChain, lastDirectionChain, lastStatusUpdate, lastMessage} = context;
        const now = actor.contentWindow.performance.now();
        if (lastStatusUpdate + 500 > now) {
            if (lastDirectionChain === directionChain && lastMessage === message) return;
        }
        context.lastDirectionChain = directionChain;
        context.lastStatusUpdate = now;
        context.lastMessage = message;
        actor.sendAsyncMessage("DNG:SetStatusMessage", {
            message,
            timeToClear,
            directionChain,
            hideDirectionChain
        });
    }
}();

Object.defineProperties(DragNGo.constructor.prototype, {
    dataRegExp: {value: /^\s*(.*)\s*$/m},
    mdataRegExp: {value: /(^\s*(.*)\s*\n?)*$/m},
    linkRegExp: {value: /(((h?t)?tps?|h..ps?|ftp|((\uff48)?\uff54)?\uff54\uff50(\uff53)?|\uff48..\uff50(\uff53)?|\uff46\uff54\uff50)(:\/\/|\uff1a\/\/|:\uff0f\uff0f|\uff1a\uff0f\uff0f)[-_.!~*\u0027()|a-zA-Z0-9;:\/?,@&=+$%#\[\]\uff0d\uff3f\u301c\uffe3\uff0e\uff01\uff5e\uff0a\u2019\uff08\uff09\uff5c\uff41-\uff5a\uff21-\uff3a\uff10-\uff19\uff1b\uff1a\uff0f\uff1f\uff1a\uff20\uff06\uff1d\uff0b\uff04\uff0c\uff05\uff03\uff5c\uff3b\uff3d]*[-_.!~*)|a-zA-Z0-9;:\/?@&=+$%#\[\]\uff0d\uff3f\u301c\uffe3\uff0e\uff01\uff5e\uff0a\u2019\uff5c\uff41-\uff5a\uff21-\uff3a\uff10-\uff19\uff1b\uff1a\uff0f\uff1f\uff20\uff06\uff1d\uff0b\uff04\uff0c\uff05\uff03\uff5c\uff3b\uff3d]+)/i},
    localLinkRegExp: {value: /(file|localhost):\/\/.+/i},
    xpiLinkRegExp: {value: /(.+)\.(xpi|jar)$/i},
    imageLinkRegExp: {value: /(.+)\.(png|jpg|jpeg|gif|bmp)$/i},
});

export class DragNGoModokiChild extends JSWindowActorChild {
    actorCreated() {
        contexts.set(this.contentWindow, {
            directionChain: "",
            lastX: null,
            lastY: null,
            lastStatusUpdate: 0,
        });
    }
    handleEvent(aEvent) {
        switch (aEvent.type) {
            case "dragenter":
            case "dragover":
            case "drop":
                DragNGo.dragover(aEvent);
                if (aEvent.type === 'drop' && !aEvent.defaultPrevented) {
                    DragNGo.dragend(aEvent);
                }
                break;
            case "dragend": {
                const context = contexts.get(this.contentWindow);
                if (context.remoteWindowIds) {
                    this.sendAsyncMessage("DNG:ResetDragSessionContext", {
                        remoteWindowIds: Array.from(context.remoteWindowIds)
                    });
                    delete context.remoteWindowIds;
                }
                DragNGo.dragend(aEvent);
                break;
            }
            case "dragstart":
                DragNGo.dragstart(aEvent);
                break;
        }
    }
    receiveMessage(message) {
        switch (message.name) {
            case "DNG:GetCurrentDragSessionContext": {
                const context = contexts.get(this.contentWindow);
                let {remoteWindowIds} = context;
                if (!remoteWindowIds) {
                    remoteWindowIds = new Set();
                    context.remoteWindowIds = remoteWindowIds;
                }
                remoteWindowIds.add(message.data.remoteWindowId);
                return Promise.resolve({
                    directionChain: context.directionChain,
                    lastX: context.lastX,
                    lastY: context.lastY
                });
            }
            case "DNG:ResetDragSessionContext": {
                const context = contexts.get(this.contentWindow);
                context.lastX = null;
                context.lastY = null;
                context.directionChain = "";
                context.dragInfo = null;
                delete context.pending;
                break;
            }
            case "DNG:ReloadGestureDefinitions": {
                DragNGo.initGestures();
                break;
            }
        }
    }
}