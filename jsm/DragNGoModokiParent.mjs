const lazy = {};
ChromeUtils.defineESModuleGetters(lazy, {
    FileUtils: "resource://gre/modules/FileUtils.sys.mjs",
    Downloads: "resource://gre/modules/Downloads.sys.mjs",
    PrivateBrowsingUtils: "resource://gre/modules/PrivateBrowsingUtils.sys.mjs",
    FormHistory: "resource://gre/modules/FormHistory.sys.mjs"
});

export const DragNGo = new class {
    constructor() {
        this.currentRegExp = /^\s*(data:|\(?javascript:)/i;
        this.statusBarTimers = new WeakMap();
    }
    attachToWindow(win) {
        win.addEventListener("unload", event => {
            if (Services.wm.getEnumerator("navigator:browser").hasMoreElements()) return;
            const entries = Services.dirsvc.get("TmpD", Ci.nsIFile).directoryEntries;
            while (entries.hasMoreElements()) {
                const entry = entries.getNext().QueryInterface(Ci.nsIFile);
                if (/DnD(-\d+)?\.tmp$/.test(entry.leafName)) {
                    try {
                        entry.remove(false);
                    } catch (e) {}
                }
            }
        }, {once: true});
    }
    //検索バーを得る
    getSearchBar(win) {
        return win.document.getElementById("searchbar");
    }
    //検索エンジン名から検索エンジンを得る
    getEngineByName(win, aEngineName) {
        if (aEngineName.toUpperCase() === "CURRENT") {
            const searchbar = this.getSearchBar(win);
            if (searchbar) return searchbar.currentEngine;
        } else {
            const engine = Services.search.getEngineByName(aEngineName);
            if (engine) return engine;
        }
        //Default
        return Services.search.defaultEngine;
    }
    searchWithEngine(win, texts, engines, _where, addHistoryEntry) {
        const text = texts[0];
        let where = _where;
        for (const engineName of engines) {
            const engine = this.getEngineByName(win, engineName);
            const submission = engine.getSubmission(text, null);
            if (!submission)
                return false;
            const url = submission.uri.spec;
            if (/tab|window/.test(where) && (win.gBrowser.selectedTab.isEmpty || this.currentRegExp.test(url)))
                where = 'current';
            switch (where) {
                case 'tab':
                case 'tabshifted':
                    const loadInBackground = Services.prefs.getBoolPref("browser.tabs.loadInBackground");
                    if (loadInBackground) {
                        if (where === 'tabshifted')
                            where = 'tab';
                        else if (where === 'tab')
                            where = 'tabshifted'
                    }
                case 'current':
                case 'window':
                    win.openLinkIn(submission.uri.spec,
                        where, {
                            fromChrome: false,
                            allowThirdPartyFixup: false,
                            postData: submission.postData,
                            charset: null,
                            referrerURI: null,
                            relatedToCurrent: true,
                            triggeringPrincipal: Services.scriptSecurityManager.createContentPrincipal(Services.io.newURI(submission.uri.spec), {})
                        }
                    );
                    break;
            }
            where = 'tabshifted';
        }
        // 検索履歴に残す
        if (addHistoryEntry)
            this.updateSearchbarHistory(win, text);
        return true;
    }
    //検索バーにテキストをコピー
    copyToSearchBar(win, searchText) {
        const searchbar = this.getSearchBar(win);
        if (!searchbar) return;
        searchbar.value = searchText;
    }
    //検索バーにテキストを追加コピー
    appendToSearchBar(win, searchText) {
        const searchbar = this.getSearchBar(win);
        if (!searchbar) return;
        searchbar.value += (searchbar.value ? " " : "") + searchText;
    }
    //検索バーにテキストをコピー, 履歴を更新
    updateSearchbarHistory(win, searchText) {
        this.copyToSearchBar(win, searchText);
        const searchbar = this.getSearchBar(win);
        if (searchText && !lazy.PrivateBrowsingUtils.isWindowPrivate(win)) {
            lazy.FormHistory.update({
                op: "bump",
                fieldname: searchbar._textbox.getAttribute("autocompletesearchparam"),
                value: searchText
            }).catch (err => {
                console.error("Saving search to form history failed: " + err.message);
            });
        }
    }
    openUrls(win, urls, _where, referrer) {
        for (const url of urls) {
            let where = _where;
            if (/tab|window/.test(where) && (win.gBrowser.selectedTab.isEmpty || this.currentRegExp.test(url)))
                where = 'current';
            switch (where) {
                case 'tab':
                case 'tabshifted':
                    const loadInBackground = Services.prefs.getBoolPref("browser.tabs.loadInBackground");
                    if (loadInBackground) {
                        if (where === 'tabshifted')
                            where = 'tab';
                        else if (where == 'tab')
                            where = 'tabshifted'
                    }
                case 'current':
                case 'window':
                    win.openLinkIn(url,
                        where, {
                            fromChrome: false,
                            allowThirdPartyFixup: false,
                            postData: null,
                            charset: null,
                            referrerURI: referrer,
                            relatedToCurrent: true,
                            triggeringPrincipal: Services.scriptSecurityManager.createContentPrincipal(Services.io.newURI(url), {})
                        }
                    );
                    break;
            }
        }
    }
    openSaveFileModokiPopup(win, event) {
        if (typeof win.saveFolderModoki !== 'undefined') {
            event.documentURIObject = Services.io.newURI(event.docLocation, event.charSet);
            win.saveFolderModoki.showHotMenu(event.screenX, event.screenY, event);
        }
    }
    saveWithSaveFolderModoki(win, url, text, folder) {
        if (typeof win.saveFolderModoki !== 'undefined') {
            win.saveFolderModoki.saveLink(url, text, folder);
        }
    }
    openConQueryPopup(win, event) {
        if (typeof win.cqrShowHotmenu !== 'undefined')
            win.cqrShowHotmenu(null, event.screenX, event.screenY);
    }
    //ページ内検索
    findWord(win, word) {
        win.gBrowser.getFindBar().then(findbar => {
            if ('onFindAgainCommand' in findbar) { //fx3
                if (findbar.hidden)
                    findbar.onFindCommand();
                findbar._findField.value = word;
                const event = win.document.createEvent("UIEvents");
                event.initUIEvent("input", true, false, win, 0);
                findbar._findField.dispatchEvent(event);
            }
        });
    }
    async saveTextToLocal(win, text, _fpath, skipPrompt) {
        let fpath = _fpath;
        let dir = null;
        if (!fpath || !PathUtils.isAbsolute(fpath)) {
            dir = new lazy.FileUtils.File(await lazy.Downloads.getPreferredDownloadsDirectory());
            if (!fpath && skipPrompt) skipPrompt = false;
        } else {
            dir = PathUtils.parent(fpath);
            await IOUtils.makeDirectory(dir, {createAncestors: true});
            dir = new lazy.FileUtils.File(dir);
            fpath = PathUtils.filename(fpath);
        }

        if (!skipPrompt) {
            const nsIFilePicker = Ci.nsIFilePicker;
            const fp = Cc["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
            if (parseInt(Services.appinfo.version) >= 125) fp.init(win.browsingContext, "Select a File", nsIFilePicker.modeSave);
            else fp.init(win, "Select a File", nsIFilePicker.modeSave);
            fp.appendFilters(nsIFilePicker.filterText | nsIFilePicker.filterImages);
            fp.appendFilters(nsIFilePicker.filterHTML | nsIFilePicker.filterAll);
            if (dir)
                fp.displayDirectory = dir;
            fp.defaultString = fpath;
            switch (await new Promise(resolve => {fp.open(rv => {resolve(rv);});})) {
                case (nsIFilePicker.returnOK):
                case (nsIFilePicker.returnReplace):
                    fpath = fp.file.path;
                    break;
                case (nsIFilePicker.returnCancel):
                default:
                    return;
            }
        } else {
            fpath = PathUtils.join(dir.path, fpath);
        }
        IOUtils.writeUTF8(fpath, text).catch(ex => {
            win.alert('failed:\n' + ex);
        });
    }
    saveAs(win, aURL, aFileName, _aReferrer, aSourceDocument, aContentType, aContentDisposition) {
        const aPrincipal = Services.scriptSecurityManager.createContentPrincipal(Services.io.newURI(aURL), {});
        const isPrivate = lazy.PrivateBrowsingUtils.isWindowPrivate(win);
        const [firefoxVer, firefoxMinorVer] = Services.appinfo.version.split(".");
        const imageLinkRegExp = /(.+)\.(png|jpg|jpeg|gif|bmp|webp|avif)$/i;
        let aReferrer = Services.io.newURI(_aReferrer);
        let referrerInfo = Cc["@mozilla.org/referrer-info;1"].createInstance(Ci.nsIReferrerInfo);
        referrerInfo.init(
            Ci.nsIHttpChannel.REFERRER_POLICY_NO_REFERRER_WHEN_DOWNGRADE,
            true,
            aReferrer
        );
        aReferrer = referrerInfo;
        if (imageLinkRegExp.test(aURL) || /^image\//i.test(aContentType)) {
            let cookieJarSettings = win.gBrowser.selectedBrowser.cookieJarSettings;
            if (/^data:/.test(aURL)) {
                win.internalSave(aURL, null, null, "index.png", aContentDisposition, aContentType, true, null, null, aReferrer, cookieJarSettings, aSourceDocument, false, null, isPrivate, aPrincipal);
            } else {
                win.internalSave(aURL, null, null, null, aContentDisposition, aContentType, false, null, null, aReferrer, cookieJarSettings, aSourceDocument, false, null, isPrivate, aPrincipal);
            }
        } else {
            let cookieJarSettings = win.gBrowser.selectedBrowser.cookieJarSettings;
                win.saveURL(aURL, null, aFileName, null, true, false, aReferrer, cookieJarSettings, aSourceDocument, isPrivate, aPrincipal);
        }
    }
    async editText(win, editor, _text) {
        let text = _text
        // Get filename.
        const path = await IOUtils.createUniqueFile(PathUtils.tempDir, "DnD.tmp");

        // Write the data to the file.
        if (Services.appinfo.OS === "WINNT") {
            // Convert Unix newlines to standard network newlines.
            text = text.replace(/\n/g, "\r\n");
        }

        IOUtils.writeUTF8(path, text).then(() => {
            // Edit the file.
            editfile(editor, path);
        }).catch(e => {
            console.error(e);
        });

        // the external view source editor or null
        function getExternalViewSourceEditorPath() {
            try {
                return Services.prefs.getStringPref("view_source.editor.path");
            } catch (ex) {
                console.error(ex);
            }
            return null;
        }

        function editfile(_editor, filename) {
            // Figure out what editor to use.
            const editor = _editor || getExternalViewSourceEditorPath();
            if (!editor) {
                win.alert("Error_No_Editor");
                return false;
            }

            const file = new lazy.FileUtils.File(editor);
            if (!file.exists()) {
                win.alert("Error_invalid_Editor_file");
                return false;
            }
            if (!file.isExecutable()) {
                win.alert("Error_Editor_not_executable");
                return false;
            }

            // Run the editor.
            const process = Cc["@mozilla.org/process/util;1"]
                .createInstance(Ci.nsIProcess);
            process.init(file);
            const args = [filename];
            process.runw(false, args, args.length); // don't block
            return true;
        }
    }
    //appPathをparamsで開く, paramsはtxtで置き換え, charsetはnsIProcess.runwがunicode対応のため無視
    launch(win, txt, appPath, params, charset) {
        const appfile = new lazy.FileUtils.File(appPath);
        if (!appfile.exists()) {
            win.alert("Executable does not exist.");
            return;
        }
        const process = Cc['@mozilla.org/process/util;1']
            .createInstance(Ci.nsIProcess);
        const args = [];
        for (const param of params) {
            if (param) {
                const converted = param.replace(/%%URL%%/i, txt).replace(/%%SEL%%/i, txt);
                args.push(converted);
            }
        }
        process.init(appfile);
        process.runw(false, args, args.length, {});
    }
    //ステータスバーに文字列を表示, timeToClearミリ秒後自動クリア
    setStatusMessage(win, msg, timeToClear, directionChain, hideDirectionChain) {
        function setStatusMessage(value) {
            const status4ever = win.document.getElementById("status4evar-status-text");
            if (status4ever) {
                status4ever.value = value;
            } else {
                win.StatusPanel._label = value;
            }
        }
        try {
            if (msg != '') {
                setStatusMessage(((!hideDirectionChain) ? (directionChain + ' : ') : '') + msg);
            } else {
                setStatusMessage('');
            }
        } catch (e) {
            setStatusMessage('');
        }
        let timer = this.statusBarTimers.get(win);
        if (timer) {
            win.clearTimeout(timer);
            this.statusBarTimers.delete(win);
        }
        timer = win.setTimeout(() => {
            setStatusMessage('');
            this.statusBarTimers.delete(win);
        }, !timeToClear ? 1500 : timeToClear);
        this.statusBarTimers.set(win, timer);
    }
}();

export class DragNGoModokiParent extends JSWindowActorParent {
    receiveMessage(message) {
        let browser = this.browsingContext.top.embedderElement;
        let win = browser.ownerGlobal;
        switch (message.name) {
            case "DNG:SetStatusMessage": {
                DragNGo.setStatusMessage(win, message.data.message, message.data.timeToClear, message.data.directionChain, message.data.hideDirectionChain);
                break;
            }
            case "DNG:ExecuteCommand": {
                DragNGo[message.data.command]?.call(DragNGo, win, ...message.data.args);
                break;
            }
            case "DNG:GetCurrentDragSessionContext": {
                let contextsToVisit = [browser.browsingContext];
                let targetWindowGlobal;
                while (contextsToVisit.length) {
                    try {
                        let currentContext = contextsToVisit.pop();
                        let global = currentContext.currentWindowGlobal;
                        if (!global) continue;
                        contextsToVisit.push(...currentContext.children);
                        if (global.innerWindowId !== message.data.starterWindowId) continue;
                        targetWindowGlobal = global;
                        break;
                    } catch (e) {}
                }
                if (targetWindowGlobal) {
                    const actor = targetWindowGlobal.getActor("DragNGoModoki");
                    return actor.sendQuery("DNG:GetCurrentDragSessionContext", {
                        remoteWindowId: message.target.manager.innerWindowId
                    });
                }
                return Promise.resolve();
            }
            case "DNG:ResetDragSessionContext": {
                let contextsToVisit = [browser.browsingContext];
                while (contextsToVisit.length) {
                    try {
                        let currentContext = contextsToVisit.pop();
                        let global = currentContext.currentWindowGlobal;
                        if (!global) continue;
                        contextsToVisit.push(...currentContext.children);
                        if (!message.data.remoteWindowIds.includes(global.innerWindowId)) continue;
                        global.getActor("DragNGoModoki").sendAsyncMessage("DNG:ResetDragSessionContext");
                    } catch (e) {}
                }
                break;
            }
            case "DNG:ReloadGestureDefinitions": {
                /* getExistingActor is available on Firefox >= 90 but buggy on < 94 / ESR91.3 (bug 1736545) */
                const version = parseFloat(Services.appinfo.version);
                for (const win of Services.wm.getEnumerator("navigator:browser")) {
                    for (const browser of win.gBrowser.browsers) {
                        if (!browser.browsingContext) continue;
                        let contextsToVisit = [browser.browsingContext];
                        while (contextsToVisit.length) {
                            try {
                                let currentContext = contextsToVisit.pop();
                                let global = currentContext.currentWindowGlobal;
                                if (!global) continue;
                                contextsToVisit.push(...currentContext.children);
                                global.getExistingActor("DragNGoModoki")?.sendAsyncMessage("DNG:ReloadGestureDefinitions");
                            } catch (e) {}
                        }
                    }
                }
                break;
            }
        }
    }
}