// ==UserScript==
// @name           saveFolderModoki40.uc.js
// @namespace      http://space.geocities.yahoo.co.jp/gl/alice0775
// @description    saveFolderModoki
// @include        main
// @compatibility  Firefox 115+ (compatible with Fission)
// @author         Alice0775
// @version        2019/05/xx See https://bitbucket.org/BSTweaker/userchromejs/history-node/master/saveFolderModoki.uc.js for further changes
// @version        2017/11/18 Bug 1334975 removed  nsIFilePicker.show(), nsILocalFile. use  nsIFilePicker.open(), nsIFile
// @version        2017/11/18 nsIPrefBranch to nsIPrefBranch
// @version        2015/08/12 18:00 Fixed due to Bug 1134769
// @version        2015/04/08 12:30 focusedWindow
// @version        2014/11/26 21:00 Bug 1103280, Bug 704320
// @version        2013/05/02 01:00 Bug 789546
// @version        2013/04/14 21:00 checking element using Ci.nsIImageLoadingContent instead of HTMLImageElement
// @version        2013/02/09 23:00 Bug 622423
// @version        2011/10/06 23:00 Bug 795065 Add privacy status to nsDownload
// ==/UserScript==
// @version        2010/04/21 01:00 Firefox3.7a5pre
// @version        2009/04/10 00:00 Minefield3.6a1pre での動作改善 (メニューのidが"SaveFolder"だとダメ なぜ?)
// @version        2008/10/17 12:00 コード整理 (DragNgoModoki5.uc.jsも修正)
// Original function of directSaveLink and setLeafName are from Drag de Go.xpi,
// and author is Yukichi http://bushwhacker.up.seesaa.net/ .
//
// ツールメニューで保存するホルダを指定する
// 指定されたホルダは about:config  userChrome.save.folders に 「,」区切りで保存
//
// 画像をダブルクリックしたときの保存フォルダをIMAGE_DBLCLICKに指定
// これはabout:config  userChrome.save.folderOnImageDblclickにて変更可能
"use strict";

(function() {
    const options = {
        IMAGE_DBLCLICK: "D:\\ほげほげ", // 画像を右ダブルクリックしたときの保存フォルダ, nullならファイルピッカ起動
        CONTENTDISPOSITION: true, // content-disposition 使う[true], 使わないfalse
    };

    const chromeDir = PathUtils.join(PathUtils.profileDir, "chrome");
    const parent = PathUtils.join(
        chromeDir,
        "jsm",
        "SaveFolderModokiParent.mjs"
    );
    const child = PathUtils.join(
        chromeDir,
        "jsm",
        "SaveFolderModokiChild.mjs"
    );

    Promise.all([IOUtils.stat(parent), IOUtils.stat(child)]).then(([parentInfo, childInfo]) => {
        const resourceHandler = Services.io.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
        if (!resourceHandler.hasSubstitution("sfm-ucjs")) {
            resourceHandler.setSubstitution("sfm-ucjs", Services.io.newURI(PathUtils.toFileURI(PathUtils.parent(parent))));
        }
        const parentURL = `resource://sfm-ucjs/SaveFolderModokiParent.mjs?${parentInfo.lastModified}`;
        const childURL = `resource://sfm-ucjs/SaveFolderModokiChild.mjs?${childInfo.lastModified}`;
        const {SaveFolderModoki} = ChromeUtils.importESModule(parentURL);
        window.saveFolderModoki = new SaveFolderModoki(window, options);
        try {
            ChromeUtils.registerWindowActor("SaveFolderModoki", {
                parent: {
                    esModuleURI: parentURL,
                },
                child: {
                    esModuleURI: childURL,
                    events: {
                        click: {},
                    },
                },
                allFrames: true,
                messageManagerGroups: ["browsers"],
                matches: ["*://*/*"],
            });
        } catch (e) {}
    }).catch(e => {
        console.error("SaveFolderModoki: required .mjs files are not placed correctly!");
    });
}());
