// ==UserScript==
// @name           DragNgoModoki_Fx40.uc.js
// @namespace      http://space.geocities.yahoo.co.jp/gl/alice0775
// @description    ファイル名をD&D
// @include        main
// @compatibility  Firefox 115+ (compatible with Fission)
// @author         Alice0775
// @version        2019/05/xx See https://bitbucket.org/BSTweaker/userchromejs/history-node/master/DragNgoModoki.uc.js for further changes
// @version        2017/11/18 Bug 1334975 removed  nsIFilePicker.show(), nsILocalFile. use  nsIFilePicker.open(), nsIFile
// @version        2017/11/18 nsIPrefBranch2 to nsIPrefBranch
// @version        2016/06/12 22:00 Fix regression from Update form history
// @version        2016/04/21 22:00 Update form history
// @version        2015/08/18 00:50 Fixed 受
// @version        2015/08/12 18:00 Fixed due to Bug 1134769
// @version        2014/11/26 21:00 Bug 1103280, Bug 704320
// @version        2014/11/10 10:00 get rid document.commandDispatcher
// @version        2014/10/30 10:00 working with addHistoryFindbarFx3.0.uc.js
// @version        2014/10/07 20:00 adjusts tolerance due to backed out Bug 378775
// @version        2014/10/07 19:00 Modified to use capturing phase for drop and event.defaultprevent
// ==/UserScript==
// @version        2014/07/05 12:00 adjusts tolerance due to Bug 378775
// @version        2014/05/01 12:00 Fix unnecessary toolbaritem creation
// @version        2013/10/31 00:00 Bug 821687  Status panel should be attached to the content area
// @version        2013/09/13 00:00 Bug 856437 Remove Components.lookupMethod
// @version        2013/08/26 14:00 use FormHistory.update and fixed typo
// @version        2013/05/30 01:00 text drag fails on http://blog.livedoor.jp/doku1108/archives/52130085.html
// @version        2013/05/02 01:00 Bug 789546
// @version        2013/04/22 14:00 typo, "use strict" mode
// @version        2013/04/19 20:00 treat HTMLCanvasElement as image
// @version        2013/04/14 23:40 remove all temp file on exit browser
// @version        2013/04/14 23:00 text open with externalEditor, char code
// @version        2013/04/14 22:00 text open with externalEditor (sloppy)
// @version        2013/04/14 21:00 checking element using Ci.nsIImageLoadingContent instead of HTMLImageElement
// @version        2013/03/05 00:00 input type=file change event が発火しないのを修正 Fx7+
// @version        2013/01/29 00:00 draggable="true"もう一度有効
// @version        2013/01/08 02:00 Bug 827546
// @version        2013/01/01 15:00 Avoid to overwrite data on dragstart. And Bug 789546
// @version        2012/10/24 23:00 href=javascript://のリンクテキストの処理変更
// @version        2012/10/06 23:00 Bug 795065 Add privacy status to nsDownload
// @version        2012/10/06 07:00 Bug 722872  call .init(null) for nsITransferable instance
// @version        2012/07/10 12:00 'テキストをConQueryで検索'にlink追加
// @version        2012/06/01 23:00 regression 04/19
// @version        2012/05/04 13:00 Bug 741216 対策
// @version        2012/04/19 00:05 debugなし
// @version        2012/04/19 00:00 designModeはなにもしないようにした
// @version        2012/03/01 12:00 isTabEmpty使うように
// @version        2012/02/12 16:00 fixed Bug 703514
// @version        2012/01/31 11:00 by Alice0775  12.0a1 about:newtab
// @version        2012/01/30 01:00 tavClose, this.sourcenode = null;
// @version        2011/07/22 21:00 Bug 50660 [FILE]Drag and drop for file upload form control (Fx7 and later)
// @version        2011/06/23 16:00 browser.tabs.loadInBackgroundに関わらずtabおよびtabshiftedはそれぞれ強制的に前面および背面に開く
// @version        2011/06/23 16:00 openLinkInにした
// @version        2011/06/22 00:00 getElementsByXPath 配列で返すのを忘れていた
// @version        2011/06/19 21:00 Google modified getElementsByXPath
// @version        2011/04/14 21:00 Google doc などでdrag drop uploadができないので外部ファイルのドロップは止め
// @version        2011/03/30 10:20 プロンプト
// @version        2011/03/29 14:20 copyToSearchBar, appendToSearchBar, searchWithEngine 追加変更
// @version        2011/03/11 10:30 Bug641090
// @version        2010/12/10 08:30 close button非表示 Bug 616014 - Add close button to the add-on bar
// @version        2010/11/13 20:30 status 4-evar
// @version        2010/09/24 20:30 Bug 574688 adon bar
// @version        2010/09/14 19:30 textのドラッグの判定時, テキストノードの制限を外してみた
// @version        2010/08/30 17:30 no more available InstallTrigger method in window since Firefox4.0b5pre
// @version        2010/08/15 17:00 パスの記入ができなくなっていた。regression from 07/15
// @version        2010/07/22 07:00 xxx Bug 580710 - Drag&Drop onto sidebar loads page into sidebar
// @version        2010/07/21 16:00 text
// @version        2010/07/15 16:00 window.getSelection()のままとした
// @version        2010/07/15 15:00 editable要素ではなにもしないようにした
// @version        2010/07/07 07:00 アドオンタブではなにもしないようにした
// @version        2010/07/06 01:05 外部テキストのドロップバグ
// @version        2010/07/06 01:00 外部テキストのドロップバグ
// @version        2010/07/06 00:55 frameへのドロップバグ, textはRESTRICT_SELECTED_TEXTにした
// @version        2010/07/05 20:55 rgression 2010/07/05 19:00 textlink
// @version        2010/07/05 20:30 検索エンジン
// @version        2010/07/05 19:00 textlink, modifier
// @version        2010/07/03 00:00 saveAs
// @version        2010/05/06 00:00 Bug 545119  - Remove browser dependency on nsDragAndDrop.js
// @version        2010/05/05 00:00 Bug 545119  - Remove browser dependency on nsDragAndDrop.js
// @version        2010/04/24 20:00 urlのjavascriptとdataは無条件にカレントタブに開くように
// @version        2010/04/22 23:00 urlの空白は削除しておく
// @version        2010/04/22 16:00 画像のドロップではリンクされている場合リンク先の画像, 保存pathのパス区切り
// @version        2010/04/21 21:35 infoない???
// @version        2010/04/21 17:50 xulエレメントは何もしないように
// @version        2010/04/21 17:50 インプットテキストエリアへのドロップができなくなっていた
// @version        2010/04/21 12:50 unload処理
// @version        2010/04/21 01:04 テキスト...が壊れていた
// @version        2010/04/21 01:03 複数の外部ファイルのtype=file へのドロップ動供くように
// @version        2010/04/21 01:02 複数の外部ファイルのドロップ動供くように
// @version        2010/04/21 01:00  saveFolderModoki.uc.xul連携
// @version        2010/04/21 01:00  Firefox3.7a5pre
// @version        2009/12/15 17:00 Fx3.6 and more
// @version        2007/08/04 20:00
// @LICENSE        MPL 1.1/GPL 2.0/LGPL 2.1
"use strict";

(function() {
    const chromeDir = PathUtils.join(PathUtils.profileDir, "chrome");
    const parent = PathUtils.join(
        chromeDir,
        "jsm",
        "DragNGoModokiParent.mjs"
    );
    const child = PathUtils.join(
        chromeDir,
        "jsm",
        "DragNGoModokiChild.mjs"
    );

    Promise.all([IOUtils.stat(parent), IOUtils.stat(child)]).then(([parentInfo, childInfo]) => {
        const resourceHandler = Services.io.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
        if (!resourceHandler.hasSubstitution("dng-ucjs")) {
            resourceHandler.setSubstitution("dng-ucjs", Services.io.newURI(PathUtils.toFileURI(PathUtils.parent(parent))));
        }
        const parentURL = `resource://dng-ucjs/DragNGoModokiParent.mjs?${parentInfo.lastModified}`;
        const childURL = `resource://dng-ucjs/DragNGoModokiChild.mjs?${childInfo.lastModified}`;
        ChromeUtils.importESModule(parentURL).DragNGo.attachToWindow(window);
        try {
            ChromeUtils.registerWindowActor("DragNGoModoki", {
                parent: {
                    esModuleURI: parentURL,
                },
                child: {
                    esModuleURI: childURL,
                    events: {
                        dragstart: {},
                        dragend: {},
                        dragenter: {},
                        dragover: {},
                        drop: {capture: true},
                    },
                },
                allFrames: true,
                messageManagerGroups: ["browsers"],
                matches: ["*://*/*", "file:///*", "about:*", "view-source:*"],
            });
        } catch (e) {}
    }).catch(e => {
        console.error("DragNGoModoki: required .mjs files are not placed correctly!");
    });
}());
