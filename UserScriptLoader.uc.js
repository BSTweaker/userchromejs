// ==UserScript==
// @name           UserScriptLoader.uc.js
// @description    Greasemonkey っぽいもの
// @namespace      http://d.hatena.ne.jp/Griever/
// @include        main
// @compatibility  Firefox 115+ (compatible with Fission)
// @license        MIT License
// @version        0.1.8.4
// @note           See https://bitbucket.org/BSTweaker/userchromejs/history-node/master/UserScriptLoader.uc.js for further changes
// @note           0.1.8.4 add persistFlags for PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION to fix @require save data
// @note           0.1.8.4 Firefox 35 用の修正
// @note           0.1.8.4 エディタで Scratchpad を使えるようにした
// @note           0.1.8.4 GM_notification を独自実装
// @note           0.1.8.3 Firefox 32 で GM_xmlhttpRequest が動かないのを修正
// @note           0.1.8.3 内臓の console を利用するようにした
// @note           0.1.8.3 obsever を使わないようにした
// @note           0.1.8.2 Firefox 22 用の修正
// @note           0.1.8.2 require が機能していないのを修正
// @note           0.1.8.1 Save Script が機能していないのを修正
// @note           0.1.8.0 Remove E4X
// @note           0.1.8.0 @match, @unmatch に超テキトーに対応
// @note           0.1.8.0 .tld を Scriptish を参考にテキトーに改善
// @note           0.1.7.9 __exposedProps__ を付けた
// @note           0.1.7.9 uAutoPagerize との連携をやめた
// @note           0.1.7.8 window.open や target="_blank" で実行されないのを修正
// @note           0.1.7.7 @delay 周りのバグを修正
// @note           0.1.7.6 require で外部ファイルの取得がうまくいかない場合があるのを修正
// @note           0.1.7.5 0.1.7.4 にミスがあったので修正
// @note           0.1.7.4 GM_xmlhttpRequest の url が相対パスが使えなかったのを修正
// @note           0.1.7.3 Google Reader NG Filterがとりあえず動くように修正
// @note           0.1.7.2 document-startが機能していなかったのを修正
// @note           0.1.7.1 .tld がうまく動作していなかったのを修正
// @note           書きなおした
// @note           スクリプトを編集時に日本語のファイル名のファイルを開けなかったのを修正
// @note           複数のウインドウを開くとバグることがあったのを修正
// @note           .user.js 間で window を共有できるように修正
// @note           .tld を簡略化した
// @note           スクリプトをキャッシュしないオプションを追加
// @note           GM_safeHTMLParser, GM_generateUUID に対応
// @note           GM_unregisterMenuCommand, GM_enableMenuCommand, GM_disableMenuCommand に対応
// @note           GM_getMetadata に対応(返り値は Array or undefined)
// @note           GM_openInTab に第２引数を追加
// @note           @require, @resource のファイルをフォルダに保存するようにした
// @note           @delay に対応
// @note           @bookmarklet に対応（from NinjaKit）
// @note           GLOBAL_EXCLUDES を用意した
// @note           セキュリティを軽視してみた
// ==/UserScript==

(function () {
	const chromeDir = PathUtils.join(PathUtils.profileDir, "chrome");
	const parent = PathUtils.join(
		chromeDir,
		"jsm",
		"UserScriptLoaderParent.mjs"
	);
	const child = PathUtils.join(
		chromeDir,
		"jsm",
		"UserScriptLoaderChild.mjs"
	);

	Promise.all([IOUtils.stat(parent), IOUtils.stat(child)]).then(([parentInfo, childInfo]) => {
		const resourceHandler = Services.io.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
		if (!resourceHandler.hasSubstitution("usl-ucjs")) {
			resourceHandler.setSubstitution("usl-ucjs", Services.io.newURI(PathUtils.toFileURI(PathUtils.parent(parent))));
		}
		const parentURL = `resource://usl-ucjs/UserScriptLoaderParent.mjs?${parentInfo.lastModified}`;
		const childURL = `resource://usl-ucjs/UserScriptLoaderChild.mjs?${childInfo.lastModified}`;
		ChromeUtils.defineESModuleGetters(window, {USL: parentURL});
		window.USL.init(window);
		try {
			ChromeUtils.registerWindowActor("UserScriptLoader", {
				parent: {
					esModuleURI: parentURL,
				},
				child: {
					esModuleURI: childURL,
					events: {
						DOMWindowCreated: {},
						DOMDocElementInserted: {capture:true},
					},
				},
				allFrames: true,
				messageManagerGroups: ["browsers"],
				matches: ["*://*/*", "about:blank"],
			});
		} catch (e) {}
	}).catch(e => {
		console.error("UserScriptLoader: required .mjs files are not placed correctly!");
	});
})();
