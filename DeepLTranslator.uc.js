// ==UserScript==
// @name           DeepL Translator
// @include        main
// @compatibility  Firefox 115+
// @description    コンテクストメニューに選択テキストをDeepLで翻訳する機能を追加する
// @note           DeepLのAPIキー (無料版で可) が必要
// @charset        UTF-8
// ==/UserScript==

"use strict";

(function() {
    /* --- 設定ここから --- */
    const apiKey = "Enter your API key here!";
    const apiEndpoint = "https://api-free.deepl.com/v2";
    const hotkey = {
        enabled: false,
        code:    "ControlLeft",
        repeat:  2,
        timeout: 500,
    };
    const defaultLang = "JA";
    const supportedLangs = {
        //"BG": "Bulgarian",
        //"CS": "Czech",
        //"DA": "Danish",
        "DE": "German",
        //"EL": "Greek",
        //"EN-GB": "English (British)",
        "EN-US": "English (American)",
        "ES": "Spanish",
        //"ET": "Estonian",
        //"FI": "Finnish",
        "FR": "French",
        //"HU": "Hungarian",
        "IT": "Italian",
        "JA": "Japanese",
        //"LT": "Lithuanian",
        //"LV": "Latvian",
        //"NL": "Dutch",
        //"PL": "Polish",
        //"PT-PT": "Portuguese",
        //"PT-BR": "Portuguese (Brazilian)",
        //"RO": "Romanian",
        //"RU": "Russian",
        //"SK": "Slovak",
        //"SL": "Slovenian",
        //"SV": "Swedish",
        //"ZH": "Chinese",
    };
    /* --- 設定ここまで --- */

    const parentModule = PathUtils.join(
        PathUtils.join(PathUtils.profileDir, "chrome", "jsm"),
        "DeepLTranslatorParent.mjs"
    );
    IOUtils.stat(parentModule).then(parentInfo => {
        const fileHandler = Services.io.getProtocolHandler("file").QueryInterface(Ci.nsIFileProtocolHandler);
        const resourceHandler = Services.io.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
        if (!resourceHandler.hasSubstitution("deepl-ucjs")) {
            resourceHandler.setSubstitution("deepl-ucjs", Services.io.newURI(PathUtils.toFileURI(PathUtils.parent(parentModule))));
        }
        ChromeUtils.importESModule(`resource://deepl-ucjs/DeepLTranslatorParent.mjs?${parentInfo.lastModified}`).DLTranslator.attachToWindow(window, {apiKey, apiEndpoint, hotkey, defaultLang, supportedLangs});
    }).catch(e => {
        console.error("DeepLTranslator: required .mjs files are not placed correctly!");
    });
}());
